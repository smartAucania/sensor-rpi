#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
## @file sensor_monitor.py
# @brief Puesta en marcha del monitoreo de sensores y publicación de telemetrías.
# @see config/config.py
# @see services/publisher.py
# @see services/sensor.py

import time, threading
from config import config
from services import sensorpool #, ngrok_telegram
from services.sensor import onRead
from services.publisher import ThingsboardPublisher, publishingLoop
from collections import deque
from lib.thingsboard import ThingsBoardHTTP, Logger
from lib.logger_ import RotatingLogger
from services.device import deviceStateLoop
import multiprocessing as mp

#comment for reset

## @brief Inicia procesos de publicación de telemetrías y de actualización de estados del dispositivo. Inicia hilos dentro del proceso principal para la medición de los sensores.
def begin():
	#Configura logger de mensajes del sistema
	rotLogger 		= RotatingLogger("debug.log", maxBytes=20*1024*1024, backupCount=2)
	# token 			= "YrHR06lYLHZ9qyQ9QKZO"
	# host 			= "146.83.206.77"
	# port 			= 80
	attr 			= config.getDeviceAttributes()
	chatId 			= 480097956
	telegramToken 	= "851377220:AAGCgbsdg8tjMQnxgAgW7oNQVr1RulbLHGE"
	logger = Logger(logger = rotLogger, telegramToken = telegramToken, chatId = chatId, level = Logger.WARNING, name = attr["nombre"])

	#Construye publicadores en base a configuraciones config.json
	publishers = deque()
	try:
		for publisher in config.getPublishers():
			if publisher["method"] == "http":
				#Añade publicador
				publishers.append(
					#Construye publicador
					ThingsboardPublisher(
						tableName 	= "t"+publisher["token"],
						format_ 	= publisher["format"],
						logger 		= logger,
						thingsBoard = ThingsBoardHTTP(
							token		= publisher["token"],
							host		= publisher["host"], 
							port		= publisher["port"],
							timeout		= publisher["timeout"]
						)
					)
				)
		publishers = list(publishers)
	except:
		logger.exception("No se ha conseguido conectar con la plataforma Thingsboard")
		publishers = []

	#Crea proceso para indicar estado del dispositivo
	ctx = mp.get_context()
	stateQ = ctx.Queue()
	rgbp = ctx.Process(target=deviceStateLoop, args=(stateQ, logger))
	rgbp.start()

	#Crea proceso para publicar los datos
	q = ctx.Queue()
	p = ctx.Process(target=publishingLoop, args=(publishers, q, stateQ))
	p.start()

	#Comienza medicion de los sensores en un hilo diferente por sensor
	kwargs = {"queue": q, "stateQueue": stateQ, "logger": logger}

	#Inicia los sensores
	sensorpool.startSensorThreads(onRead = onRead, **kwargs)

	return stateQ

if __name__ == "__main__":
	#Comienza mediciones, publicación de telemetrías, publicación de atributos y actualización de estados con led RGB
	stateQ = begin()

	while True:
		time.sleep(100)