#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file config.py
# @brief Acceso a configuraciones del dispositivo definidas en el archivo config.json. Posee varias funciones para acceder a dichas configuraciones.
# @see config/config.json

import json
import os

folder = '/'.join(os.path.realpath(__file__).split('/')[:-1])

## @var configFilePath
# @brief Define la ruta al archivo de configuración.
configFilePath = "%s/config.json"%(folder)

## @brief Obtiene la configuración para un sensor en específico.
# @params[in] sensorName Nombre del sensor a buscar.
# @params[out] sensor Configuración del sensor.
def getSensor(sensorName=""):
	sensor = None
	with open(configFilePath) as f:
		config = json.load(f)
		for sensor_ in config["telemetry"]["sensors"]:
			if sensor_["name"]==sensorName:
				sensor = sensor_.copy()
	return sensor

## @brief Obtiene los atributos del dispositivo.
# @params[out] attr Atributos del dispositivo.
def getDeviceAttributes():
    attr = None
    with open(configFilePath) as f:
        config = json.load(f)
        attr = config["attributes"]
    return attr

# def getAttributesUpdatePeriod():
#     period = None
#     with open(configFilePath) as f:
#         config = json.load(f)
#         period = config["thingsboard"]["attributes"]["period"]
#     return period

## @brief Obtiene los publicadores mediante los cuales reportar telemetrías y atributos del dispositivo.
# @params[out] publishers Configuraciones de los publicadores a usar.
def getPublishers():
    publishers = []
    with open(configFilePath) as f:
        config = json.load(f)
        publishers = config["thingsboard"]["publishers"]
    return publishers

## @brief Obtiene el período de chequeo de actualización del repositorio local. @see git_monitor.py.
# @params[out] period Período de chequeo.
def getGitUpdatePeriod():
    period = 3600
    with open(configFilePath) as f:
        config = json.load(f)
        period = config["git"]["update-period"]
    return period

## @brief Obtiene configuración de la batería.
# @params[out] battery Configuración de la batería.
def getBattery():
    battery = None
    with open(configFilePath) as f:
        config = json.load(f)
        battery = config["battery"]
    return battery

## @brief Obtiene configuración de radio lora.
# @params[out] lora Configuración de la radio lora.
def getLora():
    lora = None
    with open(configFilePath) as f:
        config = json.load(f)        
        lora = config["lora"]
    return lora

def getFakeSensors():
    fakeSensors = None
    with open(configFilePath) as f:
        config = json.load(f)
        sensors = []
        for sensor in config["telemetry"]["sensors"]:
            if "fake" in sensor and sensor["fake"] == 1:
                sensors.append(sensor)
        if len(sensors)>0:
            fakeSensors = sensors
    return fakeSensors