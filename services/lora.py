import sys
sys.path.insert(0,'..')
import traceback
import random
# import PyLora
import time
from services.packager import pack, unpack
import services.packager as packager
from services.rfm95 import RFM95

class Lora():
    NCADS_DIFS          = 11
    MAX_CADS_BACKOFF    = 144
    MAX_BACKOFF_RETRYS  = 4

    def __init__(self, frequency = 868000000, crc = True, tx_power = 13, 
        sf = 10, bw = 125000, header_mode = "explicit", sync_word = 0x73, rate = 5,
        len_preamble = 4, cs_pin = 17, rst_pin = 27, dio0_pin = 5, nodes = [],
        short_id = 0x00, rx_on_idle = False, pan_id = 0x0000):

        # #Almacena datos de la radio
        # self.frequency          = frequency
        # self.crc                = crc
        # self.tx_power           = tx_power
        # self.spreading_factor   = sf
        # self.bandwidth          = bw
        # self.header_mode        = header_mode
        # self.sync_word          = sync_word
        # self.coding_rate        = rate
        # self.preamble_length    = len_preamble
        # self.cs_pin             = cs_pin
        # self.rst_pin            = rst_pin
        # self.irq_pin            = dio0_pin
        # self.rx_on_idle         = rx_on_idle

        #Init lora
        self.rfm95                   = RFM95(cs = cs_pin, reset = rst_pin, irq = dio0_pin)

        #Configure lora
        self.rfm95.tx_power          = tx_power
        self.rfm95.frequency         = frequency
        self.rfm95.header_mode       = header_mode
        self.rfm95.spreading_factor  = sf
        self.rfm95.bandwidth         = bw
        self.rfm95.coding_rate       = rate
        self.rfm95.preamble_length   = len_preamble
        self.rfm95.sync_word         = sync_word
        self.rfm95.crc               = crc

        self.nodes              = nodes
        self.short_id           = short_id
        self.pan_id             = pan_id

        # #Configura los pines de la radio
        # PyLora.set_pins(cs_pin = self.cs_pin, rst_pin = self.rst_pin, irq_pin = self.irq_pin)

        # #Configura la radio
        # PyLora.init()
        # PyLora.set_frequency(self.frequency)
        # if self.crc:
        #     PyLora.enable_crc()
        # else:
        #     PyLora.disable_crc()
        # PyLora.set_preamble_length(self.preamble_length)
        # PyLora.set_spreading_factor(self.spreading_factor)
        # PyLora.set_coding_rate(self.coding_rate)
        # PyLora.set_bandwidth(self.bandwidth)
        # if self.header_mode == "explicit":
        #     PyLora.explicit_header_mode()
        # else:
        #     PyLora.implicit_header_mode()
        # PyLora.set_sync_word(self.sync_word)

    def get_node(self, short_id):
        return self.nodes.find_node(attr = "short_id", value = short_id)

    def add_node(self, node):
        self.nodes.append(node)
    
    def drop_node(self, short_id):
        self.nodes.remove(short_id)
    
    def set_nodes(self, nodes):
        self.nodes = nodes
    
    def __str__(self):
        # # state = "Lora RF95 Version: %s\n"%(hex(self.version()))
        # state = ""
        # state = state + "Transmission power: %d\n"%self.tx_power
        # state = state + "Header Mode: %s\n"%self.header_mode
        # state = state + "Bandwidth: %d\n"%self.bandwidth
        # state = state + "Coding rate: 4/%d\n"%self.coding_rate
        # state = state + "Preamble length: %d\n"%self.preamble_length
        # state = state + "Sync word: %s\n"%(hex(self.sync_word))
        # state = state + "Spreading Factor: %d\n"%self.spreading_factor
        # if self.crc:
        #     state = state + "CRC: Enabled"
        # else:
        #     state = state + "CRC: Disabled"
        # return state

        return self.rfm95.__str__()

    def idle(self):
        if self.rx_on_idle:
            # PyLora.receive()
            self.rfm95.receive()
        else:
            # PyLora.idle()
            self.rfm95.idle()

    def __cad(self):
        print(".", end="")
        # PyLora.idle()
        self.rfm95.idle()
        time.sleep(0.001)
        # active =  PyLora.channel_active()
        self.rfm95.cad()
        return self.rfm95.channel_active()
    
    def wait_channel(self): 
        while self.__cad():
            time.sleep(10*1e-3)
        return False

    def difs(self):
        for i in range(self.NCADS_DIFS):
            if self.__cad():
                return False
        return True

    def rssi(self):
        # return PyLora.packet_rssi()
        return self.rfm95.rssi()
    
    def backoff(self):
        #Calcula el numero aleatorio de cads necesarios
        w       = self.NCADS_DIFS*2
        ncads   = int(random.random()*w)
        ncads   = min(ncads, self.MAX_CADS_BACKOFF)

        #Inicializa contadores
        cads_consumed = 0
        nbad_difs = 0

        for i in range(self.MAX_BACKOFF_RETRYS):           
            #Consume los cads esperando que el canal este vacio
            while not self.__cad():
                #Actualiza contador de cads consumidos
                cads_consumed += 1

                if cads_consumed >= w:
                    #El canal ha estado libre el tiempo suficiente
                    return True
        
            #El canal esta ocupado            
            while True:
                #Espera a que el canal se desocupe
                self.wait_channel()

                #Ejecuta un difs
                if self.difs():
                    break
                
                #Actualiza contador de difs fallidos
                nbad_difs += 1
                if nbad_difs > 1:
                    #Recalcula el numero aleatorio de cads necesarios
                    w               = self.NCADS_DIFS*2*(2**(nbad_difs-1))
                    ncads           = int(random.random()*w)
                    ncads           = min(ncads, self.MAX_CADS_BACKOFF) 
                    cads_consumed   = 0
        return False

    def csma(self):
        if self.difs():
            return True
        self.wait_channel()
        return self.backoff()
    
    def send(self, msg, csma=True):
        self.rfm95.invert_iq(invert = True)
        if csma:
            if self.csma():
                self.rfm95.send_packet(msg)
                is_sent = self.rfm95.wait_packet_sent(timeout = 3500)
                self.idle()
                return is_sent
        else:
            self.rfm95.send_packet(msg)
            is_sent = self.rfm95.wait_packet_sent(timeout = 3500)
            self.idle()
            return is_sent
        self.idle()
        return False

    def send_ack(self, packet_id = 0, npacket = 0, short_id = None):
        key = None
        if short_id == None or short_id == 0x00:
            #El dispositivo es un satelite. El destino es el coordinador
            short_id = self.short_id            
        node = self.get_node(short_id = short_id)
        if not node is None:
            key = node.key #Si este es un satelite, este key debiese pertenecer a este dispositivo
        packet = pack(pan_id = self.pan_id, packet_id = packet_id, short_id = short_id, cmd = packager.CMD_ACK, data = {"packet id": npacket&0xFFFF}, key = key)
        return self.send(packet,csma=False)
    
    def send_wait_ack(self, msg):
        #Falta chequear que el ack corresponda al paquete que enviamos
        for i in range(3):
            if self.send(msg):
                raw, ack = self.receive(timeout = 2500)
                if not (raw is None and ack is None):
                    short_id = self.short_id
                    if short_id == 0x00:
                        short_id = ack["short id"]
                    node = self.get_node(short_id)
                    if not ack is None and ack["pan id"] == self.pan_id and ack["cmd"] == packager.CMD_ACK and not node is None:
                        msg = unpack(raw, node.auth_key)
                        return msg
    
    def receive(self, timeout = -1):
        self.rfm95.invert_iq(invert = False)
            # PyLora.idle()
        # self.rfm95.idle()
        # time.sleep(0.1)
        # PyLora.receive()
        self.rfm95.receive()
        # c = 0
        # while not PyLora.packet_available():
        #     time.sleep(0.1)
        #     if timeout > 0:
        #         c += 1
        #         if c >= timeout/100:
        #             return (None, None)
        # msg = PyLora.receive_packet()
        # print([int(c) for c in msg])
        # if not msg is None:
        #     # self.idle()
        #     return (msg, unpack(msg))
        # else:
        #     # self.idle()
        #     return (None, None)
        if(self.rfm95.wait_for_packet(timeout = timeout)):
            msg = self.rfm95.receive_packet()
            # print("Llega mensaje")
            return (msg, unpack(msg))
        else:
            # print("No llego nada...")
            return (None, None)
     
    #short id para unirse a una red es 0x00
    #
    def receive_send_ack(self):
        for i in range(3):
            raw, msg    = self.receive(timeout = 1500)
            if not raw is None:
                node      = self.get_node(msg["short id"])
                #El mensaje existe
                #El mensaje proviene de esta red
                #La dirección es la de este dispositivo o es reconocida
                #Este dispositivo es el cooordinador y el comando es para asociarse a la red
                if (not msg is None) and msg["pan id"] == self.pan_id and ((msg["short id"] == self.short_id) or (not node is None and self.short_id == 0x00) or (self.short_id == 0x00 and msg["cmd"] == packager.CMD_REQ_ASSOCIATE)): 
                    if not self.short_id == 0x00:
                        node = self.get_node(self.short_id)
                    if msg["cmd"] == packager.CMD_REQ_ASSOCIATE and self.short_id == 0x00:
                        return msg
                    key = node.auth_key
                    msg = unpack(raw, key)
                    #Falta chequear crc
                    try:
                        time.sleep(0.05)
                        print("Sending ack")
                        self.send_ack(npacket = msg["packet id"], short_id = msg["short id"])
                        return msg
                    except Exception as e:
                        traceback.print_exc()
    
    def receive_broadcast(self):
        for i in range(3):
            msg    = self.receive(timeout = 850)
            # device      = self.get_device(msg["short id"])
            #El mensaje existe
            #El mensaje es tipo broadcast
            if (not msg is None) and msg["pan id"] == self.pan_id and (msg["short id"] == 0xFF): 
                # key = device["key"]
                # msg = unpack(raw, key)
                #Falta chequear crc
                return msg
    
    def sleep(self):
        # PyLora.sleep()
        self.rfm95.sleep()

if __name__ == "__main__":
    from node import Node, NodeGroup
    short_id = 0x00
    key = b"123456789012345D"
    nodes = [
        Node(short_id = 0xAA, auth_key = b"123456789012345A", key = "imdevice1"),
        Node(short_id = short_id, auth_key = key, key = "coordinator")
    ]
    node_group = NodeGroup(nodes)

    #Create object for lora
    lora = Lora(short_id = short_id, nodes = node_group)

    #Print lora configuration
    print()
    print("LORA INFORMATION")
    print("****************")
    print(lora)

    i = 0
    while True:
        msg = lora.receive_send_ack()
        if(not msg is None):
            print("message %d received"%i)
            if msg["cmd"] == packager.CMD_TELEMETRY:
                for meas in msg["data"]["measurements"]:
                    print(meas)
        else:
            print("message %d wasn't received"%i)
        i+=1

        # meas = packager.Measurement(meas_id = packager.MEAS_AM2315_HUM, value = 62.5)
        # packet = pack(packet_id = 0, short_id = nodes[0].short_id, cmd = packager.CMD_TELEMETRY, data = [meas], key = nodes[0].auth_key)
        # # print("Enpaquetado", [int(c) for c in packet], len(packet))
        # # print(unpack(packet, key = devices[1]["key"]))
        # if(lora.send_wait_ack(packet)):
        #     print("message %d sent"%i)
        # else:
        #     print("message %d wasn't sent"%i)
        # i+=1