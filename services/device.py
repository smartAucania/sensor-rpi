#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file device.py
# @brief Funcionalidades referentes a la indicación de estados del dispositivo.
# @see config/config.py
# @see sensor_monitor.py
# @see services/publisher.py

import sys
sys.path.insert(0,'..')
import time
from lib import rgbled

## @class Device
class Device:
    ## @brief Constructor de la clase.
    # @param[in] battery Nivel de batería.
    # @param[in] measuring Bandera que indica si el dispositivo está midiendo correctamente.
    # @param[in] publishing Bandera que indica si el dispositiov está publicando.
    # @param[in] rgb Objeto RGBLed que permite interactuar con el led RGB.
    # @param[in] publishingBlinkTimeout Duración de parpadeo del led que indica publicación.
    # @param[in] measuringBlinkTimeout Duración de parpadeo del led que indica medición.
    def __init__(self, battery='low', measuring = False, publishing = True, rgb = None, publishingBlinkTimeout = 0.5, measuringBlinkTimeout = 0.7, logger = None):
        ## Estado de carga de la bateria
        self.battery    = battery                   
        ## Flag que indica si el dispositivo está midiendo correctamente
        self.measuring  = measuring                 
        ## Flag que indica si el dispositivo está publicando correctamente
        self.publishing = publishing                
        ## Objeto que maneja el led rgb
        self.rgb        = rgb                       
        ## Duración de encendido de parpadeo del led para indicar medición
        self.mbt        = measuringBlinkTimeout     
        ## Duración de encendido de parpadeo del led para indicar publicación
        self.pbt        = publishingBlinkTimeout
        ## Objeto para almacenar mensajes de log
        self.logger     = logger
    
    ## @brief Configura el nivel de batería.
    # @param[in] level Nivel de batería. Puede ser "low", "medium" o "full".
    def setBatteryLevel(self, level):
        if not self.logger is None:
            self.logger.info("Battery Level: %s"%level)

        #Almacena nivel de bateria
        self.battery = level

        #Verifica que el manejador del led RGB no sea None
        if not self.rgb is None:
            if self.battery == 'low':
                #Bateria baja. Selecciona color rojo
                color = rgbled.Color.RED
            elif self.battery == 'medium':
                #Bateria media. Selecciona color amarillo
                color = rgbled.Color.YELLOW
            elif self.battery == 'full':
                #Bateria llena. Selecciona color verde
                color = rgbled.Color.GREEN
            else:
                #No existe tal nivel
                color = None
            #Actualiza color led RGB
            self.rgb.setColor(color)

    ## @brief Establece si se están publicando los datos.
    # @param[in] publishing Bandera que indica si se está publicando.
    def setIsPublishing(self, publishing, measurement):
        #Almacena bandera que indica que se hicieron todas las publicaciones
        self.publishing = publishing

        if not self.isPublishing() and not self.logger is None:
            self.logger.warning("La medicion %s no se está publicando en todos los publicadores"%measurement)

        #Verifica que exista el manejador del led RGB
        if not self.rgb is None:
            if self.publishing:
                #Se están publicando todos los datos. Selecciona color azul
                color = rgbled.Color.BLUE
            else:
                #No se están publicando todos los datos. Selecciona color morado.
                color = rgbled.Color.PURPLE
            
            #Realiza parpadeo con el color seleccionado
            self.rgb.blink(color, self.pbt)
    
    ## @brief Establece si se está midiendo correctamente.
    # @param[in] measuring Bandera que indicas si se está midiendo correctamente.
    def setIsMeasuring(self, measuring, sensor):
        #Almacena bandera que indica que se hicieron todas las mediciones
        self.measuring = measuring

        if not self.isMeasuring() and not self.logger is None:
            self.logger.warning("El sensor %s no está proporcionando todas sus mediciones"%sensor)

        if not self.measuring and not self.rgb is None:
            #No se hicieron todas las medicionse. Realiza parpadeo con luz celeste
            self.rgb.blink(rgbled.Color.LIGHT_BLUE, self.mbt)
    
    ## @brief Indica si el dispositivo está midiendo correctamente.
    def isMeasuring(self):
        return self.measuring
    
    ## @brief Indica si el dispositivo se encuentra publicando.
    def isPublishing(self):
        return self.publishing
    
    ## @brief Indica el nivel de batería del dispositivo.
    def batteryLevel(self):
        return self.battery

## @brief Loop que recibe información de cambios de estado del dispositivo, informando de estos mediante el uso de un led RGB.
# @param[in] stateQueue Queue multiproceso mediante la cual recibe información de cambios de estado del dispositivo. El formato es un diccionario. A continuación se ejemplifican los distintos cambios de estado.
# - Publicando datos: {"attribute": "publishing", "state": True}
# - No se ha podido publicar: {"attribute": "publishing", "state": False}
# - Midiendo todas las variables del sensor: {"attribute": "measuring", "state": True}
# - Al menos una de las variables del sensor no se está midiendo: {"attribute": "measuring", "state": False}
# - Nivel de bateria bajo: {"attribute": "battery-level", "state": "low"}
# - Nivel de bateria medio: {"attribute": "battery-level", "state": "medium"}
# - Nivel de bateria lleno: {"attribute": "battery-level", "state": "full"}
def deviceStateLoop(stateQueue, logger):
    #Crea objeto para manejar led rgb
    rgb = rgbled.RGBLed(20, 16, 21)

    #Configura color inicial
    rgb.setColor(rgbled.Color.GREEN)

    #Crea objeto con el estado del dispositivo
    device = Device(rgb = rgb, logger = logger)

    while True:
        #Espera a que se indique un cambio en el estado del dispositivo
        while stateQueue.empty():
            time.sleep(1)
        
        #Obtiene el estado
        state = stateQueue.get()

        #Procesa el cambio de estado
        if state["attribute"] == "measuring":
            #Indica si el sensor está midiendo todas sus variables
            device.setIsMeasuring(state["state"], state["sensor"])
        elif state["attribute"] == "publishing":
            #Indica si el sensor está publicando todas sus mediciones
            device.setIsPublishing(state["state"], state["measurement"])
        elif state["attribute"] == "battery-level":
            #Indica el nivel de batería del dispositivo
            device.setBatteryLevel(state["state"])
        
        #Delay
        time.sleep(0.2)