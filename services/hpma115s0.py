#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file hpma115s0.py
# @brief Contiene clase con funciones para monitoreo del sensor hpma115s0 usando esquema multihilo.
# @see config/config.py
# @see services/sensor.py

import sys
sys.path.insert(0,'..')
from services import uart
import pigpio, threading, time, traceback
from collections import deque
from services.sensor import Sensor
from services.sensorpool import SensorGuard
import numpy as np

## @class HPMA115S0
# Hereda de clase Sensor. Permite la medición periódica del sensor mediante esquema multihilo.

## @code{.py}
# if __name__=='__main__':
#     import statistics, numpy as np
#     from config import config
#     def onRead(sensorObj):
#         #Obtiene marca de tiempo en unix y timestamp str
#         unix 		= np.datetime64('now').astype('float64')*1000 #Thingsboard acepta tiempo unix en milisegundos
#         timestamp 	= str(np.datetime64('now')).replace('T', ' ')
#
#         #Carga configuraciones del sensor
#         sensor = config.getSensor(sensorObj.name)
#
#         if not sensor is None:			
#             for measurement in sensor["measurements"]:
#                 #Obtiene valor de la medición en base a mediana
#                 buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))
#
#                 if len(buff)>0:
#                     value 										= statistics.median(buff)
#
#                     #Manda mensaje de debugueo
#                     print("%s | Data obtenida de %s. %s: %s"%(timestamp, sensor["name"], measurement["name"], repr(value)))
#     threads = []
#     #Obtiene configuración para el sensor am2315
#     amConfig = config.getSensor("am2315")
#     if not amConfig is None:
#         from services.am2315_ import AM2315_
#         #Crea hilo para lectura del sensor am2315
#         am2315 = AM2315_(buffLen = amConfig["buffLen"], pin=amConfig["enable_gpio"], period = amConfig["period"], sem = None, on_read = onRead)
#         #Comienza lectura del sensor
#         am2315.start()
#         #Añade sensor a la lista
#         threads.append(am2315)
#     #Obtiene configuración para el sensor hpma115s0
#     hpmaConfig = config.getSensor("hpma115s0")
#     if not hpmaConfig is None:
#         from services.hpma115s0 import HPMA115S0
#         #Crea hilo para lectura del sensor hpma115s0
#         hpma = HPMA115S0(buffLen = hpmaConfig["buffLen"], pin = hpmaConfig["enable_gpio"], on_read = onRead, period = hpmaConfig["period"], port = hpmaConfig["bus"]["port"])
#         #Comienza lectura del sensor
#         hpma.start()
#         threads.append(hpma)
#     #Inicia hilo para proteger los sensores manteniendolos en sus rangos de operación
#     sensorGuard = SensorGuard(threads)
#     sensorGuard.start()
# @endcode
class HPMA115S0(Sensor):
    ## Nombre del sensor
    name    = "hpma115s0"   
    ## Limite superior del rango de medición del material particulado en ug/m^3
    LIMIT   = 1000

    ## @brief Constructor de la clase.
    # @param[in] buffLen Tamaño del buffer por cada medición.\n
    # @param[in] pin Pin GPIO para habilitar el sensor.\n
    # @param[in] on_read Función callback que se llama al llenar el buffer.\n
    # @param[in] period Período de tiempo entre mediciones.\n
    # @param[in] port Puerto serial al que está conectado el sensor.\n
    # @param[in] nattempts Cantidad de intentos de lectura.\n
    # @param[in] maxTimeOffset Máximo tiempo de offset entre que termina un período de mediciones y comienza el siguiente.\n
    # @param[in] kwargs Diccionario con parámetros de entrada para función callback.
    def __init__(self, buffLen = 11, pin = 18, on_read = None, period = 10, port = "/dev/ttyAMA0", nattempts = 3, maxTimeOffset = 15, **kwargs):
        ## Pin gpio para encender o apagar el sensor
        self.pin            = pin                   
        ## Objeto pigpio para acceder a la gpio de la rpi                    
        self.pi             = pigpio.pi()                               
        ## Evento para indicar si el puerto serial ha finalizado la operación
        self.ready          = threading.Event()
        ## Objeto serial para lectura y comandos del sensor
        self.ser            = uart.Serial_(port, ready = self.ready, logger = kwargs.get("logger", None))    
        self.ser.start()     
        ## Tamaño del buffer de mediciones
        self.buffLen 	    = buffLen                                                    
        ## Callback a llamar cuando se hayan completado los buffers
        self.on_read        = on_read                                   
        ## Periodo de tiempo entre lecturas
        self.period         = period                                    
        ## Variable temporal de lectura de pm2.5
        self._pm2p5         = None                                      
        ## Variable temporal de lectura de pm10
        self._pm10          = None                                      
        ## Argumentos extra para callback
        self.kwargs         = kwargs                                    
        ## Cantidad de intentos de comunicación por comandos
        self.nattempts      = nattempts                                 
        ## Cantidad máxima de tiempo extra que puede tardar en tomar todas las muestras
        self.maxTimeOffset 	= maxTimeOffset					            
        self.logger         = kwargs.get("logger", None)
        #Inicializa hilo
        Sensor.__init__(self, pin = pin, buffLen = buffLen, on_read = on_read, period = period, maxTimeOffset = maxTimeOffset, **kwargs)

        #Apaga el sensor
        self.powerOff()

    def appendMeasure(self):
        # self.readFake()
        self.stopAutoSend()
        self.startConversion()
        time.sleep(8)
        if self.read():
            self.pm2p5Buff.append(self.pm2p5())
            self.pm10Buff.append(self.pm10())
        else:
            if not self.logger is None:
                self.logger.error("Ha fallado la lectura del sensor %s"%self.name)
        self.stopConversion()
    
    def initBuffers(self):
        ## Buffer de mediciones de pm10
        self.pm10Buff 	    = deque([], maxlen = self.buffLen)                 
        ## Buffer de mediciones de pm2.5
        self.pm2p5Buff 	    = deque([], maxlen = self.buffLen)
    
    def readFake(self):
        self.pm10Buff.append(np.random.normal(100, 5, 1)[0])
        self.pm2p5Buff.append(np.random.normal(90, 10, 1)[0])

    ## @brief Obtiene el último valor de PM 2.5.
    # @param[out] _pm2p5 Último valor de PM 2.5.
    def pm2p5(self):
        #Limita valores del pm2.5 al rango indicado en el datasheet
        if self._pm2p5<0:
            self._pm2p5 = 0
        if self._pm2p5 > HPMA115S0.LIMIT:
            self._pm2p5 = HPMA115S0.LIMIT
        return self._pm2p5

    ## @brief Obtiene el último valor de PM 10.
    # @param[out] _pm2p5 Último valor de PM 10.
    def pm10(self):
        #Limita valores del pm10 al rango indicado en el datasheet
        if self._pm10<0:
            self._pm10 = 0
        if self._pm10 > HPMA115S0.LIMIT:
            self._pm10 = HPMA115S0.LIMIT
        return self._pm10
    
    ## @brief Escribe un comando en el sensor.
    # @param[in] cmd Comando a enviar.
    # @param[out] res Bandera que indica que el comando fue enviado exitosamente.
    def sendCmd(self, cmd):
        #Limpia buffer de entrada
        self.ser.resetInput()

        #Inicializa variables de ack
        ack1 = 0x00
        ack2 = 0x00

        #Inicializa contador de intentos de comunicación
        attempt = 1

        #Escribe mensaje de comienzo de lectura
        while(not ack1 == 0xA5 and not ack2 == 0xA5) and (attempt <= self.nattempts):
            ack1=0x00
            while(not ack1 == 0xA5) and (attempt <= self.nattempts):
                #Limpia buffer serial de entrada
                # print("HPMA: reset input")
                self.ser.resetInput()

                #Escribe el comando
                # print("HPMA: Escribe %s"%(repr(cmd)))
                self.ser.write(cmd)

                #Espera que el comando sea enviado
                self.ser.flush()
                # print("HPMA: Flush")

                time.sleep(0.1)
                try:
                    #Lee ack
                    # print("HPMA: Lee %d bytes"%(1))
                    self.ser.read(1)
                    # print("HPMA: Espera que uart termine el trabajo")
                    self.waitToFinishWork()
                    ack1 = bytearray(self.ser.getBuffer())[0]
                    # print("HPMA: ACK1 recibido %s"%(repr(hex(ack1))))
                except:
                    traceback.print_exc()
                    ack1 = 0x00	
                attempt += 1
            try:
                #Lee segundo byte del ack
                # print("HPMA: Lee %d bytes"%(1))
                self.ser.read(1)
                # print("HPMA: Espera que uart termine el trabajo")
                self.waitToFinishWork()
                ack2 = bytearray(self.ser.getBuffer())[0]
                # print("HPMA: ACK2 recibido %s"%(repr(hex(ack2))))
            except:
                traceback.print_exc()
                ack2 = 0x00
        #Limpia buffer de entrada
        # print("HPMA: reset input")
        self.ser.resetInput()

        return (ack1 == 0xA5 and ack2 == 0xA5)
    
    ## @brief Detiene función autosend.
    # @param[out] res Bandera que indica si el comando fue ejecutado exitosamente.
    def stopAutoSend(self):
        # print("start autosend")
        return self.sendCmd([0x68,0x01,0x20,0x77]) #Detiene autosend

    ## @brief Comienza conversión.
    # @param[out] res Bandera que indica si el comando fue ejecutado exitosamente.
    def startConversion(self):
        # print("start conversion")
        return self.sendCmd([0x68,0x01,0x01,0x96]) #Comienza conversión
    
    ## @brief Detiene la conversión.
    # @param[out] res Bandera que indica si el comando fue ejecutado exitosamente.
    def stopConversion(self):
        # print("start conversion")
        return self.sendCmd([0x68,0x01,0x02,0x95]) #Detiene conversión
    
    def waitToFinishWork(self):
        st = time.time()
        self.ready.wait(timeout=5.2)
        self.ready.clear()
        # print("Elapsed time: %.2f"%(time.time()-st))
        if(time.time()-st>=5):
            # print("HPMA115S0: Se debe reiniciar UART")
            self.ser.reopen()
            return False
        return True

    
    ## @brief Lee PM 2.5 y PM 10.
    # @param[out] res Bandera que indica si se logró realizar la lectura.
    def read(self):
        # print("read")
        cmd = [0x68, 0x01, 0x04, 0x93]
        ack1 = 0x96
        length = 0x96

        #Inicializa contador de intentos de comunicación
        attempt = 1
        
        while ((ack1 is None) or (not ack1 == 0x40) or (not length == 0x05)) and (attempt <= self.nattempts):
            #Escribe comando de solicitud de lectura
            self.ser.write(cmd)
            while (not ack1 == 0x40) and (attempt <= self.nattempts):
                time.sleep(0.2)
                try:
                    #Lee ack
                    self.ser.read(1)
                    if not self.waitToFinishWork():
                        return False
                    ack1 = bytearray(self.ser.getBuffer())[0]
                except:
                    traceback.print_exc()
                    ack1 = 0x40
                
                #Actualiza contador de intentos
                attempt += 1
            time.sleep(0.2)
            try:
                #Lee largo del paquete
                self.ser.read(1)
                if not self.waitToFinishWork():
                    return False
                length = bytearray(self.ser.getBuffer())[0]
            except:
                traceback.print_exc()
                ack1 = 0x96

            time.sleep(0.2)
            # print("Lenght: %d"%length)
            if(not ack1==0x96 and not length==0x96):
                resp = [ack1, length]
                
                #Lee medicion
                self.ser.read(6)
                self.waitToFinishWork()
                buff = self.ser.getBuffer()
                resp.extend(buff)

                #Guarda medicion
                hpma = {"pm2.5":   	int(resp[3]*256+resp[4]),\
                        "pm10":     int(resp[5]*256+resp[6]),\
                        "checksum": int(resp[7])}

                if(hpma['checksum'] == (65536 - sum(resp[:7]))%256):
                    #La medicion es correcta, por lo que se almacena en el objeto
                    self._pm2p5     = hpma["pm2.5"]
                    self._pm10      = hpma["pm10"]
                    return True
            return False
        return False

if __name__=='__main__':
    import statistics, numpy as np
    from config import config
    from lib.thingsboard import ThingsBoardHTTP, Logger
    from lib.logger_ import RotatingLogger
    #Configura logger de mensajes del sistema
    rotLogger 	= RotatingLogger("debug.log", maxBytes=20*1024*1024, backupCount=2)
    token 		= "YrHR06lYLHZ9qyQ9QKZO"
    host 		= "146.83.206.77"
    port 		= 80
    logger = Logger(logger = rotLogger, thingsboard = ThingsBoardHTTP(token, host, port = port), level = Logger.WARNING, name = "rpi desarrollo")
    kwargs = {"logger": logger}
    def onRead(sensorObj, **kwargs):
        #Obtiene marca de tiempo en unix y timestamp str
        unix 		= np.datetime64('now').astype('float64')*1000 #Thingsboard acepta tiempo unix en milisegundos
        timestamp 	= str(np.datetime64('now')).replace('T', ' ')

        #Carga configuraciones del sensor
        sensor = config.getSensor(sensorObj.name)

        if not sensor is None:			
            for measurement in sensor["measurements"]:
                #Obtiene valor de la medición en base a mediana
                buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))

                if len(buff)>0:
                    value 										= statistics.median(buff)

                    #Manda mensaje de debugueo
                    print("%s | Data obtenida de %s. %s: %s"%(timestamp, sensor["name"], measurement["name"], repr(value)))
    threads = []
    #Obtiene configuración para el sensor am2315
    amConfig = config.getSensor("am2315")
    if not amConfig is None:
        from services.am2315_ import AM2315_
        #Crea hilo para lectura del sensor am2315
        am2315 = AM2315_(buffLen = amConfig["buffLen"], pin=amConfig["enable_gpio"], period = amConfig["period"], sem = None, on_read = onRead, **kwargs)
        #Comienza lectura del sensor
        am2315.start()
        #Añade sensor a la lista
        threads.append(am2315)
    #Obtiene configuración para el sensor hpma115s0
    hpmaConfig = config.getSensor("hpma115s0")
    if not hpmaConfig is None:
        from services.hpma115s0 import HPMA115S0
        #Crea hilo para lectura del sensor hpma115s0
        hpma = HPMA115S0(buffLen = hpmaConfig["buffLen"], pin = hpmaConfig["enable_gpio"], on_read = onRead, period = hpmaConfig["period"], port = hpmaConfig["bus"]["port"], **kwargs)
        #Comienza lectura del sensor
        hpma.start()
        threads.append(hpma)
    #Inicia hilo para proteger los sensores manteniendolos en sus rangos de operación
    sensorGuard = SensorGuard(threads)
    sensorGuard.start()