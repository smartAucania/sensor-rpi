import sys
sys.path.insert(0,'..')
import time
from collections import deque
import services.packager
import json

#Documentado desde el punto de vista del gateway
class Node:
    def __init__(self, short_id = 0x00, key = None, auth_key = None, sw_version = "0.0.0"):
        self.short_id                   = short_id  #Identificador corto del dispositivo
        self.key                        = key       #Clave usada para encriptar
        self.auth_key                   = auth_key  #Clave para obtener clave para encriptar
        self.packets_received           = 0x00      #Cantidad de paquetes recibidos por este despositivo
        self.packets_sent               = 0x00      #Cantidad de paquetes enviados a mi
        self.last_packet_sent           = None      #Ultimo paquete recibido por mi
        self.last_packet_received       = None      #Ultimo paquete recibido en el dispositivo
        self.last_sent_timestamp        = None      #Marca de tiempo del último paquete recibido por mi
        self.last_received_timestamp    = None      #Marca de tiempo del último paquete recibido en el dispositivo
        self.ack_packets                = 0x00      #Cantidad de acks recibidos por mi
        self.polling_queue              = deque([]) #Cola de mensajes polling
        self.unack_msgs                 = deque([]) #Cola de mensajes enviados al dispositivo sin ACK
        self.sw_version                 = sw_version#Versión de software del dispositivo
        self.new_sw_available           = False
        self.new_sw_version             = sw_version
        self.dev                        = None
        self.new_sw_files               = []

    def dict(self):
        return {
            "short id":         self.short_id,
            "key":              self.key,
            "auth key":         self.auth_key,
            "packets received": self.packets_received,
            "packets sent":     self.packets_sent,
            "ack packets":      self.ack_packets,
            "sw version":       self.sw_version,
            "new sw available": self.new_sw_available,
            "new sw version":   self.new_sw_version,
            "dev":              self.dev,
            "new sw files":     self.new_sw_files
        }
    
    @staticmethod
    def from_dict(dict_):
        node                    = Node()
        node.short_id           = dict_["short id"]
        node.key                = dict_["key"]
        node.auth_key           = dict_["auth key"]
        node.packets_received   = dict_["packets received"]
        node.packets_sent       = dict_["packets sent"]
        node.ack_packets        = dict_["ack packets"]
        node.sw_version         = dict_["sw version"]
        node.new_sw_available   = dict_["new sw available"]
        node.new_sw_version     = dict_["new sw version"]
        node.dev                = dict_["dev"]      
        node.new_sw_files       = dict_["new sw files"]
        return node
    
    def lost_packets(self):
        return self.packets_sent - self.ack_packets

    def generate_packet_id(self):
        packet_id = 0
        if not self.last_packet_sent is None:
            packet_id = self.last_packet_sent["packet id"]
            packet_id += 1
            if packet_id > 254:
                packet_id = 0x01
        return packet_id
    
    def remove_polling_msg(self, msg):
        print("buscando polling msg con packet id %d"%msg["packet id"])
        for i in range(len(self.polling_queue)):
            print("polling msg: ", self.polling_queue[i])
            if self.polling_queue[i]["packet id"] == msg["packet id"]:
                print("Packet id encontrado! Se elimina polling msg")
                msg = json.dumps(self.polling_queue[i])
                del self.polling_queue[i]
                return json.loads(msg)
    
    def remove_unack_msg(self, msg):
        for i in range(len(self.unack_msgs)):
            if self.unack_msgs[i]["packet id"] == msg["packet id"]:
                del self.unack_msgs[i]
                break
    
    def remove_polling_update_packet(self, msg):
        for i in range(len(self.polling_queue)):
            if self.polling_queue[i]["data"]["filename"] == msg["data"]["filename"] and msg["cmd"] == self.polling_queu[i]["cmd"]:
                del self.polling_queue[i]
                break
    
    def mark_sw_updated(self):
        self.new_sw_available   = False
        self.sw_version         = self.new_sw_version
        self.new_sw_files       = []
    
    def mark_sw_file(self, filename):
        if filename in self.new_sw_files:
            self.new_sw_files.remove(filename)
        for pm in list(self.polling_queue):
            if pm["cmd"] == packager.CMD_NEW_VERSION and pm["data"]["new version"]["filename"] == filename:
                self.remove_polling_msg(pm)

class NodeGroup:
    def __init__(self, nodes):
        self.nodes = nodes
    
    def append(self, node):
        self.nodes.append(node)

    def remove(self, short_id):
        for i in range(len(self.nodes)):
            if self.nodes[i].short_id == short_id:
                del self.nodes[i]
                return True
        return False

    def find_node(self, attr = "short_id", value = None):
        for node in self.nodes:
            if getattr(node, attr) == value:
                return node
    
    def last_packet_sent(self, short_id):
        node = self.find_node(attr = "short_id", value = short_id)
        if not node is None:
            return node.last_packet_sent

    def generate_packet_id(self, short_id):
        node = self.find_node(attr = "short_id", value = short_id)
        if not node is None:
            return node.generate_packet_id()
        return 0

    def generate_short_id(self):
        short_id_list = [node.short_id for node in self.nodes]
        for i in range(1,256):
            if not i in short_id_list:
                return i
        return None
    
    def update_last_packet_sent(self, packet):
        node = self.find_node(attr = "short_id", value = packet["short id"])
        if not node is None:
            node.last_packet_sent       = packet
            node.last_sent_timestamp    = time.time()
            node.packets_sent           += 1
    
    def update_last_received_packet(self, packet):
        node = self.find_node(attr = "short_id", value = packet["short id"])
        if not node is None:
            node.last_received_packet       = packet
            node.last_received_timestamp    = time.time()
            node.packets_received           += 1

    def first_polling_msg(self, short_id):
        node                = self.find_node(attr = "short_id", value = short_id)
        len_polling_queue   = self.count_polling_msgs(short_id)
        if len_polling_queue > 0:
            if not node.polling_queue[0]["cmd"] == 9:
                node.polling_queue[0]["data"]["npackets"] = len_polling_queue - 1
            return node.polling_queue[0]
    
    def count_polling_msgs(self, short_id):
        node = self.find_node(attr = "short_id", value = short_id)
        if not node is None:
            return len(node.polling_queue)
        return 0

    def remove_polling_msg(self, msg):
        node = self.find_node(attr = "short_id", value =  msg["short id"])
        if not node is None:
            print("Removiendo polling msg de nodo con short id: %d"%msg["short id"])
            print("Mensaje: ", msg)
            return node.remove_polling_msg(msg)
    
    def remove_unack_msg(self, msg):
        node = self.find_node(attr = "short_id", value =  msg["short id"])
        if not node is None:
            node.remove_unack_msg(msg) 

    def append_unack_msg(self, msg):
        node = self.find_node(attr = "short_id", value =  msg["short id"])
        if not node is None:
            msg["timestamp"] = time.time()
            node.unack_msgs.append(msg)
        
    def append_polling_msg(self, msg):
        node = self.find_node(attr = "short_id", value =  msg["short id"])
        if not node is None:
            node.polling_queue.append(msg)
    
    def lost_packets(self, short_id):
        node = self.find_node(attr = "short_id", value = short_id)
        if not node is None:
            return node.lost_packets()
    
    def drop_inactive_nodes(self, timeout = 60*60*4, cb = None):
        i = 0
        while i < len(self.nodes):
            if self.nodes[i].last_sent_timestamp is None:
                i+=1
                continue
            if self.nodes[i].last_received_timestamp is None:
                i+=1
                continue
            if (time.time() - self.nodes[i].last_sent_timestamp > timeout) and (time.time() - self.nodes[i].last_received_timestamp > timeout):
                if not cb is None:
                    cb(self.nodes[i])
                del self.nodes[i]
            i+=1 