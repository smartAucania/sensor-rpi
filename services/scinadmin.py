import traceback
import requests
import json
import time

def mkdir_rec(dir_):
    import os
    path = ""
    for spath in dir_.split("/"):
        path += spath
        try:
            os.mkdir(path)
        except Exception as e:
            pass
        path += "/"
    del os

class ScinaboxAdmin:
    def __init__(self, host = None, port = 8080, email = None, password = None, timeout=5):
        self.host       = host
        self.port       = port
        self.email      = email
        self.password   = password
        self.timeout    = timeout
        self.authenticated  = False

    def login(self):
        headers = {'content-type': 'application/json'}
        try:
            r = requests.post(
                url     = "http://%s:%d/api/user/login"%(self.host, self.port),
                data    = json.dumps({"email": self.email, "pass": self.password}),
                headers = headers,
                timeout = self.timeout
            )
            print(r.json()["message"])
            user = r.json()
            self.user = user
            self.authenticated =  r.status_code == 200
            if self.authenticated:
                self.login_time  = time.time()
            return self.authenticated
        except:
            self.user = None
            traceback.print_exc()
            return False
    
    def login_if_needed(self):
        if (not self.authenticated) or (time.time() - self.login_time > 4*60*60):
            return self.login()
        return True
    
    def register(self, name, rol):
        headers = {'content-type': 'application/json'}
        try:
            r = requests.post(
                url     = "http://%s:%d/api/user/register"%(self.host, self.port),
                data    = json.dumps({"email": self.email, "pass": self.password, "name": name, "rol": rol}),
                headers = headers,
                timeout = self.timeout
            )
            print(r.json()["message"])
            return r.status_code == 200
        except:
            self.user = None
            traceback.print_exc()
            return False
    
    def add_device(self, device):
        headers = {'content-type': 'application/json'}
        try:
            r = requests.post(
                url     = "http://%s:%d/api/user/add-device"%(self.host, self.port),
                data    = json.dumps({"token": self.user["token"], "device": device}),
                headers = headers,
                timeout = self.timeout
            )  
            print(r.json()["message"])
            return r.status_code == 200
        except:
            traceback.print_exc()
            return False
    
    def remove_device(self, device):
        headers = {'content-type': 'application/json'}
        try:
            r = requests.post(
                url     = "http://%s:%d/api/user/remove-device"%(self.host, self.port),
                data    = json.dumps({"token": self.user["token"], "device": device}),
                headers = headers,
                timeout = self.timeout
            )  
            print(r.json()["message"])
            return r.status_code == 200
        except:
            traceback.print_exc()
            return False

    def get_devices(self):
        headers = {'content-type': 'application/json', "Authorization": self.user["token"]}
        try:
            r = requests.get(
                url     = "http://%s:%d/api/user/get-devices"%(self.host, self.port),
                headers = headers,
                timeout = self.timeout
            )  
            print(r.json()["message"])
            return r.json()["devices"] 
        except:
            traceback.print_exc()
            return False

    def get_device(self, device):
        headers = {'content-type': 'application/json', "Authorization": self.user["token"]}
        try:
            r = requests.get(
                url     = "http://%s:%d/api/user/get-device"%(self.host, self.port),
                params  = device,
                headers = headers,
                timeout = self.timeout
            )  
            print(r.json()["message"])
            return r.json()["device"]
        except:
            traceback.print_exc()
            return None
    
    def get_device_sw(self, device):
        headers = {"Authorization": self.user["token"]}
        try:
            r = requests.get(
                url     = "http://%s:%d/api/gateway/get-device-sw"%(self.host, self.port),
                params  = {"auth_key": device["auth_key"]},
                headers = headers,
                timeout = self.timeout
            )

            if not r.status_code == 200:
                raise Exception(r.json()["message"])
            device_name = device["name"].replace('-','')

            zip_filepath = '/tmp/%s.zip'%device_name
            with open(zip_filepath, 'w+b') as f:
                f.write(r.content)
            
            dst_filepath = '/tmp/devices/%s/'%device_name
            import os
            try:
                os.mkdir("/tmp/devices")
            except FileExistsError as e:
                print("Directorio %s ya existe"%"/tmp/devices")
            try:
                mkdir_rec(dst_filepath)
            except FileExistsError as e:
                print("Directorio %s ya existe"%dst_filepath)
            os.system("unzip -o %s -d %s"%(zip_filepath, dst_filepath))
            del os

            return dst_filepath
        except:
            traceback.print_exc()
        return None

    def add_group(self, group):
        headers = {'content-type': 'application/json'}
        try:
            r = requests.post(
                url     = "http://%s:%d/api/user/add-group"%(self.host, self.port),
                data    = json.dumps({"token": self.user["token"], "group": group}),
                headers = headers,
                timeout = self.timeout
            )  
            print(r.json()["message"])
            return r.status_code == 200
        except:
            traceback.print_exc()
            return False
        
    def remove_group(self, group):
        headers = {'content-type': 'application/json'}
        try:
            r = requests.post(
                url     = "http://%s:%d/api/user/remove-group"%(self.host, self.port),
                data    = json.dumps({"token": self.user["token"], "group": group}),
                headers = headers,
                timeout = self.timeout
            )  
            print(r.json()["message"])
            return r.status_code == 200
        except:
            traceback.print_exc()
            return False
    
    def set_last_packet_sent(self, gateway, auth_key, packet, timestamp):
        headers = {'content-type': 'application/json', 'Authorization': self.user["token"]}
        try:
            r = requests.post(
                url     = "http://%s:%d/api/gateway/device-last-packet-sent"%(self.host, self.port),
                data    = json.dumps({"gateway":{"name": gateway}, "device":{"auth_key": auth_key, "last_packet_sent": packet, "last_sent_timestamp": timestamp}}),
                headers = headers,
                timeout = self.timeout
            )  
            print(r.json()["message"])
            return r.status_code == 200
        except:
            traceback.print_exc()
            return False
    
    def set_last_packet_received(self, gateway, auth_key, packet, timestamp):
        headers = {'content-type': 'application/json', 'Authorization': self.user["token"]}
        try:
            r = requests.post(
                url     = "http://%s:%d/api/gateway/device-last-packet-received"%(self.host, self.port),
                data    = json.dumps({"gateway":{"name": gateway}, "device":{"auth_key": auth_key, "last_packet_received": packet, "last_received_timestamp": timestamp}}),
                headers = headers,
                timeout = self.timeout
            )  
            print(r.json()["message"])
            return r.status_code == 200
        except:
            traceback.print_exc()
            return False
    
    def add_publisher(self, device, publisher):
        headers = {'content-type': 'application/json'}
        try:
            r = requests.post(
                url     = "http://%s:%d/api/user/add-publisher"%(self.host, self.port),
                data    = json.dumps({"token": self.user["token"], "publisher": publisher, "device": device}),
                headers = headers,
                timeout = self.timeout
            )  
            print(r.json()["message"])
            return r.status_code == 200
        except:
            traceback.print_exc()
            return False
    
    def remove_publisher(self, device, publisher):
        headers = {'content-type': 'application/json'}
        try:
            r = requests.post(
                url     = "http://%s:%d/api/user/remove-publisher"%(self.host, self.port),
                data    = json.dumps({"token": self.user["token"], "publisher": publisher, "device": device}),
                headers = headers,
                timeout = self.timeout
            )  
            print(r.json()["message"])
            return r.status_code == 200
        except:
            traceback.print_exc()
            return False
    
    def add_gateway(self, gateway):
        headers = {'content-type': 'application/json'}
        try:
            r = requests.post(
                url     = "http://%s:%d/api/gateway/add"%(self.host, self.port),
                data    = json.dumps({"token": self.user["token"], "gateway": gateway}),
                headers = headers,
                timeout = self.timeout
            )  
            print(r.json()["message"])
            return r.status_code == 200
        except:
            traceback.print_exc()
            return False
    
    def assign_gateway(self, gateway, device):
        headers = {'content-type': 'application/json'}
        try:
            r = requests.post(
                url     = "http://%s:%d/api/gateway/add-device"%(self.host, self.port),
                data    = json.dumps({"token": self.user["token"], "gateway": gateway, "device": device}),
                headers = headers,
                timeout = self.timeout
            )  
            print(r.json()["message"])
            if r.status_code == 200:
                return r.json()
        except:
            traceback.print_exc()
        return False

    def get_gateway_devices(self, gateway):
        headers = {'content-type': 'application/json', "Authorization": self.user["token"]}
        try:
            r = requests.get(
                url     = "http://%s:%d/api/gateway/get-devices"%(self.host, self.port),
                params  = gateway,
                headers = headers,
                timeout = self.timeout
            )  
            print(r.json()["message"])
            if r.status_code == 200:
                return r.json()
        except:
            traceback.print_exc()
        return False
    
    def get_gateway(self, gateway):
        headers = {'content-type': 'application/json', "Authorization": self.user["token"]}
        try:
            r = requests.get(
                url     = "http://%s:%d/api/gateway/"%(self.host, self.port),
                params  = gateway,
                headers = headers,
                timeout = self.timeout
            )  
            print(r.json()["message"])
            if r.status_code == 200:
                return r.json()
        except:
            traceback.print_exc()
        return False

if __name__ == "__main__":
    host        = "146.83.206.131"
    port        = 443
    email       = "j.martinez09@ufromail.cl"
    password    = "scinabox"
    admin       = ScinaboxAdmin(host=host, port=port, email=email, password=password)
    devices     = [{
        "name":         "esp32lablora",
        "version":      "3.2.0",
        "description":  "esp32 para desarrollo LoRa del lab smartcity IoT",
        "auth_key":     "9xvK27fzvnaqptk53WS9zC7NCX2saXTW",
        "key":          "XrR7e2K2CKjPtbtN",
        "short_id":     1,
        "config":{
            "publishers": [{
                "timeout" : 30, 
                "method" : "http",
                "host" : "146.83.206.131", 
                "port" : 8080, 
                "token" : "8Rz8tjohGFbtz07d0Xan", 
                "format" : "simple" 
                }, 
                { 
                "timeout" : 30, 
                "method" : "http",
                "host" : "146.83.206.131", 
                "port" : 8080, 
                "token" : "qcwGo4sNMOx8Du4Kfxki", 
                "format" : "complete" 
            }],
            "attributes":{
                "uid":          "es2424:sensor",
                "hardware":     "esp32",
                "info-extra":   "{}",
                "latitud":      "-38.748653",    
                "longitud":     "-72.620791",
                "modelo":       "prototipo7",
                "nombre":       "esp32-lab-ufro",
                "sector":       "universidad",
                "tipo":         "sensor:interno",
                "version":      "3.1.0"
            }
        },
        "group": "mygroup1"
    },
    {
        "name":         "esp32lablora2",
        "version":      "3.2.0",
        "description":  "esp32 para desarrollo LoRa del lab smartcity IoT",
        "auth_key":     "vDBcwEvJK4Xegg8MUbgDDSU42RwUVxX8",
        "key":          "bUtGR7ZrypcQ95dk",
        "short_id":     2,
        "config":{
            "publishers": [{
                "timeout" : 30, 
                "method" : "http",
                "host" : "146.83.206.131", 
                "port" : 8080, 
                "token" : "FGYA2tQZPPxBoGNxdVCd", 
                "format" : "simple" 
                }],
            "attributes":{
                "uid":          "es6435:sensor",
                "hardware":     "esp32",
                "info-extra":   "{}",
                "latitud":      "-38.748653",
                "longitud":     "-72.620791",
                "modelo":       "prototipo7",
                "nombre":       "esp32-lab-ufro2",
                "sector":       "universidad",
                "tipo":         "sensor:interno",
                "version":      "3.1.0"
            }
        },
        "group": "mygroup1"
    },
    {
        "name":         "esp32lablora3",
        "version":      "3.2.0",
        "description":  "esp32 para desarrollo LoRa del lab smartcity IoT",
        "auth_key":     "htmu3bmG9wfsj7mZ2hGySkaMrUMCGzpX",
        "key":          "uv52W7Y7Jn9NcJFu",
        "short_id":     3,
        "config":{
            "publishers": [{
                "timeout" : 30, 
                "method" : "http",
                "host" : "146.83.206.131", 
                "port" : 8080, 
                "token" : "Vl0Vk794WynRmEHZWLTa", 
                "format" : "simple" 
                }],
            "attributes":{
                "uid":          "es1235:sensor",
                "hardware":     "esp32",
                "info-extra":   "{}",
                "latitud":      "-38.748653",
                "longitud":     "-72.620791",
                "modelo":       "prototipo7",
                "nombre":       "esp32-lab-ufro3",
                "sector":       "universidad",
                "tipo":         "sensor:interno",
                "version":      "3.1.0"
            }
        },
        "group": "mygroup1"
    }]
    gateway     = {

        "name":         "gateway-lab",
        "version":      "3.2.0",
        "description":  "gateway para pruebas de laboratorio",
        "group":        "mygroup1",
        "pan_id":       0x0ABB,
        "battery":{
            "sensor": "ina219",
            "field": "ina219_voltage"
        },
        "config":{
            "git":{
                "update_period": 60*10,
            },
            "publishers": [
                {
                    "host"          : "remo.ceisufro.cl",
                    "token"         : "F2PDInZzhrp70pp0QjfC",
                    "format"        : "simple",
                    "port"          : 8080,
                    "timeout"       : 30,
                    "method"        : "http"
                },
                {
                    "host"          : "146.83.206.131",
                    "token"         : "6DWc2bgTOlxcM7t1sB7b",
                    "format"        : "simple",
                    "port"          : 8080,
                    "timeout"       : 30,
                    "method"        : "http"
                }
            ],
            "attributes":{
                "uid":            "rtc0008:sensor",
                "hardware":       "rpi3b-caja8",
                "info-extra":     "prototipo-7",
                "latitud":        "-38.7129017",
                "longitud":       "-72.6571608",
                "modelo":         "el-carmen",
                "nombre":         "raspberry-pi-3-model-b",
                "sector":         "v0.0.7",
                "tipo":           "sensor:externo",
                "version":        "{}"
            },
        }
    }
    old_publishers = [{
        "timeout" : 30, 
        "method" : "http",
        "host" : "146.83.206.131", 
        "port" : 8080, 
        "token" : "8Rz8tjohGFbtz07d0Xan", 
        "format" : "simple" 
        },
        { 
        "timeout" : 30, 
        "method" : "http",
        "host" : "146.83.206.131", 
        "port" : 8080, 
        "token" : "qcwGo4sNMOx8Du4Kfxki", 
        "format" : "complete" 
    }]

    new_publishers = [{
            "host":     "146.83.206.131",
            "port":     8080,
            "token":    "8Rz8tjohGFbtz07d0Xan",
            "format":   "simple",
            "method":   "http"
        },
        {
            "host":     "146.83.206.131",
            "port":     8080,
            "token":    "qcwGo4sNMOx8Du4Kfxki",
            "format":   "complete",
            "method":   "http"
    }]

    admin.register("jose", "ADMIN")
    if admin.login():
        print("Logged in !")
        print("Token: %s"%admin.user["token"])
        admin.add_group({"name": "mygroup1"})
        admin.add_gateway(gateway)
        for device in devices:
            if admin.add_device(device):
                print("Dispositivo agregado")
                admin.assign_gateway(gateway["name"], device)
        devices = admin.get_devices()
        print(devices)

        #Remueve publicadores
        #NO ESTA FUNCIONANDO
        # for pub in old_publishers:
        #     print(pub)
        #     admin.remove_publisher(device, pub)

        #Añade publicadores
        # for pub in new_publishers:
        #     admin.add_publisher(device, pub)
        # admin.remove_device(devices[0])

        # 
        # admin.add_gateway(gateway)
        # device["short_id"] = 0x01
        # admin.get_device_sw(device)
        # admin.assign_gateway(gateway["name"], device)
        # print(admin.get_gateway_devices({"name": gateway["name"]}))
        # print("GATEWAY")
        # print(admin.get_gateway({"name": gateway["name"]}))
        # print(admin.get_device({"auth_key": device["auth_key"]}))
    else:
        print("Cannot log in :c")