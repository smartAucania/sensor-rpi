#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file publisher.py
# @brief Se define la clase interfaz para publicar los datos en thingsboard, además de la lógica de la publicación.
# @see config/config.py
# @see lib/thingsboard.py

import sys
sys.path.insert(0,'..')
import time, statistics, traceback, json
from services import datalogger, mongogger
from config import config

## @class ThingsboardPublisher
class ThingsboardPublisher:
    ## @brief Constructor de la clase.
    # @param[in] tableName Nombre de la tabla temporal SQLite3 donde se almacenarán los datos en caso de no conexión.
    # @param[in] thingsBoard Manejador de peticiones http a plataforma thingsboard.
    # @param[in] format_ Formato de la telemetría para este publicador en particular. Puede ser "simple" o "completo".
    # @param[in] logger Logger.
    def __init__(self, tableName = "tableName", thingsBoard = None, format_ = "simple", logger = None):
        ## Manejador de peticiones http a plataforma thingsboard.
        self.thingsBoard    = thingsBoard   
        ## Nombre de la tabla sqlite3 para almacenar en caso de no conexión.
        self.tableName      = tableName     
        ## Tipo de formato en el cual enviar la telemetria.
        self.format         = format_       
        ## Logger de mensajes del sistema.
        self.logger         = logger        
        # datalogger.createTable(self.tableName)
        ## Manejador de base de datos temporal json para almacenar telemetrías no enviadas
        self.db             = mongogger.DB(''.join(self.thingsBoard.host.split('.')), tableName)
    
    ## @brief Publica una telemetría, seguido de los atributos del dispositivo.
    # @param[in] sensordata Telemetría asociada a una medición.
    # @param[in] attributes Atributos del dispositivo.
    # @param[out] published Bandera que indica si se logró publicar la telemetría.
    def publish(self, sensordata, attributes):
        published = False
        try:
            if not self.logger is None:
                self.logger.debug("Publicando mediciones del dispositivo %s"%(self.tableName))

            #Inserta los datos en thingsboard
            if(self.publishTelemetry(sensordata)):
                self.publishAttributes(attributes)

                #Marca telemetria como publicada
                published = True

                # #Rescata datos guardados en bd y los manda
                # publishedDeq = datalogger.getTable(self.tableName, lambda tel: self.publishTelemetry(tel, mode = "offline") and self.publishAttributes(attributes), logger=self.logger)
                # if not( None in publishedDeq or False in publishedDeq ):
                #     #Vacia tabla temporal
                #     datalogger.dropAndCreateTable(self.tableName)
        except Exception as e:
            #Imprime traceback
            traceback.print_exc()
            if not self.logger is None:
                #Almacena traceback en log
                self.logger.exception(e)
        return published
    
    ## @brief Publica una telemetría.
    # @param[in] telemetry Telemetría.
    # @param[in] attempts Cantidad máxima de reintentos de publicación.
    # @param[out] published Bandera que indica si logró publicar la telemetría.
    def publishTelemetry(self, telemetry, mode="online", attempts = 3):
        #Inicializa bandera
        published = False

        if self.format == "complete":
            info                    = json.loads(telemetry["info-extra"])
            info["modo"]            = mode
            telemetry["info-extra"] = json.dumps(info)

        #Loop de reintentos de transmisión de telemetrías
        for i in range(attempts):
            #Intenta publicar telemtria
            if(self.thingsBoard.publishTelemetry(telemetry)):
                #Actualiza bandera
                published = True

                #Finaliza loop de reintentos de transmisión de telemetrías
                break
            #Delay hasta el siguiente reintento
            time.sleep(0.1)
        if not published:
            if not self.logger is None:
                self.logger.warning("La telemetría del dispositivo %s en el sitio %s no ha podido ser publicada"%(self.tableName, self.thingsBoard.host))
                self.logger.debug("La telemetría del dispositivo %s en el sitio %s es almacenada en tabla asociada"%(self.tableName, self.thingsBoard.host))
            #Almacena datos del sensor en tabla
            self.db.add(telemetry)
            # datalogger.insertDict(self.tableName, telemetry)
        else:
            self.logger.info("Telemetría del dispositivo %s en el sitio %s es enviada con exito"%(self.tableName, self.thingsBoard.host))
        #Indica si la publicación de telemetría ha sido enviada exitosamente
        return published
    
    ## @brief Publica atributos del dispositivo.
    # @param[in] attr Atributos del dispositivo.
    # @param[in] attempts Cantidad de reintentos de publicación de atributos.
    # @param[out] published Bandera que indica si se lograron publicar los atributos.
    def publishAttributes(self, attr, attempts = 3):
        #Inicializa bandera
        published = False

        #Loop de reintentos de transmisión de atributos del dispositivo
        for i in range(attempts):
            #Intenta publicar atributos
            if(self.thingsBoard.publishAttributes(attr)):
                #Actualiza bandera
                published = True

                #Finaliza loop de reintentos de transmisión de telemetrías
                break
            #Delay hasta el siguiente reintento
            time.sleep(0.1)
        #Indica si la publicación de telemetría ha sido enviada exitosamente
        return published

    ## @brief Publica telemetrías no enviadas.
    # @param[in] attr Atributos del dispositivo.
    # @param[out] published Bandera que indica si se lograron publicar las telemetrías sin enviar.
    def publishUnsent(self, attr):
        if self.db.size()>0:
            if not self.logger is None:
                self.logger.debug("Publicador %s comienza a publicar %d telemetrías no enviadas al servidor %s usando key %s"%(
                        self.tableName, self.db.size(), self.thingsBoard.host, self.thingsBoard.token
                    ))
            while True:
                try:
                    if self.db.size()==0:
                        break
                    tel = self.db.pop()
                    if not self.publish(tel, attr):
                        self.db.add(tel)
                except Exception as e:
                    traceback.print_exc()
                    if not self.logger is None:
                        self.logger.exception(e)
                    break
            if not self.logger is None:
                self.logger.debug("Publicador %s termina de publicar telemetrías no enviadas al servidor %s usando key %s. Telemetrías restantes: %d"%(
                    self.tableName, self.thingsBoard.host, self.thingsBoard.token, self.db.size()
                ))
    
    def __str__(self):
        return "Host: %s. Port: %d. Thingsboard key: %s. Format: %s"%(self.thingsBoard.host, self.thingsBoard.port, self.thingsBoard.token, self.format)
## @brief Loop de publicación de telemetrías y atributos. Recibe telemetrías del proceso principal y envía actualizaciones de estado del dispositivo al proceso respectivo. 
# De no haber conexión con el servidor, utiliza tablas temporales para almacenar las telemetrías y enviarlas al momento de recuperar la conexión. 
# Además, utiliza una tabla para tener un respaldo de todas las telemetrías.
# @param[in] publishers Publicadores mediante los cuales envíar las telemetrías.
# @param[in] mqueue Cola multiproceso por la cual recibe telemetrías.
# @param[in] stateQueue Cola multiproceso por la cual envía actualizaciones de estado del dispositivo.
def publishingLoop(publishers, mqueue, stateQueue):

    #crea tabla de datos historica 
    datalogger.createTable('datalogger_history')

    #Carga atributos
    attr = config.getDeviceAttributes()
    while True:
        time.sleep(1)
        #Espera a recibir telemetria para enviarla al servidor
        while(mqueue.empty()):
            time.sleep(1) 
        
        #Recibe medición
        measurement = mqueue.get()

        if len(publishers)>0:
            publishers[0].logger.debug("Medición %s recibida. Se almacenan los datos en tabla historica."%measurement["measurement"]["name"])

            #Almacena medición en base de datos de respaldo
            datalogger.insertDict('datalogger_history', measurement)

            #Inicializa flag de publicación
            publishing = True

            #publica la medición
            for pub in publishers:
                if not pub is None:
                    try:
                        if not pub.publish(measurement[pub.format], attr):
                            publishing = False
                    except:
                        traceback.print_exc()
                        pub.logger.exception()
                        publishing = False
                if publishing:
                    try:
                        pub.publishUnsent(attr)
                    except:
                        traceback.print_exc()
                        pub.logger.exception("Error al publicar telemetrias almacenadas anteriormente")
            #Maneja led
            if not publishing:
                #Se indica que no está publicando
                stateQueue.put({
                    "attribute": "publishing",
                    "state": False,
                    "measurement": measurement["measurement"]["name"]
                })
            else:
                #Se indica que está publicando
                stateQueue.put({
                    "attribute": "publishing",
                    "state": True,
                    "measurement": measurement["measurement"]["name"]
                })
        else:
            print("Proceso de publicación: No se han encontrado publicadores")
            time.sleep(1)
