#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file sdmonitor.py
# @brief Se define la clase interfaz para monitorear el espacio en tarjeta SD usando esquema multihilo.
# @see config/config.py
# @see services/sensor.py

import sys
sys.path.insert(0,'..')
from services.sensor import Sensor
import threading, time, shutil, statistics
import numpy as np
from collections import deque

## @class SDMonitor
# @code{.py}
# from config import config
# import statistics, numpy as np
# def onRead(sensorObj):
#     #Obtiene marca de tiempo en unix y timestamp str
#     unix 		= np.datetime64('now').astype('float64')*1000 #Thingsboard acepta tiempo unix en milisegundos
#     timestamp 	= str(np.datetime64('now')).replace('T', ' ')
#     print(timestamp)
#
#     #Carga configuraciones del sensor
#     sensor = config.getSensor(sensorObj.name)
#
#     if not sensor is None:			
#         for measurement in sensor["measurements"]:
#             #Obtiene valor de la medición en base a mediana
#             buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))
#
#             if len(buff)>0:
#                 value 										= statistics.median(buff)
#
#                 #Manda mensaje de debugueo
#                 print("Data obtenida de %s. %s: %s"%(sensor["name"], measurement["name"], repr(value)))
#
# #Obtiene configuración para el monitoreo del espacio en disco SDMonitor
# sdmonitorConfig = config.getSensor("sdmonitor")
# if not sdmonitorConfig is None:
#     from sdmonitor import SDMonitor
#     #Crea hilo para lectura del sensor de temperatura de la rpi
#     sdmonitor = SDMonitor(buffLen=sdmonitorConfig["buffLen"], period = sdmonitorConfig["period"], on_read = onRead)
#     #Comienza lectura del sensor
#     sdmonitor.start()
# @endcode
class SDMonitor(Sensor):

    ## Nombre del sensor.
    name    = "sdmonitor"

    ## @brief Constructor de la clase.
    # @param[in] buffLen Tamaño de los buffers de mediciones.
    # @param[in] period Período de tiempo entre mediciones.
    # @param[in] on_read Callback a llamar al llenarse los buffers.
    # @param[in] maxTimeOffset Tiempo máximo entre que termina el periodo de mediciones y comienza el siguiente.
    # @param[in] kwargs Argumentos extra para llamar al callback.
    def __init__(self, buffLen = 11, period = 10, on_read = None, maxTimeOffset = 15, **kwargs):
        ## Tamaño del buffer de mediciones.
        self.buffLen        = buffLen                   
        ## Período entre lecturas.
        self.period         = period                    
        ## Buffer de mediciones: memoria total.
        self.totalSizeBuff  = None                      
        ## Buffer de mediciones: memoria libre.
        self.freeSizeBuff   = None                      
        ## Buffer de mediciones: memoria usada.
        self.usedSizeBuff   = None                      
        ## Argumentos extra para el callback.
        self.kwargs         = kwargs                                         
        ## Callback a llamar cuando se hayan completado los buffers.
        self.on_read        = on_read                   
        ## Cantidad máxima de tiempo extra que puede tardar en tomar todas las muestras.
        self.maxTimeOffset 	= maxTimeOffset

        Sensor.__init__(self, buffLen = buffLen, on_read = on_read, period = period, maxTimeOffset = maxTimeOffset, **kwargs)
    
    def appendMeasure(self):
        #Almacena muestra de temperatura
        totalSize, usedSize, freeSize = self.read()
        self.totalSizeBuff.append(totalSize)
        self.freeSizeBuff.append(freeSize)
        self.usedSizeBuff.append(usedSize)

    def initBuffers(self):
        #Inicialización de buffer de mediciones
        self.totalSizeBuff  = deque([], maxlen = self.buffLen)
        self.freeSizeBuff   = deque([], maxlen = self.buffLen)
        self.usedSizeBuff   = deque([], maxlen = self.buffLen)
    
    ## @brief Realiza lectura de espacio en disco.
    # @param[out] total Espacio total en el disco en GB.
    # @param[out] used Espacio usado en el disco en GB.
    # @param[out] free Espacio libre en el disco en GB.
    def read(self):
        #Comando para obtener la temperatura en una RPI
        total, used, free = shutil.disk_usage("/root")
        total   = total/2**30
        used    = used/2**30
        free    = free/2**30
        return total, used, free

if __name__ == '__main__':
    from config import config
    import statistics, numpy as np
    def onRead(sensorObj):
        #Obtiene marca de tiempo en unix y timestamp str
        unix 		= np.datetime64('now').astype('float64')*1000 #Thingsboard acepta tiempo unix en milisegundos
        timestamp 	= str(np.datetime64('now')).replace('T', ' ')
        print(timestamp)

        #Carga configuraciones del sensor
        sensor = config.getSensor(sensorObj.name)

        if not sensor is None:			
            for measurement in sensor["measurements"]:
                #Obtiene valor de la medición en base a mediana
                buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))

                if len(buff)>0:
                    value 										= statistics.median(buff)

                    #Manda mensaje de debugueo
                    print("Data obtenida de %s. %s: %s"%(sensor["name"], measurement["name"], repr(value)))

    #Obtiene configuración para el monitoreo del espacio en disco SDMonitor
    sdmonitorConfig = config.getSensor("sdmonitor")
    if not sdmonitorConfig is None:
        # from sdmonitor import SDMonitor
        #Crea hilo para lectura del sensor de temperatura de la rpi
        sdmonitor = SDMonitor(buffLen=sdmonitorConfig["buffLen"], period = sdmonitorConfig["period"], on_read = onRead)
        #Comienza lectura del sensor
        sdmonitor.start()