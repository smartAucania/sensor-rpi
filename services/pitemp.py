#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file pitemp.py
# @brief Contiene clase con funciones para monitoreo del sensor de temperatura del BCM2387 usando esquema multihilo.
# @see config/config.py
# @see services/sensor.py

import sys
sys.path.insert(0,'..')
import threading, os, time, statistics, traceback
from services.sensor import Sensor
import numpy as np
from collections import deque

## @class PiTempMonitor
# @code{.py}
# from config import config
# import statistics, numpy as np
# def onRead(sensorObj):
#     #Obtiene marca de tiempo en unix y timestamp str
#     unix 		= np.datetime64('now').astype('float64')*1000 #Thingsboard acepta tiempo unix en milisegundos
#     timestamp 	= str(np.datetime64('now')).replace('T', ' ')
#     print(timestamp)
#
#     #Carga configuraciones del sensor
#     sensor = config.getSensor(sensorObj.name)
#
#     if not sensor is None:			
#         for measurement in sensor["measurements"]:
#             #Obtiene valor de la medición en base a mediana
#             buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))
#
#             if len(buff)>0:
#                 value 										= statistics.median(buff)
#
#                 #Manda mensaje de debugueo
#                 print("Data obtenida de %s. %s: %s"%(sensor["name"], measurement["name"], repr(value)))
#
# #Obtiene configuración para el sensor de temperatura de la bcm2837
# bcmConfig = config.getSensor("bcm2837")
# if not bcmConfig is None:
#     from pitemp import PiTempMonitor
#     #Crea hilo para lectura del sensor de temperatura de la rpi
#     pitemp = PiTempMonitor(buffLen=bcmConfig["buffLen"], period = bcmConfig["period"], on_read = onRead)
#     #Comienza lectura del sensor
#     pitemp.start()
# @endcode
class PiTempMonitor(Sensor):
    name = "bcm2837"
    def __init__(self, buffLen=11, period=10, on_read = None, maxTimeOffset = 15, **kwargs):
        ## Tamaño del buffer de mediciones.
        self.buffLen        = buffLen                   
        ## Periodo entre lecturas.
        self.period         = period                         
        ## Callback llamado al completar buffer.
        self.on_read        = on_read                   
        ## Argumentos extra para el callback.
        self.kwargs         = kwargs                   
        ## Cantidad máxima de tiempo extra que puede tardar en tomar todas las muestras.
        self.maxTimeOffset 	= maxTimeOffset             

        Sensor.__init__(self, buffLen = buffLen, on_read = on_read, period = period, maxTimeOffset = maxTimeOffset, **kwargs)
    
    def appendMeasure(self):
        self.tempBuff.append(self.read())
    
    def initBuffers(self):
        #Inicialización de buffer de mediciones
        ## Buffer de mediciones.
        self.tempBuff   = deque([], maxlen=self.buffLen)
    
    def readFake(self):
        self.tempBuff.append(np.random.normal(50, 3, 1)[0])
    
    ## @brief Realiza una medición de temperatura.
    #
    # @params[out] temp Temperatura del chip bcm2387.
    def read(self):
        #Comando para obtener la temperatura en una RPI
        temp = os.popen("vcgencmd measure_temp").readline()
        return float(temp.replace("temp=","").replace("'C\n", ""))

if __name__ == '__main__':
    from config import config
    import statistics, numpy as np
    def onRead(sensorObj):
        #Obtiene marca de tiempo en unix y timestamp str
        unix 		= np.datetime64('now').astype('float64')*1000 #Thingsboard acepta tiempo unix en milisegundos
        timestamp 	= str(np.datetime64('now')).replace('T', ' ')
        print(timestamp)

        #Carga configuraciones del sensor
        sensor = config.getSensor(sensorObj.name)

        if not sensor is None:			
            for measurement in sensor["measurements"]:
                #Obtiene valor de la medición en base a mediana
                buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))

                if len(buff)>0:
                    value 										= statistics.median(buff)

                    #Manda mensaje de debugueo
                    print("Data obtenida de %s. %s: %s"%(sensor["name"], measurement["name"], repr(value)))

    #Obtiene configuración para el sensor de temperatura de la bcm2837
    bcmConfig = config.getSensor("bcm2837")
    if not bcmConfig is None:
        # from pitemp import PiTempMonitor
        #Crea hilo para lectura del sensor de temperatura de la rpi
        pitemp = PiTempMonitor(buffLen=bcmConfig["buffLen"], period = bcmConfig["period"], on_read = onRead)
        #Comienza lectura del sensor
        pitemp.start()