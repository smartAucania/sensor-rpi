#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file datalogger.py
# @brief Funciones para la interacción con base de datos local SQLite3.
# @see config/config.py

import json, logging, sqlite3, logging, traceback
from logging.handlers import RotatingFileHandler
from collections import deque

## @brief Crea una tabla.
# @param[in] tableName Nombre de la tabla.
def createTable(tableName):
    #Se conecta a la base de datos
    conn = sqlite3.connect('sensor.db')
    try:
        #Crea cursor
        c = conn.cursor()

        # Crea tabla si es que no existe ya
        c.execute('''CREATE TABLE IF NOT EXISTS %s
                    (data text)'''%(tableName))

        #Confirma cambios y cierra la db
        conn.commit()
    except:
        traceback.print_exc()
    conn.close()

## @brief Crea tabla llamada datalogger.
# @param[in] logger Logger.
def initLocalDatabase(logger = None):
    if not logger is None:
        logger.debug("Inicializando base de datos")

    createTable('datalogger')

## @brief Crea tabla llamada datalogger_history. Usada para acumular datos históricos.
# @param[in] logger Logger.
def initLocalHistoryDatabase(logger = None):
    if not logger is None:
        logger.debug("Inicializando base de datos historica")

    createTable('datalogger_history')

## @brief Inserta diccionario en una tabla convirtiendolo en un JSON.
# @param[in] tableName Nombre de la tabla.
# @param[in] data Diccionario a insertar.
def insertDict(tableName, data):
    conn = sqlite3.connect('sensor.db')
    try:
        #Crea cursor
        c = conn.cursor()

        #Construye consulta de inserción de datos
        insert = "insert into %s values (?)"%(tableName)

        #Ejecuta comando de inserción
        c.execute(insert, [json.dumps(data)])

        #Confirma cambios y cierra la conexión
        conn.commit()
    except:
        traceback.print_exc()
    conn.close()

## @brief Inserta diccionario en tabla datalogger como un JSON.
# @param[in] sensordata Diccionario con data.
# @param[in] logger Logger.
def saveSensorData(sensordata, logger = None):
	#Se conecta a la db
    if not logger is None:
        logger.debug("Se almacena la data en la db: %s"%(repr(sensordata)))

    insertDict('datalogger', sensordata)

## @brief Inserta diccionario en tabla datalogger_history como un JSON.
# @param[in] sensordata Diccionario con data.
def saveDataHistory(sensordata):
    #Se conecta a la db
    if not logger is None:
        logger.debug("Se almacena la data en la db: %s"%(repr(sensordata)))

    insertDict('datalogger_history', sensordata)

## @brief Elimina una tabla.
# @param[in] tableName Nombre de la tabla.
# @param[in] logger Logger.
def dropTable(tableName, logger = None):
    #Se conecta a la base de datos
    conn = sqlite3.connect('sensor.db')

    #Crea cursor
    c = conn.cursor()

    if not logger is None:
        logger.debug("Vacia tabla %s de la base de datos"%(tableName))

    #Elimina tabla y la vuelve a crear
    c.execute('drop table if exists %s'%(tableName))

    #Cierra la conexión
    conn.close()

## @brief Elimina una tabla y la vuelve a crear.
# @param[in] tableName Nombre de la tabla.
def dropAndCreateTable(tableName):
    dropTable(tableName)
    createTable(tableName)

## @brief Mapea una función a lo largo de una tabla.
# @param[in] tableName Nombre de la tabla.
# @param[in] on_data Callback ejecutado por cada fila, dando como argumento la misma fila.
# @param[in] logger Logger.
# @param[out] deq Deque con los retornos de todas las llamadas al Callback on_data.
def getTable(tableName, on_data, logger = None):
    if not logger is None:
        logger.debug("Obtiene datos de la base de datos local")

    #Se conecta a la base de datos
    conn = sqlite3.connect('sensor.db')

    #Crea cursor
    c = conn.cursor()

    #Crea cola de datos
    deq = deque([])

    #Consulta tabla de data de sensores
    for row in c.execute('select * from %s'%(tableName)):

        #Ejecuta funcion argumento por cada muestreo de los sensores
        deq.append(on_data(json.loads(row[0])))
    conn.close()

    return deq