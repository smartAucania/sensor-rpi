#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file ina219_.py
# @brief Contiene clase con funciones para monitoreo del sensor ina219 usando esquema multihilo en base a una librería de github.
# @see https://github.com/chrisb2/pi_ina219/
# @see config/config.py
# @see services/sensor.py

import sys
sys.path.insert(0,'..')
import threading, statistics, time, pigpio, traceback
from services.sensor import SensorI2C
from ina219 import INA219
from ina219 import DeviceRangeError
from collections import deque
import numpy as np

## @class INA219_
# @code{.py}
# from config import config
# import statistics, numpy as np
# def onRead(sensorObj):
#     #Obtiene marca de tiempo en unix y timestamp str
#     unix 		= np.datetime64('now').astype('float64')*1000 #Thingsboard acepta tiempo unix en milisegundos
#     timestamp 	= str(np.datetime64('now')).replace('T', ' ')
#     print(timestamp)
#
#     #Carga configuraciones del sensor
#     sensor = config.getSensor(sensorObj.name)
#
#     if not sensor is None:			
#         for measurement in sensor["measurements"]:
#             #Obtiene valor de la medición en base a mediana
#             buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))
#
#             if len(buff)>0:
#                 value 										= statistics.median(buff)
#
#                 #Manda mensaje de debugueo
#                 print("Data obtenida de %s. %s: %s"%(sensor["name"], measurement["name"], repr(value)))
# #Obtiene configuración para el sensor am2315
# inaConfig = config.getSensor("ina219")
# if not inaConfig is None:
#     from services.ina219_ import INA219_
#     #Crea hilo para lectura del sensor am2315
#     ina219 = INA219_(buffLen = inaConfig["buffLen"], pin=inaConfig["enable_gpio"], period = inaConfig["period"], sem = None, on_read = onRead)
#     #Comienza lectura del sensor
#     ina219.start()
# @endcode
class INA219_(SensorI2C):
    ## Nombre del sensor.
    name                = "ina219"
    ## Resistencia shunt en ohms del sensor.
    SHUNT_OHMS          = 0.1
    ## Cantidad máxima de amperes que se esperan que pasen por el sensor.
    MAX_EXPECTED_AMPS   = 1.5

    ## @brief Constructor de la clase.
    # @param[in] buffLen Tamaño del buffer de mediciones.\n
    # @param[in] period Periodo de tiempo entre mediciones.\n
    # @param[in] sem Semáforo i2c para evitar conflictos.\n
    # @param[in] pin Pin destinado a encender y apagar el sensor transistorizado.\n
    # @param[in] on_read Callback a llamar cada vez que se llenan los buffers de mediciones.\n
    # @param[in] nattempts Cantidad de reintentos de lectura por medición.\n
    # @param[in] maxTimeOffset Máximo tiempo de offset entre que termina un período de mediciones y comienza el siguiente.\n
    # @param[in] kwargs Diccionario con parámetros de entrada para función callback.
    def __init__(self, buffLen = 11, pin=4, period = 60, on_read = None, nattempts = 3, maxTimeOffset = 15,  **kwargs):
        ## Tamaño del buffer de mediciones
        self.buffLen        = buffLen                       
        ## Periodo de tiempo entre mediciones
        self.period         = period                             
        ## Objeto pigpio acceso a gpio
        self.pi             = pigpio.pi()                   
        ## Pin destinado a encender y apagar el sensor transistorizado
        self.pin            = pin                           
        ## Callback a llamar cada vez que se llenan los buffers de mediciones
        self.on_read        = on_read                       
        ## Argumentos extra a pasar a la función callback
        self.kwargs         = kwargs                        
        ## Cantidad de intentos de lectura por medición
        self.nattempts      = nattempts                                          
        ## Cantidad máxima de tiempo extra que puede tardar en tomar todas las muestras
        self.maxTimeOffset 	= maxTimeOffset                 

        #Inicializa hilo
        SensorI2C.__init__(self, pin = pin, buffLen = buffLen, on_read = on_read, period = period, maxTimeOffset = maxTimeOffset, **kwargs)
    
    def appendMeasure(self):
        self.read()
        # self.readFake()
    
    def initBuffers(self):
        ## Buffer de mediciones de voltaje en V.
        self.voltBuff   = deque([], maxlen=self.buffLen)
        ## Buffer de mediciones de corriente en mA.
        self.currBuff   = deque([], maxlen=self.buffLen)
        ## Buffer de mediciones de potencia en mW.
        self.powBuff    = deque([], maxlen=self.buffLen)
    
    def readFake(self):
        self.voltBuff.append(np.random.normal(3.7, 0.4, 1)[0])
        self.currBuff.append(np.random.normal(700, 200, 1)[0])
        self.powBuff.append(np.random.normal(1900, 100, 1)[0])

    def read(self):
        #Vuelve a crear objeto. Al parecer debe hacerse luego de encenderlo.
        for i in range(3):
            try:
                ## Objeto INA219 de la librería Adafruit.
                self.ina219         = INA219(INA219_.SHUNT_OHMS, INA219_.MAX_EXPECTED_AMPS)
                self.ina219.configure(bus_adc = INA219.ADC_64SAMP, shunt_adc = INA219.ADC_64SAMP)
                break
            except Exception as e:
                traceback.print_exc()
                time.sleep(0.1)

        #Inicializa cantidad de intentos realizados
        attempt = 1

        while attempt <= self.nattempts:
            try:
                #Realiza lectura de corriente, voltaje y potencia
                self.voltBuff.append(self.ina219.voltage())
                self.currBuff.append(self.ina219.current())
                self.powBuff.append(self.ina219.power())
                return True
            except:
                traceback.print_exc()
                print("Error al leer sensor ina219")

                #Actualiza contador de intentos
                attempt += 1

                #Duerme un segundo hasta el siguiente intento
                time.sleep(1)
        return False
    
    # ## @brief Rutina del sensor
    # def run(self):
    #     #Activa flag para saber que el hilo se está ejecutando
    #     self.startSensor()

    #     while True:
    #         #Marca tiempo de inicio
    #         st = time.time()
    #         while True and not self.isStopping():
    #             #Inicializa buffers
    #             ## Buffer de mediciones de voltaje en V.
    #             self.voltBuff   = deque([], maxlen=self.buffLen)
    #             ## Buffer de mediciones de corriente en mA.
    #             self.currBuff   = deque([], maxlen=self.buffLen)
    #             ## Buffer de mediciones de potencia en mW.
    #             self.powBuff    = deque([], maxlen=self.buffLen)
                
    #             for i in range(self.buffLen):

    #                 if time.time()-st > self.period*self.buffLen + self.maxTimeOffset:
    #                     #Salta timeout por finalización de tiempo estipulado para muestreo
    #                     break

    #                 #Duerme hasta que sea necesario muestrear
    #                 if time.time()-st <self.period*(i+1):
    #                     time.sleep(self.period*(i+1)-(time.time()-st))
                    
    #                 self.startCritical()

    #                 #Pide recurso I2C
    #                 self.startI2CCritical()

    #                 #Enciende sensor i2c
    #                 val = self.pi.read(self.pin)
    #                 self.pi.write(self.pin, 1)

    #                 #Vuelve a crear objeto. Al parecer debe hacerse luego de encenderlo.
    #                 for i in range(3):
    #                     try:
    #                         ## Objeto INA219 de la librería Adafruit.
    #                         self.ina219         = INA219(INA219_.SHUNT_OHMS, INA219_.MAX_EXPECTED_AMPS)
    #                         self.ina219.configure(bus_adc = INA219.ADC_64SAMP, shunt_adc = INA219.ADC_64SAMP)
    #                         break
    #                     except Exception as e:
    #                         traceback.print_exc()
    #                         time.sleep(0.1)

    #                 #Inicializa cantidad de intentos realizados
    #                 attempt = 1

    #                 while attempt <= self.nattempts:
    #                     try:
    #                         #Realiza lectura de corriente, voltaje y potencia
    #                         self.voltBuff.append(self.ina219.voltage())
    #                         self.currBuff.append(self.ina219.current())
    #                         self.powBuff.append(self.ina219.power())
    #                         break
    #                     except:
    #                         traceback.print_exc()
    #                         print("Error al leer sensor ina219")

    #                         #Actualiza contador de intentos
    #                         attempt += 1

    #                         #Duerme un segundo hasta el siguiente intento
    #                         time.sleep(1)
    #                 #Vuelve voltaje sensores i2c a su estado anterior
    #                 self.pi.write(self.pin, val)

    #                 #Libera recurso I2C
    #                 self.stopI2CCritical()

    #                 #Libera control del hilo
    #                 self.stopCritical()

    #                 #Muestra por pantalla los valores obbtenidos
    #                 if len(self.voltBuff)>0 and len(self.currBuff)>0 and len(self.powBuff)>0:
    #                     print("INA219 %s: Voltaje: %.2f. Corriente: %.2f. Potencia: %.2f"%(str(np.datetime64('now')).replace('T', ' '), self.voltBuff[-1], self.currBuff[-1], self.powBuff[-1]))
                
    #             #Adquiere control del hilo
    #             self.startCritical()

    #             #Actualiza última medición de corriente
    #             if len(self.currBuff)>0:
    #                 ## Último valor de corriente extraido de la mediana del último buffer de mediciones de corriente.
    #                 self.current    = statistics.median(list(sorted(self.currBuff)))
                
    #             #Actualiza última medición de voltaje
    #             if len(self.voltBuff)>0:
    #                 ## Último valor de voltaje extraido de la mediana del último buffer de mediciones de voltaje.
    #                 self.voltage    = statistics.median(list(sorted(self.voltBuff)))

    #             #Actualiza última medición de potencia
    #             if len(self.powBuff)>0:
    #                 ## Último valor de potencia extraido de la mediana del último buffer de mediciones de potencia.
    #                 self.power      = statistics.median(list(sorted(self.powBuff)))

    #             if not self.on_read is None:
    #                 #Llama callback para procesamiento de los datos
    #                 self.on_read(self, **self.kwargs)
                
    #             #Libera control del hilo
    #             self.stopCritical()

    #             #Actualiza tiempo inicial
    #             st = st + self.buffLen*self.period

    #             #Espera a siguiente período de muestreo
    #             if(time.time() < st):
    #                 time.sleep(st-time.time())
    #         #Avisa que ha terminado de ejecutarse el hilo
    #         self.endSensor()

    #         #Espera a que se requiera nuevamente la ejecución del hilo
    #         self.waitForActivation()
if __name__ == '__main__':
    from config import config
    import statistics, numpy as np
    def onRead(sensorObj):
        #Obtiene marca de tiempo en unix y timestamp str
        unix 		= np.datetime64('now').astype('float64')*1000 #Thingsboard acepta tiempo unix en milisegundos
        timestamp 	= str(np.datetime64('now')).replace('T', ' ')
        print(timestamp)

        #Carga configuraciones del sensor
        sensor = config.getSensor(sensorObj.name)

        if not sensor is None:			
            for measurement in sensor["measurements"]:
                #Obtiene valor de la medición en base a mediana
                buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))

                if len(buff)>0:
                    value 										= statistics.median(buff)

                    #Manda mensaje de debugueo
                    print("Data obtenida de %s. %s: %s"%(sensor["name"], measurement["name"], repr(value)))
    #Obtiene configuración para el sensor am2315
    inaConfig = config.getSensor("ina219")
    if not inaConfig is None:
        #Crea hilo para lectura del sensor am2315
        ina219 = INA219_(buffLen = inaConfig["buffLen"], pin=inaConfig["enable_gpio"], period = inaConfig["period"], on_read = onRead)
        #Comienza lectura del sensor
        ina219.start()