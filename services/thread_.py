#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file thread_.py
# @brief Funcionalidades para los hilos de manera genérica. Contiene la clase Thread_, que hereda de Thread, la cual es usada como padre para todos los hilos.
# @see config/config.py
import sys
sys.path.insert(0,'..')
import time, traceback
from threading import Thread, Semaphore
from collections import deque

## @class Thread_
# @brief Esta clase hereda de Thread. Es utilizada como padre por todos los hilos. Establece funcionalidades básicas para los hilos, permitiendo la pausa y la reanudación.
class Thread_(Thread):
    ## Nombre por defecto del hilo
    name = "unnamed"
    
    ## @brief Constructor de la clase
    def __init__(self):                        
        ## Semáforo para acceder a recursos del hilo desde afuera
        self.runSem     = Semaphore(1)        
        ## Flag para saber si está ejecutandose
        self.running 	= False			                
        ## Flag para saber si se está pausando
        self.pausing 	= False
        ## Flag para saber si se está deteniendo
        self.stopping 	= False
        #Inicializa hilo
        Thread.__init__(self)

        self.cbks = deque([], maxlen=20)
    
    def every(self, seconds, fun):
        self.cbks.append({"period": seconds, "cbk": fun, "st": time.time()})
    
    def processCallbacks(self):
        #Ejecuta callbacks periodicos
        for cbk in self.cbks:
            if time.time()-cbk["st"] > cbk["period"]:
                try:
                    cbk["cbk"]()
                except:
                    traceback.print_exc()
                cbk["st"] += cbk["period"]

                #Corrige tiempo inicial en caso de existir gap
                if cbk["st"]+60*60<time.time():
                    cbk["st"] = time.time()
    
    def _run(self):
        print("You must replace _run() function")
            
    ## @brief Rutina del hilo.
    def run(self):
        #Activa flag para saber que el hilo se está ejecutando
        self.mark_started()

        while not self.is_stopping():

            while not self.is_pausing():               
                #Procesa callbacks timer
                self.processCallbacks()

                #Adquiere control del hilo
                self.start_critical()

                #Ejecuta funcion definida por el usuario
                self._run()

                #Libera control del hilo
                self.stop_critical()
            
            #Avisa que el hilo se ha pausado
            self.mark_paused()
                
            #Espera a que se requiera nuevamente la ejecución del hilo
            self.waitForActivation()
        
        #Avisa que el hilo se ha pausado
        self.mark_stopped()
        
    
    ## @brief Establece que el hilo ha comenzado a ejecutarse
    def mark_started(self):
        #Adquiere control del hilo
        self.start_critical()

        #Actualiza estado del self
        self.running = True

        #Libera control del hilo
        self.stop_critical()
    
    ## @brief Establece que el hilo se ha detenido
    def mark_paused(self):
        #Adquiere control del hilo
        self.start_critical()

        #Actualiza estado del self
        self.pausing 	= False
        self.running 	= False
        self.stopping   = False

        #Libera control del hilo
        self.stop_critical()
    
    ## @brief Establece que el hilo se ha detenido
    def mark_stopped(self):
        #Adquiere control del hilo
        self.start_critical()

        #Actualiza estado del self
        self.pausing 	= False
        self.running 	= False
        self.stopping   = False

        #Libera control del hilo
        self.stop_critical()
    
    ## @brief Espera a que el hilo reanude su ejecución
    def waitForActivation(self):
        #Espera a que se requiera la ejecución del hilo
        while(not self.is_running()):
            time.sleep(1)
    
    ## @brief Ordena pausar el hilo en el próximo ciclo.
    def pause(self):
        #Adquiere control del hilo
        self.start_critical()

        self.pausing = True

        #Libera control del hilo
        self.stop_critical()
    
    ## @brief Pregunta si el hilo se está pausando
    #
    # @params[out] stopping Bandera que indica si el hilo se está pausando
    def is_pausing(self):
        return self.pausing

    ## @brief Pregunta si el hilo se está deteniendo
    #
    # @params[out] stopping Bandera que indica si el hilo se está deteniendo
    def is_stopping(self):
        return self.stopping
    
    ## @brief Pregunta si el hilo está ejecutandose
    #
    # @params[out] running Bandera que indica si el hilo se está ejecutando
    def is_running(self):
        return self.running

    ## @brief Adquiere control del hilo mediante un semáforo
    def start_critical(self):
        self.runSem.acquire()
    
    ## @brief Libera control del hilo mediante un semáforo
    def stop_critical(self):
        self.runSem.release()
    
    ## @brief Ordena reanudar el hilo en el próximo ciclo.
    def resume(self):
        #Adquiere control del hilo
        self.start_critical()

        self.running = True

        #Libera control del hilo
        self.stop_critical()
    
    def stop(self):
        #Adquiere control del hilo
        self.start_critical()

        self.stopping = True

        #Libera control del hilo
        self.stop_critical()