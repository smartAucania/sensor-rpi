﻿#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file sensorpool.py
# @brief Se define la clase para habilitar o deshabilitar sensores dependiendo de mediciones de otros sensores. Se define método para iniciar todos los sensores en esquema multihilo.
# @see config/config.py
# @see services/sensor.py

import sys
sys.path.insert(0,'..')
from config import config
import threading, traceback, time
from collections import deque
from threading import Thread
from services.sensor import Sensor

## @class SensorGuard
class SensorGuard(Thread):
    ## @brief Constructor de la clase.
    # @param[in] sensors Lista de objetos de sensores.
    def __init__(self, sensors):
        self.sensors = sensors
        
        #Inicializa hilo
        Thread.__init__(self)
    
    ## @brief Rutina de SensorGuard.
    def run(self):
        # cont = 0
        while(True):
            #Duerme 10 minutos antes de actualizar estados de los sensores
            time.sleep(60)

            #Por cada sensor
            for sensorObj in self.sensors:

                #Obtiene configuración del sensor
                sensorConf = config.getSensor(getattr(sensorObj, 'name'))

                if not sensorConf is None and 'operation' in sensorConf:
                    try:
                        #Obtiene requerimientos de operación del sensor
                        operationConf = sensorConf['operation']

                        #Adquiere control del hilo
                        Sensor.startCritical(sensorObj)

                        #Inicializa flag para saber si el dispositivo está en su rango de operación en todas las variables
                        inRange = True

                        #Por cada requerimiento para operar del sensor
                        for op in operationConf:

                            if 'sensor' in op and 'range' in op and 'device' in op:
                                #Busca el sensor requerido en la lista de sensores
                                opSensor = next(filter(lambda s: getattr(s, 'name') == op['device'], self.sensors), None)
                                
                                if not opSensor is None:
                                    
                                    #Obtiene el valor del sensor requerido
                                    value = getattr(opSensor, op['sensor'])
                                    # if cont < 10:
                                    #     value = getattr(opSensor, op['sensor'])*-3
                                    #     print("cont: %d "%cont)
                                    #     cont += 1
                                    # else:
                                    #     value = getattr(opSensor, op['sensor'])

                                    #Obtiene el rango de operación permitido del sensor dependiente
                                    vrange = op['range']

                                    # print("Sensor: %s. Variable: %s. Rango de operación: %s. Medición: %s"%(sensorObj.name, op['sensor'], repr(op['range']), repr(value)))

                                    if (not value is None) and (value > vrange[1] or value < vrange[0]):
                                        inRange = False
                                else:
                                    print("Sensor limitante %s no se ha encontrado"%(op['device']))
                        #Libera control del hilo
                        Sensor.stopCritical(sensorObj)
                        
                        #Caso en que el sensor no esta dentro del rango en al menos una de las mediciones de las que este depende. Ej: fuera de rango en temperatura
                        if not inRange:
                            if sensorObj.isRunning() and not sensorObj.isStopping():
                                print("se detiene el sensor %s"%(sensorObj.name))
                                #Se detiene el sensor dependiente, ya que no está en su rango de operación
                                sensorObj.stop()
                            elif sensorObj.isRunning() and sensorObj.isStopping():
                                print("el sensor %s ya se está deteniendo"%(sensorObj.name))
                            elif not sensorObj.isRunning():
                                print("el sensor %s ya se ha detenido"%(sensorObj.name))
                        elif(not sensorObj.isRunning()):
                            print("se inicia el sensor %s"%(sensorObj.name))
                            #Comienza a operar el sensor dependiente, ya que está dentro del rango y no está operando actualmente
                            sensorObj.resume()
                        else:
                            print("el sensor ya se está ejecutando")
                    except Exception as e:
                        traceback.print_exc()

## @brief Inicia todos los sensores basado en el archivo de configuración.
# @see config/config.json
# @param[in] i2c_sem Semáforo para acceder al bus I2C.
# @param[in] onRead Callback a llamar por los sensores al llenar sus buffers de medición.
# @param[in] kwargs Argumentos adicionales al crear los objetos de los sensores.
# @param[out] threads Lista con los objetos de los sensores.
def startSensorThreads(onRead = None, **kwargs):
    logger = kwargs["logger"]

    #Inicializa lista de hilos de medicion de sensores
    threads = deque()

    # #Obtiene configuración para el sensor sps30
    spsConfig = config.getSensor("sps30")
    if not spsConfig is None:
        try:
            from services.sps30 import SPS30
            #Crea hilo para lectura del sensor sps30
            sps = SPS30(pin = spsConfig["enable_gpio"], busNum = spsConfig["bus"]["buss_num"], buffLen = spsConfig["buffLen"], on_read = onRead, 
                period = spsConfig["period"], cleanPeriod = spsConfig["clean_period"], **kwargs)
            #Comienza lectura del sensor
            sps.start()
            #Añade sensor a la lista
            threads.append(sps)
        except:
            traceback.print_exc()
            logger.error("No se pudo iniciar sps30")
    else:
        logger.debug("Configuración del sensor sps30 no ha sido encontrada")

    #Obtiene configuración para el sensor hpma115s0
    hpmaConfig = config.getSensor("hpma115s0")
    if not hpmaConfig is None:
        try:
            from services.hpma115s0 import HPMA115S0
            #Crea hilo para lectura del sensor hpma115s0
            hpma = HPMA115S0(buffLen = hpmaConfig["buffLen"], pin = hpmaConfig["enable_gpio"], on_read = onRead, period = hpmaConfig["period"], port = hpmaConfig["bus"]["port"], **kwargs)
            #Comienza lectura del sensor
            hpma.start()
            #Añade sensor a la lista
            threads.append(hpma)
        except:
            traceback.print_exc()
            logger.error("No se pudo iniciar hpma115s0")
    else:
        logger.debug("Configuración del sensor hpma115s0 no ha sido encontrada")
    #Obtiene configuración para el sensor pms7003
    pmsConfig = config.getSensor("pms7003")
    if not pmsConfig is None:
        try:
            from services.pms7003 import PMS7003
            #Crea hilo para lectura del sensor pms7003
            pms = PMS7003(rx = pmsConfig["bus"]["rx_gpio"], buffLen = pmsConfig["buffLen"], pin = pmsConfig["enable_gpio"], on_read = onRead, period = pmsConfig["period"], **kwargs)
            #Comienza lectura del sensor
            pms.start()
            #Añade sensor a la lista
            threads.append(pms)
        except:
            traceback.print_exc()
            logger.error("No se pudo iniciar pms7003")
    else:
        logger.debug("Configuración del sensor pms7003 no ha sido encontrada")
    #Obtiene configuración para el sensor am2315
    amConfig = config.getSensor("am2315")
    if not amConfig is None:
        try:
            from services.am2315_ import AM2315_
            #Crea hilo para lectura del sensor am2315
            am2315 = AM2315_(buffLen = amConfig["buffLen"], pin=amConfig["enable_gpio"], period = amConfig["period"], on_read = onRead, **kwargs)
            #Comienza lectura del sensor
            am2315.start()
            #Añade sensor a la lista
            threads.append(am2315)
        except:
            traceback.print_exc()
            logger.error("No se pudo iniciar am2315")
    else:
        logger.debug("Configuración del sensor am2315 no ha sido encontrada")
    #Obtiene configuración para el sensor de temperatura de la bcm2837
    bcmConfig = config.getSensor("bcm2837")
    if not bcmConfig is None:
        try:
            from services.pitemp import PiTempMonitor
            #Crea hilo para lectura del sensor de temperatura de la rpi
            pitemp = PiTempMonitor(buffLen=bcmConfig["buffLen"], period = bcmConfig["period"], on_read = onRead, **kwargs)
            #Comienza lectura del sensor
            pitemp.start()
            #Añade sensor a la lista
            threads.append(pitemp)
        except:
            traceback.print_exc()
            logger.error("No se pudo iniciar sensor de temperatura del bcm2837")
    else:
        logger.debug("Configuración del sensor de temperatura del bcm2837 no ha sido encontrada")
    #Obtiene configuración para el monitoreo del espacio en disco SDMonitor
    sdmonitorConfig = config.getSensor("sdmonitor")
    if not sdmonitorConfig is None:
        try:
            from services.sdmonitor import SDMonitor
            #Crea hilo para lectura del sensor de temperatura de la rpi
            sdmonitor = SDMonitor(buffLen=sdmonitorConfig["buffLen"], period = sdmonitorConfig["period"], on_read = onRead, **kwargs)
            #Comienza lectura del sensor
            sdmonitor.start()
            #Añade sensor a la lista
            threads.append(sdmonitor)
        except:
            traceback.print_exc()
            logger.error("No se pudo iniciar sdmonitor")
    else:
        logger.debug("Configuración del sensor sdmonitor no ha sido encontrada")
    #Obtiene configuración para el sensor ina219
    inaConfig = config.getSensor("ina219")
    if not inaConfig is None:
        try:
            from services.ina219_ import INA219_
            #Crea hilo para lectura del sensor ina219
            ina219 = INA219_(buffLen = inaConfig["buffLen"], pin=inaConfig["enable_gpio"], period = inaConfig["period"], on_read = onRead, **kwargs)
            #Comienza lectura del sensor
            ina219.start()
            threads.append(ina219)
        except:
            traceback.print_exc()
            logger.error("No se pudo iniciar ina219")
    else:
        logger.debug("Configuración del sensor ina219 no ha sido encontrada")
    fakeSensorsConfig = config.getFakeSensors()
    if not fakeSensorsConfig is None:
        from services.fake_sensor import FakeSensor
        for fakeSensorConfig in fakeSensorsConfig:
            try:
                fakeSensor = FakeSensor(buffLen = fakeSensorConfig["buffLen"], period = fakeSensorConfig["period"], on_read = onRead, 
                    name = fakeSensorConfig["name"], generator = fakeSensorConfig["generator"], **kwargs)
                fakeSensor.start()
                threads.append(fakeSensor)
            except:
                traceback.print_exc()
                logger.error("No se pudo iniciar sensor fake")
    else:
        logger.debug("Configuración de sensores fake no ha sido encontrada")
    # #Inicia hilo para proteger los sensores manteniendolos en sus rangos de operación
    # try:
    #     sensorGuard = SensorGuard(threads)
    # except Exception as e:
    #     traceback.print_exc()
    # sensorGuard.start()

    print("sensores inicializados")

    return list(threads)