#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file pms7003.py
# @brief Contiene clase con funciones para monitoreo del sensor de material particulado 2.5 y 10 Plantower PMS7003 usando esquema multihilo.
# @see config/config.py
# @see services/sensor.py

import sys
sys.path.insert(0,'..')
import pigpio
from collections import deque
import threading
from services.sensor import Sensor
import time
import numpy as np
from services import uart

## @class PMS7003
# @code{.py}
# from config import config
# import statistics, numpy as np
# def onRead(sensorObj):
# 	#Obtiene marca de tiempo en unix y timestamp str
# 	unix 		= np.datetime64('now').astype('float64')*1000 #Thingsboard acepta tiempo unix en milisegundos
# 	timestamp 	= str(np.datetime64('now')).replace('T', ' ')
# 	print(timestamp)
#
# 	#Carga configuraciones del sensor
# 	sensor = config.getSensor(sensorObj.name)
#
# 	if not sensor is None:			
# 		for measurement in sensor["measurements"]:
# 			#Obtiene valor de la medición en base a mediana
# 			buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))
#
# 			if len(buff)>0:
# 				value 										= statistics.median(buff)
#
# 				#Manda mensaje de debugueo
# 				print("Data obtenida de %s. %s: %s"%(sensor["name"], measurement["name"], repr(value)))
# #Obtiene configuración para el sensor pms7003
# pmsConfig = config.getSensor("pms7003")
# if not pmsConfig is None:
# 	# from pms7003 import PMS7003
# 	#Crea hilo para lectura del sensor pms7003
# 	pms = PMS7003(rx = pmsConfig["bus"]["rx_gpio"], buffLen = pmsConfig["buffLen"], pin = pmsConfig["enable_gpio"], on_read = onRead, period = pmsConfig["period"])
# 	#Comienza lectura del sensor
# 	pms.start()
# @endcode
class PMS7003(Sensor):
	## Nombre del sensor.
	name 		= "pms7003"	
	## Velocidad del bus UART.
	BAUDRATE 	= 9600
	## Limite superior del rango de medición del material particulado en ug/m^3.
	LIMIT   	= 1000

	## @brief Constructor de la clase.
	# @param[in] rx Pin GPIO para lectura serial.\n
	# @param[in] pin Pin GPIO destinado a habilitar el sensor.\n
	# @param[in] buffLen Tamaño de buffers de mediciones.\n
	# @param[in] on_read Callback a llamar al llenarse los buffers.\n
	# @param[in] period Período de tiempo entre muestras.\n
	# @param[in] nattempts Cantidad de reintentos de lectura por medición.\n
	# @param[in] maxTimeOffset Cantidad de tiempo máximo entre que termina un período de lectura y comienza el siguiente.
	def __init__(self, rx = 12, buffLen = 11, pin = 25, on_read = None, period = 11, nattempts = 3, maxTimeOffset = 15, **kwargs):
		## Objeto pigpio para acceder a gpio y puero virtual serial.
		self.pi 			= pigpio.pi() 					
		## Pin destinado a lectura puerto serial virtual.
		self.rx 			= rx							
		## Pin destinado a encender o apagar el sensor.
		self.pin 			= pin							
		## Tamaño de buffers de mediciones.
		self.buffLen 		= buffLen						
		## Variable temporal de medicion pm1.
		self._pm1 			= None							
		## Variable temporal de medicion pm10.
		self._pm10 			= None							
		## Variable temporal de medicion pm2.5.
		self._pm2p5 		= None												
		## Periodo entre lecturas.
		self.period 		= period						
		## Callback a llamar cuando se llenan los buffers.
		self.on_read 		= on_read						
		## Argumentos extra para callback.
		self.kwargs 		= kwargs		
		self.logger         = kwargs.get("logger", None)				
		## Cantidad de reintentos de lectura por medición.
		self.nattempts 		= nattempts 					
		## Cantidad máxima de tiempo extra que puede tardar en tomar todas las muestras.
		self.maxTimeOffset 	= maxTimeOffset					

		#Inicializa hilo
		Sensor.__init__(self, pin = pin, buffLen = buffLen, on_read = on_read, period = period, maxTimeOffset = maxTimeOffset, **kwargs)

		#Configura puerto serial virtual para plantower
		while True:
			if(uart.openVirtualSerial(self.pi, self.rx, baudrate=PMS7003.BAUDRATE, logger = self.logger)):
				break
	
	def appendMeasure(self):
		# self.readFake()
		if(self.read()):
			self.pm2p5Buff.append(self.pm2p5())
			self.pm10Buff.append(self.pm10())
			if not self.logger is None:
				self.logger.info("Sensor Plantower pms7003 logra medir. PM2.5: %.2f. PM10: %.2f."%(self.pm2p5(), self.pm10()))
			else:
				print("Sensor Plantower pms7003 logra medir. PM2.5: %.2f. PM10: %.2f."%(self.pm2p5(), self.pm10()))
	
	def readFake(self):
		self.pm10Buff.append(np.random.normal(100, 5, 1)[0])
		self.pm2p5Buff.append(np.random.normal(90, 10, 1)[0])

	def initBuffers(self):
		## Buffer de mediciones de pm10
		self.pm10Buff 	    = deque([], maxlen = self.buffLen)                 
		## Buffer de mediciones de pm2.5
		self.pm2p5Buff 	    = deque([], maxlen = self.buffLen)

	## @brief Obtiene último valor de PM 1.0.
	# @param[out] _pm1 Último valor de PM 1.0.
	def pm1(self):
		#Limita valores del pm10 al rango indicado en el datasheet
		if self._pm1<0:
			self._pm1 = 0
		if self._pm1 > PMS7003.LIMIT:
			self._pm1 = PMS7003.LIMIT
		return self._pm1
	
	## @brief Obtiene último valor de PM 2.5.
	# @param[out] _pm2p5 Último valor de PM 2.5.
	def pm2p5(self):
		#Limita valores del pm2.5 al rango indicado en el datasheet
		if self._pm2p5<0:
			self._pm2p5 = 0
		if self._pm2p5 > PMS7003.LIMIT:
			self._pm2p5 = PMS7003.LIMIT
		return self._pm2p5
	
	## @brief Obtiene último valor de PM 10.
	# @param[out] Último valor de PM 10.
	def pm10(self):
		#Limita valores del pm10 al rango indicado en el datasheet
		if self._pm10<0:
			self._pm10 = 0
		if self._pm10 > PMS7003.LIMIT:
			self._pm10 = PMS7003.LIMIT
		return self._pm10
	
	## @brief Rutina del sensor.
	def read(self):
		#Inicializa contador de caracteres de lectura
		count = 0
		
		#Inicializa contador de intentos de lectura
		attempt = 0

		time.sleep(9)

		while attempt <= self.nattempts:
			#Duerme 1 segundo entre lecturas
			time.sleep(1)

			#Lee puerto serial virtual
			(count,data) = self.pi.bb_serial_read(self.rx)

			if count>=32:
				if data[0]==0x42 and data[1]==0x4d:
					
					#Almacena la medición
					self._pm1	= ord(chr(data[5]))+(ord(chr(data[4]))<<8)
					self._pm2p5	= ord(chr(data[7]))+(ord(chr(data[6]))<<8)
					self._pm10	= ord(chr(data[9]))+(ord(chr(data[8]))<<8)
					return True
			if count>0:
				if not self.logger is None:
					self.logger.debug("Sensor Plantower pms7003 realiza lectura pero el largo no corresponde")
				else:
					print("Sensor Plantower pms7003 realiza lectura pero el largo no corresponde")
			
			if count==0:
				if not self.logger is None:
					self.logger.debug("Sensor Plantower pms7003 no entrega ninguna información")
				else:
					print("Sensor Plantower pms7003 no entrega ninguna información")
			
			#Actualiza contador de intentos
			attempt += 1
		if not self.logger is None:
			self.logger.warn("Sensor Plantower pms7003 no logra medir PM2.5 y PM10 luego de %d intentos"%(attempt))
		else:
			print("Sensor Plantower pms7003 no logra medir PM2.5 y PM10 luego de %d intentos"%(attempt))
		return False

if __name__ == '__main__':
	from config import config
	import statistics, numpy as np
	def onRead(sensorObj):
		#Obtiene marca de tiempo en unix y timestamp str
		unix 		= np.datetime64('now').astype('float64')*1000 #Thingsboard acepta tiempo unix en milisegundos
		timestamp 	= str(np.datetime64('now')).replace('T', ' ')
		print(timestamp)

		#Carga configuraciones del sensor
		sensor = config.getSensor(sensorObj.name)

		if not sensor is None:			
			for measurement in sensor["measurements"]:
				#Obtiene valor de la medición en base a mediana
				buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))

				if len(buff)>0:
					value 										= statistics.median(buff)

					#Manda mensaje de debugueo
					print("Data obtenida de %s. %s: %s"%(sensor["name"], measurement["name"], repr(value)))
	#Obtiene configuración para el sensor pms7003
	pmsConfig = config.getSensor("pms7003")
	if not pmsConfig is None:
		# from pms7003 import PMS7003
		#Crea hilo para lectura del sensor pms7003
		pms = PMS7003(rx = pmsConfig["bus"]["rx_gpio"], buffLen = pmsConfig["buffLen"], pin = pmsConfig["enable_gpio"], on_read = onRead, period = pmsConfig["period"])
		#Comienza lectura del sensor
		pms.start()