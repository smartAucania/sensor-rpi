import time, spidev, pigpio, threading
from collections import deque

#Register definitions
REG_FIFO                       = 0x00
REG_OP_MODE                    = 0x01
REG_FRF_MSB                    = 0x06
REG_FRF_MID                    = 0x07
REG_FRF_LSB                    = 0x08
REG_PA_CONFIG                  = 0x09
REG_LNA                        = 0x0c
REG_FIFO_ADDR_PTR              = 0x0d
REG_FIFO_TX_BASE_ADDR          = 0x0e
REG_FIFO_RX_BASE_ADDR          = 0x0f
REG_FIFO_RX_CURRENT_ADDR       = 0x10
REG_IRQ_FLAGS_MASK             = 0x11
REG_IRQ_FLAGS                  = 0x12
REG_RX_NB_BYTES                = 0x13
REG_PKT_SNR_VALUE              = 0x19
REG_PKT_RSSI_VALUE             = 0x1a
REG_MODEM_CONFIG_1             = 0x1d
REG_MODEM_CONFIG_2             = 0x1e
REG_PREAMBLE_MSB               = 0x20
REG_PREAMBLE_LSB               = 0x21
REG_PAYLOAD_LENGTH             = 0x22
REG_MODEM_CONFIG_3             = 0x26
REG_RSSI_WIDEBAND              = 0x2c
REG_DETECTION_OPTIMIZE         = 0x31
REG_DETECTION_THRESHOLD        = 0x37
REG_SYNC_WORD                  = 0x39
REG_DIO_MAPPING_1              = 0x40
REG_VERSION                    = 0x42
REG_INVERTIQ                   = 0x33

#Transceiver modes
MODE_RANGE_MODE                = 0x80
MODE_SLEEP                     = 0x00
MODE_STDBY                     = 0x01
MODE_TX                        = 0x03
MODE_RX_CONTINUOUS             = 0x05
MODE_RX_SINGLE                 = 0x06
MODE_CAD                       = 0x07
MODE_UNKNOWN                   = 0xff

#PA configuration
PA_BOOST                       = 0x80

#IRQ masks
IRQ_TX_DONE_MASK               = 0x08
IRQ_PAYLOAD_CRC_ERROR_MASK     = 0x20
IRQ_RX_DONE_MASK               = 0x40
IRQ_CAD_DONE_MASK              = 0X04
IRQ_CAD_DETECTED_MASK          = 0x01
RX_TIMEOUT                     = 0x80

PA_OUTPUT_RFO_PIN              = 0
PA_OUTPUT_PA_BOOST_PIN         = 1

class RFM95:
    def __init__(self, irq=21, cs=26, reset=25):
        self.baud               = 8000000
        self.irq                = irq
        self.cs                 = cs
        self.reset_pin          = reset
        self.spi                = spidev.SpiDev()
        self.spi.open(0, 0)
        self.pi                 = pigpio.pi()                              
        self.pi.set_mode(self.irq, pigpio.INPUT)
        self.pi.set_mode(self.reset_pin, pigpio.OUTPUT)
        self.pi.set_mode(self.cs, pigpio.OUTPUT)
        # self.spi.open(0,0)
        self.spi.max_speed_hz   = 8000000 
        self.__header_mode      = "unknown"
        self.__mode             = MODE_UNKNOWN
        self.__tx_power         = -1
        self.__frequency        = -1
        self.__sf               = -1
        self.__bw               = -1
        self.__cr               = -1
        self.__pl               = -1
        self.__syncw            = -1
        self.__crc              = False
        self.__cad              = False
        self.rxbuff             = deque()
        self.__rx_buff_valid    = False
        self.rx_bad             = 0
        self.rx_good            = 0
        self.tx_bad             = 0
        self.tx_good            = 0
        self.__lock             = False
        self.last_length        = 0
        self.last_rssi          = 0

        #Prepara interrupción
        # self.irq.irq(trigger = Pin.IRQ_RISING, handler = self.handle_interrupt)
        
        # self.pi.callback(self.irq, pigpio.RISING_EDGE, lambda gpio, level, tick: self.handle_interrupt(gpio))

        #Al parecer sin resetear lora no funciona
        self.reset()

        # try:
        #     self.cs.off()
        #     self.spi = SPI(baudrate = 8000000, polarity = 0, phase = 0, bits = 8, firstbit = SPI.MSB,
        #             sck = Pin(self.sck, Pin.OUT, Pin.PULL_DOWN),
        #             mosi = Pin(self.mosi, Pin.OUT, Pin.PULL_UP),
        #             miso = Pin(self.miso, Pin.IN, Pin.PULL_UP))
        #     self.cs.on()
        # except Exception as e:
        #     if self.spi:
        #         self.spi.deinit()
        #     print(e)
        
        #Chequea que exista la radio
        if not self.check_radio():
            raise Exception("LoRa Invalid version. Expected version 0x12.")
        
        self.sleep()
        
        self.transfer(REG_FIFO_RX_BASE_ADDR, 0)
        self.transfer(REG_FIFO_TX_BASE_ADDR, 0)
        self.transfer(REG_LNA, self.transfer(REG_LNA) | 0x03)

        # set auto AGC
        self.transfer(REG_MODEM_CONFIG_3, 0x04)
        self.tx_power = 17
        self.idle()
        # self.clear_irq()

    def lock(self):
        i = 0
        while(self.__lock):
            time.sleep(5/1000)
            i += 1
            if i>1000:
                print("se pego :c")
                # break
        self.__lock = True

    def unlock(self):
        self.__lock = False

    def check_radio(self):
        for i in range(3):
            if self.version() == 0x12:
                return True
        return False
    
    def version(self):
        return self.transfer(REG_VERSION)

    def transfer(self, address, value = None):
        # response = bytearray(1)
        # #self.lock()
        # self.cs.off()
        self.pi.write(self.cs, 0)
        if value is None:
            address = address&0x7f
            value = 0x00
        else:
            address = address|0x80
            value = value & 0xFF
        
        resp = self.spi.xfer2(bytes([address, value]))
        if resp is None:
            resp = [0, 0]
        resp = resp[1]
        self.pi.write(self.cs, 1)
        # print(resp, type(resp))
        # self.spi.write_readinto(bytes([value]), response)
        # self.cs.on()
        # #self.unlock()
        # return int.from_bytes(resp, "big")
        return resp

    def handle_interrupt(self, gpio):
        #self.unlock()
        #self.lock()
        try:
            # Read the errupt register
            self.__irq_flags = self.transfer(REG_IRQ_FLAGS)
            # Clear all IRQ flags
            self.irq_flags = self.__irq_flags

            if self.irq_flags & (IRQ_PAYLOAD_CRC_ERROR_MASK):
                # print("rx bad")
                self.rx_bad += 1
                self.idle()
            elif  self.irq_flags & RX_TIMEOUT:
                # print("rx timeout")
                self.idle()
            elif (self.irq_flags & IRQ_RX_DONE_MASK) and (self.irq_flags & IRQ_PAYLOAD_CRC_ERROR_MASK) == 0:
                # packet received            
                # length = self.packet_length()

                # Reset the fifo read ptr to the beginning of the packet
                # self.reset_fifo_ptr()
                # set FIFO address to current RX address
                self.transfer(REG_FIFO_ADDR_PTR, self.transfer(REG_FIFO_RX_CURRENT_ADDR))
                
                # ret_length = self.store_packet()
                self.last_length = self.store_packet()

                # save SNR
                self.last_snr = self.packet_snr()

                # save RSSI
                self.last_rssi = self.packet_rssi()
                
                # We have received a message
                self.rx_good += 1
                self.__rx_buff_valid = True
                self.idle()
                # self.mode = MODE_RX_CONTINUOUS
            elif(self.mode == (MODE_TX )  and self.irq_flags & IRQ_TX_DONE_MASK):
                # print("tx done")
                self.tx_good += 1
                self.mode = MODE_STDBY
            elif(self.mode == (MODE_CAD | MODE_RANGE_MODE) and self.irq_flags & IRQ_CAD_DONE_MASK):
                self.__cad = (self.irq_flags & IRQ_CAD_DETECTED_MASK) == IRQ_CAD_DETECTED_MASK
                self.mode = MODE_STDBY
            # else:
                # print("unknown interrupt")
            
            # self.collect_garbage()
        except:
            print("error en la interrupcion")
        #self.unlock()

    def snr(self):
        return self.last_snr

    def rssi(self):
        return self.last_rssi

    def length(self):
        return self.last_length

    # def close(self):
    #     self.spi.deinit()
    
    def reset(self):
        # self.cs.on()
        self.pi.write(self.cs, 0)
        # self.reset_pin.off()
        self.pi.write(self.reset_pin, 0)
        time.sleep(300/1000)
        # self.reset_pin.on()
        self.pi.write(self.reset_pin, 1)
        time.sleep(10/1000)
        self.pi.write(self.cs, 1)

    def packet_length(self):
        length = -1
        if (self.header_mode == "implicit"):
            length = self.transfer(REG_PAYLOAD_LENGTH)
        elif self.header_mode == "explicit": 
            length = self.transfer(REG_RX_NB_BYTES)
        else:
            raise Exception("Cannot determine packet length because header mode is invalid")
        return length
    
    def reset_fifo_ptr(self):
        self.transfer(REG_FIFO_ADDR_PTR, self.transfer(REG_FIFO_RX_CURRENT_ADDR))

    @property
    def header_mode(self):
        return self.__header_mode
    
    @header_mode.setter
    def header_mode(self, mode):
        #self.lock()
        if mode == "explicit":
            self.__header_mode = mode
            self.transfer(REG_MODEM_CONFIG_1, self.transfer(REG_MODEM_CONFIG_1) & 0xfe)
        elif mode == "implicit":
            self.transfer(REG_MODEM_CONFIG_1, self.transfer(REG_MODEM_CONFIG_1) | 0x01)
            self.__header_mode = mode
        else:
            raise Exception("Unknown header mode")
        #self.unlock()

    # def explicit_header(self):
    #     self.header_mode = "explicit"
    #     self.transfer(REG_MODEM_CONFIG_1, self.transfer(REG_MODEM_CONFIG_1) & 0xfe)
    
    # ## Configure implicit header mode.
    # # All packets will have a predefined size.
    # # @param size Size of the packets.
    # def implicit_header(self, size):
    #     self.header_mode = "implicit"
    #     self.transfer(REG_MODEM_CONFIG_1, self.transfer(REG_MODEM_CONFIG_1) | 0x01)
    #     self.transfer(REG_PAYLOAD_LENGTH, size) 
    
    ## Sets the radio transceiver mode
    # @param mode Radio mode
    @property
    def mode(self):
        return self.transfer(REG_OP_MODE)
        
    @mode.setter
    def mode(self, mode):
        mode = mode & 0xFF
        if(not self.__mode == mode):
            self.transfer(REG_OP_MODE, MODE_RANGE_MODE | mode)
            self.__mode = mode
    
    ## Sets the radio transceiver in idle mode.
    # Must be used to change registers and access the FIFO.
    def idle(self):
        #self.lock()
        self.mode = MODE_STDBY
        #self.unlock()
        
    ## Sets the radio transceiver in sleep mode.
    # Low power consumption and FIFO is lost.
    def sleep(self):
        #self.lock()
        self.mode = MODE_SLEEP
        #self.unlock()
    
    ## Sets the radio transceiver in receive mode.
    # Incoming packets will be received.
    def receive(self):
        #self.lock()
        if not self.mode == (MODE_RX_SINGLE | MODE_RANGE_MODE):
            self.transfer(REG_FIFO_ADDR_PTR, value = 0)
            self.mode = MODE_RX_SINGLE
        # self.mode = MODE_RX_SINGLE
            self.transfer(REG_DIO_MAPPING_1, 0x00)
        #self.unlock()
    
    ## Sets the radio transceiver in tx mode.
    # Radio buffer will be transmitted.
    def send(self):
        self.mode = MODE_TX
        self.transfer(REG_DIO_MAPPING_1, 0x40)

    ## Sets the radio transceiver in cad mode.
    def cad(self):
        #self.lock()
        self.mode = MODE_CAD
        self.transfer(REG_DIO_MAPPING_1, 0x80)
        #self.unlock()
    
    @property
    def irq_flags(self):
        return self.__irq_flags
    
    @irq_flags.setter
    def irq_flags(self, flags):
        self.__irq_flags = flags
        self.transfer(REG_IRQ_FLAGS, flags)
    
    ##Configure power level for transmission
    # @param level 2-17, from least to most power
    @property
    def tx_power(self):
        return self.__tx_power
    
    @tx_power.setter
    def tx_power(self, level):
        #self.lock()
        level = min(max(level, 2), 20)
        self.__tx_power = level
        self.transfer(REG_PA_CONFIG, PA_BOOST | (level - 2))
        #self.unlock()

    ## Set carrier frequency.
    # @param frequency Frequency in Hz
    @property
    def frequency(self):
        return self.__frequency
    
    @frequency.setter
    def frequency(self, freq):
        #self.lock()
        freq = int(freq)
        if not freq == self.frequency:
            self.__frequency    = freq
            frf                 = int((freq << 19) / 32000000)
            self.transfer(REG_FRF_MSB, frf >> 16)
            self.transfer(REG_FRF_MID, frf >> 8)
            self.transfer(REG_FRF_LSB, frf >> 0)
        #self.unlock()

    ## Set spreading factor.
    # @param sf 6-12, Spreading factor to use.
    @property
    def spreading_factor(self):
        return self.__sf
    
    @spreading_factor.setter
    def spreading_factor(self, sf):
        #self.lock()
        sf          = min(max(sf, 6), 12)
        self.__sf   = sf

        if (sf == 6):
            self.transfer(REG_DETECTION_OPTIMIZE, 0xc5)
            self.transfer(REG_DETECTION_THRESHOLD, 0x0c)
        else:
            self.transfer(REG_DETECTION_OPTIMIZE, 0xc3)
            self.transfer(REG_DETECTION_THRESHOLD, 0x0a)

        self.transfer(REG_MODEM_CONFIG_2, (self.transfer(REG_MODEM_CONFIG_2) & 0x0f) | ((sf << 4) & 0xf0))
        self.set_ldo_flag()
        #self.unlock()
        
    ## Set bandwidth (bit rate)
    # @param sbw Bandwidth in Hz (up to 500000)
    @property
    def bandwidth(self):
        return self.__bw
    
    @bandwidth.setter
    def bandwidth(self, sbw):
        #self.lock()
        if (sbw <= 7.8E3):
            bw = 0
        elif(sbw <= 10.4E3): 
            bw = 1
        elif(sbw <= 15.6E3): 
            bw = 2
        elif(sbw <= 20.8E3): 
            bw = 3
        elif(sbw <= 31.25E3): 
            bw = 4
        elif(sbw <= 41.7E3): 
            bw = 5
        elif(sbw <= 62.5E3): 
            bw = 6
        elif(sbw <= 125E3): 
            bw = 7
        elif(sbw <= 250E3): 
            bw = 8
        else: 
            bw = 9
        self.__bw = sbw
        self.transfer(REG_MODEM_CONFIG_1, (self.transfer(REG_MODEM_CONFIG_1) & 0x0f) | (bw << 4))
        self.set_ldo_flag()
        #self.unlock()

    ## Set coding rate 
    # @param denominator 5-8, Denominator for the coding rate 4/x
    @property
    def coding_rate(self):
        return self.__cr

    @coding_rate.setter
    def coding_rate(self, cr):
        #self.lock()
        cr = min(max(cr, 5), 8)
        self.__cr = cr
        cr = cr - 4        
        self.transfer(REG_MODEM_CONFIG_1, (self.transfer(REG_MODEM_CONFIG_1) & 0xf1) | (cr << 1))
        #self.unlock()
    
    ## Set the size of preamble.
    # @param length Preamble length in symbols.
    @property
    def preamble_length(self):
        return self.__pl

    @preamble_length.setter
    def preamble_length(self, pl):
        #self.lock()
        self.__pl = pl
        self.transfer(REG_PREAMBLE_MSB, pl >> 8)
        self.transfer(REG_PREAMBLE_LSB, pl >> 0)
        #self.unlock()
    
    ## Change radio sync word.
    # @param syncw New sync word to use.
    @property
    def sync_word(self):
        return self.__syncw
    
    @sync_word.setter
    def sync_word(self, syncw):
        #self.lock()
        self.__syncw = syncw
        self.transfer(REG_SYNC_WORD, syncw)
        #self.unlock()

    ## Enable/Disable appending/verifying packet CRC.
    @property
    def crc(self):
        return self.__crc
    
    @crc.setter
    def crc(self, _crc):
        #self.lock()
        self.__crc = _crc
        if(_crc):
            self.transfer(REG_MODEM_CONFIG_2, self.transfer(REG_MODEM_CONFIG_2) | 0x04)
        else:
            self.transfer(REG_MODEM_CONFIG_2, self.transfer(REG_MODEM_CONFIG_2) & 0xfb)
        #self.unlock()

    ## Clean irq flags.     
    def clear_irq(self):
        #self.lock()
        # self.irq_flags = 0xff
        self.__irq_flags = self.transfer(REG_IRQ_FLAGS)
        # Clear all IRQ flags
        self.irq_flags = self.__irq_flags
        #self.unlock()
    
    ## Clean RX buffer
    def clear_rx_buff(self):
        if len(self.rxbuff) == 0:
            self.__rx_buff_valid = False
    
    def drop_rx_buff(self):
        self.rxbuff = deque()
        self.clear_rx_buff()
    
    def invert_iq(self, invert = True):
        if not invert:
            code = 0x27 
        else: 
            code = 0x66
        self.transfer(REG_INVERTIQ , code)
    
    ## Send a packet.
    # @param buf Data to be sent
    # @param size Size of data.
    def send_packet(self, buff):
        #self.lock()
        self.mode = MODE_STDBY
        # buff = buff.encode()
        self.transfer(REG_FIFO_ADDR_PTR, 0)
        self.transfer(REG_PAYLOAD_LENGTH, 0)

        #Transfer data to radio
        [self.transfer(REG_FIFO, buff_byte) for buff_byte in buff]
        self.transfer(REG_PAYLOAD_LENGTH, min(len(buff), 250))

        #Start transmission
        self.send()

        # self.collect_garbage()
        #self.unlock()
    
    ## Wait for irq meaning packet was sent.
    def wait_packet_sent(self, timeout = -1):
        st = time.time()
        while not self.packet_sent():
        # while self.transfer(REG_IRQ_FLAGS) & IRQ_TX_DONE_MASK == 0:
            if timeout > 0 and (time.time() - st)>timeout:
                return False
            time.sleep(10/1000)
        #self.lock()
        self.transfer(REG_IRQ_FLAGS, IRQ_TX_DONE_MASK)
        #self.unlock()
        return True
        
    ## @return True if packet was sent.
    def packet_sent(self):
        return self.mode != MODE_TX | MODE_RANGE_MODE
        # return not (self.transfer(REG_IRQ_FLAGS) & IRQ_TX_DONE_MASK == 0)
    
    ## Check if the channel is active. Radio mode must be set to MODE_CAD before.
    # @return Boolean indicating if channel is active.
    def channel_active(self):
        self.cad()
        while(self.mode == MODE_CAD):
            time.sleep(5/1000)

        cad             = self.__cad
        self.__cad      = False
        return cad
    
    def store_packet(self):
        # self.mode = MODE_STDBY

        #Find packet size
        length = self.packet_length()

        #Transfer data
        self.reset_fifo_ptr()
        buff = [self.transfer(REG_FIFO) for i in range(length)]

        #Store packet in FIFO
        self.rxbuff.append(([int(b) for b in buff]))

        return length

    ## Read a received packet.
    # @return List of bytes received (None if no packet available).
    def receive_packet(self):
        if(self.received()):
            #self.lock()
            buff = self.rxbuff.popleft()
            self.buff = None
            self.clear_rx_buff()
            # self.collect_garbage()
            #self.unlock()
            return buff
        return None
    
    ## @return True if there is data to read (packet received).
    def received(self):
        if(self.mode == MODE_TX):
            return False
        return self.__rx_buff_valid

    ## Suspend the current thread until a packet arrives or a timeout occurs.
    # @param timeout Timeout in ms.
    def wait_for_packet(self, timeout = -1):
        st = time.time()
        if timeout > 0:
            while(time.time()-st < timeout/1000):
                self.handle_interrupt(None)
                self.receive()
                if self.received():
                    return True
                time.sleep(1/1000)
        else:
            while True:
                self.handle_interrupt(None)
                self.receive()
                if self.received():
                    return True
                time.sleep(1/1000)
        return False

    ## @return last packet's RSSI.
    def packet_rssi(self):
        v = self.transfer(REG_PKT_RSSI_VALUE)
        if self.frequency < 868E6:
            delta = 164
        else:
            delta = 157
        return v - delta
    
    ## Return last packet's SNR (signal to noise ratio)
    def packet_snr(self):
        v = self.transfer(REG_PKT_SNR_VALUE)
        return v * 0.25
    
    def set_ldo_flag(self):
        # Section 4.1.1.5
        symbol_duration = 1000 / (self.bandwidth/(1 << self.spreading_factor)) 

        # Section 4.1.1.6
        ldo_on = symbol_duration > 16

        config3 = self.transfer(REG_MODEM_CONFIG_3)
        config3 = config3 & 0xFB
        config3 = config3 | (ldo_on & 0x04)
        self.transfer(REG_MODEM_CONFIG_3, value = config3)
    
    def parsePacket(self, size = None):
        packet_length = 0
        # irq_flags = self.transfer(REG_IRQ_FLAGS)

        # # clear IRQ's
        # self.transfer(REG_IRQ_FLAGS, value = irq_flags)

        self.__irq_flags = self.transfer(REG_IRQ_FLAGS)
        # Clear all IRQ flags
        self.clear_irq()

        if ((self.__irq_flags & IRQ_RX_DONE_MASK) and (self.__irq_flags & IRQ_PAYLOAD_CRC_ERROR_MASK) == 0):
            print("Paquete recibido")
            # received a packet
            self._packet_index = 0

            packet_length = self.packet_length()

            # set FIFO address to current RX address
            self.transfer(REG_FIFO_ADDR_PTR, value = self.transfer(REG_FIFO_RX_CURRENT_ADDR))
            # self.transfer(REG_FIFO_ADDR_PTR, value = 0)

            # put in standby mode
            # self.idle()
        elif (self.transfer(REG_OP_MODE) != (MODE_RANGE_MODE | MODE_RX_SINGLE)):
            # print("No estaba en rx :c")
            # not currently in RX mode

            # reset FIFO address
            self.transfer(REG_FIFO_ADDR_PTR, value = 0)

            # put in single RX mode
            self.transfer(REG_OP_MODE, value = MODE_RANGE_MODE | MODE_RX_SINGLE)
        else:
            pass
            # print("Algo ha ocurrido :C")
        return packet_length
    
    def __str__(self):
        state = "Lora RF95 Version: %s\n"%(hex(self.version()))
        state = state + "Transmission power: %d\n"%self.tx_power
        state = state + "Header Mode: %s\n"%self.header_mode
        state = state + "Bandwidth: %d\n"%self.bandwidth
        state = state + "Coding rate: 4/%d\n"%self.coding_rate
        state = state + "Preamble length: %d\n"%self.preamble_length
        state = state + "Sync word: %s\n"%(hex(self.sync_word))
        state = state + "Spreading Factor: %d\n"%self.spreading_factor
        state = state + "Frequency: %d\n"%self.frequency
        if self.crc:
            state = state + "CRC: Enabled"
        else:
            state = state + "CRC: Disabled"
        return state
    
    # def collect_garbage(self):
    #     gc.collect()
if __name__ == "__main__":
    frequency       = 868000000 
    crc             = True
    tx_power        = 17
    sf              = 10
    bw              = 125000
    header_mode     = "explicit"
    sync_word       = 0x73
    rate            = 5
    len_preamble    = 4
    cs_pin          = 17
    rst_pin         = 27
    dio0_pin        = 5

    #Init lora
    rfm95                   = RFM95(cs = cs_pin, reset = rst_pin, irq = dio0_pin)

    #Configure lora
    rfm95.tx_power          = tx_power
    rfm95.frequency         = frequency
    rfm95.header_mode       = header_mode
    rfm95.spreading_factor  = sf
    rfm95.bandwidth         = bw
    rfm95.coding_rate       = rate
    rfm95.preamble_length   = len_preamble
    rfm95.sync_word         = sync_word
    rfm95.crc               = crc

    print(rfm95)
    npackets = 0

    # rfm95.mode = MODE_RX_CONTINUOUS
    # rfm95.receive()
    while True:
        # rfm95.send_packet([85,86,87,88,89,90,91,92,93,94,95,96,97,98])
        # rfm95.wait_packet_sent()
        # npackets+=1
        # print("Npackets: ", npackets)

        # size = rfm95.parsePacket()
        # if(size > 0):
        #     npackets+=1
        #     print("npackets: ",npackets)
        rfm95.handle_interrupt(None)

        # print("Sending ")
        # rfm95.send_packet([85,86,87,88,89,90,91,92,93,94,95,96,97,98])
        # print("waiting")
        # rfm95.wait_packet_sent()
        
        # rfm95.receive()
        rfm95.wait_for_packet()
        # print("paquete")
        # if rfm95.received():
        npackets+=1
        print("npacket: ", npackets)
        print(rfm95.receive_packet())

        # rfm95.receive()    
        # if(rfm95.wait_for_packet(timeout=-1)):
        #     print(rfm95.receive_packet())
        #     npackets+=1
        #     print("Paquetes: %d"%npackets)
        