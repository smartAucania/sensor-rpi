import sys
sys.path.insert(0,'..')
import pigpio
import numpy as np
from collections import deque
from services.sensor import Sensor

class FakeSensor(Sensor):
    def __init__(self, buffLen = 11, on_read = None, period = 10, maxTimeOffset = 15, generator = [], name="FakeSensor", **kwargs):            
        ## Objeto pigpio para acceder a la gpio de la rpi                    
        self.pi             = pigpio.pi()
        ## Tamaño del buffer de mediciones
        self.buffLen 	    = buffLen            
        ## Callback a llamar cuando se hayan completado los buffers
        self.on_read        = on_read                                   
        ## Periodo de tiempo entre lecturas
        self.period         = period                        
        ## Argumentos extra para callback
        self.kwargs         = kwargs                                
        ## Cantidad máxima de tiempo extra que puede tardar en tomar todas las muestras
        self.maxTimeOffset 	= maxTimeOffset	
        self.name           = name
        self.generator      = generator
        Sensor.__init__(self, buffLen = buffLen, on_read = on_read, period = period, maxTimeOffset = maxTimeOffset, **kwargs)
    
    def read(self, mu, sigma, llimit, hlimit):
        value = np.random.normal(mu, sigma, 1)[0]
        if value >= hlimit:
            value = hlimit
        if value <= llimit:
            value = llimit
        return value
    
    def appendMeasure(self):
        # self.fakeBuff.append(self.read())
        for gen in self.generator:
            getattr(self, gen["name"]).append(self.read(gen["mu"], gen["sigma"], gen["limit"]["lower"], gen["limit"]["higher"]))
    
    def initBuffers(self):
        for gen in self.generator:
            setattr(self, gen["name"], deque([], maxlen=self.buffLen))


if __name__ == '__main__':
    def on_read(sensor):
        import statistics
        value = statistics.median(list(sorted(sensor.pm2p5Buff)))
        timestamp = str(np.datetime64('now'))
        print("%s - %.2f"%(timestamp, value))
    def lol(sensor):
        print(sensor.name)
    generator = [
                    {   
                        "name"      : "pm2p5Buff",
                        "mu"        : 120,
                        "sigma"     : 100,
                        "limit"     :{
                            "lower"         : 0,
                            "higher"        : 1000
                        }
                    },
                    {   
                        "name"      : "pm10Buff",
                        "mu"        : 120,
                        "sigma"     : 100,
                        "limit"     :{
                            "lower"         : 0,
                            "higher"        : 1000
                        }
                    }
                ]
    fakeSensor = FakeSensor(on_read = on_read, buffLen=1, period = 1, maxTimeOffset=3, generator = generator)
    fakeSensor.every(5, lol)
    fakeSensor.start()
    fakeSensor.join()