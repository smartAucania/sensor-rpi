#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file sensor.py
# @brief Funcionalidades para los sensores de manera genérica. Contiene la clase Sensor, que hereda de Thread, la cual es usada como padre para todos los sensores, estableciendo un esquema multihilo. Se define una función callback para las mediciones de todos los sensores.
# @see config/config.py
import sys
sys.path.insert(0,'..')
import threading, time, statistics, pigpio, traceback
from threading import Thread
from datetime import datetime
import numpy as np
import pandas as pd
from collections import deque
from config import config

## @class Sensor
# @brief Esta clase hereda de Thread. Es utilizada como padre por todos los sensores. Establece funcionalidades básicas para los sensores, permitiendo la pausa y la reanudación.
class Sensor(Thread):
    ## Nombre por defecto del sensor
    name = "unnamed"
    
    ## @brief Constructor de la clase
    def __init__(self, buffLen = 11, pin = None, on_read = None, period = 10, maxTimeOffset = 15, **kwargs):
        ## Pin gpio para encender o apagar el sensor
        self.pin            = pin                   
        ## Objeto pigpio para acceder a la gpio de la rpi                    
        self.pi             = pigpio.pi()
        ## Tamaño del buffer de mediciones
        self.buffLen 	    = buffLen            
        ## Callback a llamar cuando se hayan completado los buffers
        self.on_read        = on_read                                   
        ## Periodo de tiempo entre lecturas
        self.period         = period                        
        ## Argumentos extra para callback
        self.kwargs         = kwargs                                
        ## Cantidad máxima de tiempo extra que puede tardar en tomar todas las muestras
        self.maxTimeOffset 	= maxTimeOffset		
        ## Semáforo para acceder a recursos del hilo desde afuera
        self.runSem     = threading.Semaphore(1)        
        ## Flag para saber si está ejecutandose
        self.running 	= False			                
        ## Flag para saber si se está pausando
        self.pausing 	= False
        ## Flag para saber si se está deteniendo
        self.stopping 	= False
        #Inicializa hilo
        Thread.__init__(self)

        self.cbks = deque([], maxlen=20)

    ## @brief Inicialización de buffers de mediciones        
    def initBuffers(self):
        self.tempBuff   = deque([], maxlen=self.buffLen)
    
    ## @brief Realiza una medición y la almacena en el buffer
    def appendMeasure(self):
        pass
    
    def every(self, seconds, fun):
        self.cbks.append({"period": seconds, "cbk": fun, "st": time.time()})
    
    def powerOn(self):
        if not self.pin is None:
            self.pi.write(self.pin, 1)
    
    def powerOff(self):
        if not self.pin is None:
            self.pi.write(self.pin, 0)
    
    def isPowered(self):
        if not self.pin is None:
            return self.pi.read(self.pin) == 1
        return False
    
    def processCallbacks(self):
        #Ejecuta callbacks periodicos
        for cbk in self.cbks:
            if time.time()-cbk["st"] > cbk["period"]:
                try:
                    cbk["cbk"]()
                except:
                    traceback.print_exc()
                cbk["st"] += cbk["period"]
            
    ## @brief Rutina del sensor.
    def run(self):
        #Activa flag para saber que el hilo se está ejecutando
        self.startSensor()

        while not self.isStopping():
            #Marca de tiempo inicial
            st = time.time()
            while not self.isPausing():               

                #Inicialización de buffer de mediciones
                self.initBuffers()
    
                nreads = 1
                while(nreads<=self.buffLen and time.time()-st <= self.period*self.buffLen + self.maxTimeOffset):
                    self.processCallbacks()

                    #Duerme periodo de tiempo para siguiente muestra
                    if time.time()-st <self.period*(nreads+1):
                        time.sleep(0.1)
                        continue
                    
                    #Enciende el sensor
                    self.powerOn()

                    #Adquiere control del hilo
                    self.startCritical()

                    try:
                        #Obtiene y almacena nuevas medicionse
                        self.appendMeasure()
                    except:
                        traceback.print_exc()

                    #Libera control del hilo
                    self.stopCritical()

                    #Apaga el sensor
                    self.powerOff()

                    #Actualiza contador de lecturas
                    nreads = nreads + 1

                    #Libera control del hilo
                    self.stopCritical()

                    #Muestra por consola valor de temperatura
                    # print("Temperatura raspberry %s: %.2f"%(str(np.datetime64('now')).replace('T', ' '), self.tempBuff[i]))
                    
                #Adquiere control del hilo
                Sensor.startCritical(self)

                if not self.on_read is None:
                    try:
                        #Llama callback
                        self.on_read(self, **self.kwargs)
                    except:
                        traceback.print_exc()

                #Libera control del hilo
                Sensor.stopCritical(self)

                #Actualiza tiempo inicial
                st = st + self.buffLen*self.period

                #Corrige tiempo inicial en caso de existir gap
                if st+60*60<time.time():
                    st = time.time()

                #Espera a siguiente período de muestreo
                while(time.time() < st):
                    time.sleep(0.1)
                    self.processCallbacks()
                
            #Avisa que ha terminado de ejecutarse el hilo
            self.endSensor()

            #Espera a que se requiera nuevamente la ejecución del hilo
            self.waitForActivation()
    
    ## @brief Establece que el hilo ha comenzado a ejecutarse
    def startSensor(self):
        #Adquiere control del hilo
        self.startCritical()

        #Actualiza estado del sensor
        self.running = True

        #Libera control del hilo
        self.stopCritical()
    
    ## @brief Establece que el hilo se ha detenido
    def endSensor(self):
        #Adquiere control del hilo
        self.startCritical()

        #Actualiza estado del sensor
        self.pausing 	= False
        self.running 	= False
        self.stopping   = False

        #Libera control del hilo
        self.stopCritical()
    
    ## @brief Espera a que el hilo reanude su ejecución
    def waitForActivation(self):
        #Espera a que se requiera la ejecución del hilo
        while(not self.isRunning()):
            time.sleep(1)
    
    ## @brief Ordena pausar el hilo en el próximo ciclo.
    def pause(self):
        #Adquiere control del hilo
        Sensor.startCritical(self)

        self.pausing = True

        #Libera control del hilo
        Sensor.stopCritical(self)
    
    ## @brief Pregunta si el hilo se está pausando
    #
    # @params[out] stopping Bandera que indica si el hilo se está pausando
    def isPausing(self):
        return self.pausing

    ## @brief Pregunta si el hilo se está deteniendo
    #
    # @params[out] stopping Bandera que indica si el hilo se está deteniendo
    def isStopping(self):
        return self.stopping
    
    ## @brief Pregunta si el hilo está ejecutandose
    #
    # @params[out] running Bandera que indica si el hilo se está ejecutando
    def isRunning(self):
        return self.running

    ## @brief Adquiere control del hilo mediante un semáforo
    def startCritical(self):
        self.runSem.acquire()
    
    ## @brief Libera control del hilo mediante un semáforo
    def stopCritical(self):
        self.runSem.release()
    
    ## @brief Ordena reanudar el hilo en el próximo ciclo.
    def resume(self):
        #Adquiere control del hilo
        Sensor.startCritical(self)

        self.running = True

        #Libera control del hilo
        Sensor.stopCritical(self)
    
    def stop(self):
        #Adquiere control del hilo
        Sensor.startCritical(self)

        self.stopping = True

        #Libera control del hilo
        Sensor.stopCritical(self)

class SensorI2C(Sensor):
    SEM_I2C = threading.Semaphore(1)

    def __init__(self, buffLen = 11, pin = None, on_read = None, period = 10, maxTimeOffset = 15, **kwargs):
        Sensor.__init__(self, pin = pin, buffLen = buffLen, on_read = on_read, period = period, maxTimeOffset = maxTimeOffset, **kwargs)
        self.powered = self.isPowered()
    
    def powerOn(self):
        self.powered = self.isPowered()
        Sensor.powerOn(self)

    def powerOff(self):
        if(not self.powered):
            Sensor.powerOff(self)
    
    ## @brief Adquiere control del bus I2C
    def startI2CCritical(self):
        self.SEM_I2C.acquire()
    
    ## @brief Libera control del bus I2C
    def stopI2CCritical(self):
        self.SEM_I2C.release()
    
    def startCritical(self):
        Sensor.startCritical(self)
        self.startI2CCritical()
    
    def stopCritical(self):
        Sensor.stopCritical(self)
        self.stopI2CCritical()

## @brief Función callback para los sensores. Genera paquetes de telemetría con las mediciones. Informa si no se ha podido medir alguna variable de algun sensor.
# @param[in] sensorObj Sensor que llama a la función luego de medir
# @param[in] **kwargs Colas multiproceso para envíar telemetrias e informar de fallas en la medición.
# 						kwargs es un diccionario con el siguiente formato: {"queue": q, "stateQueue": rgbqueue}. Donde "queue" es la cola para publicación de telemetrías y 
# 						"stateQueue" es la cola para la actualización de estado.
def onRead(sensorObj, **kwargs):
    if "queue" in kwargs.keys():
        #Obtiene cola multiproceso
        q 			= kwargs["queue"]

        #Obtiene manejador estado dispositivo
        stateQ	    = kwargs["stateQueue"]

        #Obtiene logger
        logger      = kwargs["logger"]

        #Obtiene marca de tiempo en unix y timestamp str
        #Thingsboard acepta tiempo unix en milisegundos
        unix 		= np.datetime64(datetime.utcnow(), dtype='datetime64[ms]').astype('float64')/1000 
        timestamp 	= str(np.datetime64('now')).replace('T', ' ')

        #Carga configuraciones bateria
        battery = config.getBattery()

        #Carga configuraciones del sensor
        sensor = config.getSensor(sensorObj.name)
        if not sensor is None:
            measuring = True
            for measurement in sensor["measurements"]:
                #Obtiene valor de la medición en base a mediana
                try:
                    buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))
                except:
                    traceback.print_exc()
                    logger.error("El sensor %s no posee el atributo %s"%(sensorObj.name, measurement["obj_attr_name"]))
                    continue
                    
                if len(buff)>0:                                                         
                    #Extrae valores centrales entre los cuantiles 0.25 y 0.75
                    buff            = np.array(buff)
                    df              = pd.DataFrame({"buff": buff})                    
                    # buff            = df["buff"].where(df["buff"] <= df["buff"].quantile(0.75) & df["buff"] >= df["buff"].quantile(0.25))
                    buff2           = df.buff[df.buff.le(df.buff.quantile(0.75)) & df.buff.ge(df.buff.quantile(.25))]

                    if len(buff2)>0:
                        buff = buff2.tolist()

                    #Extrae promedio de las mediciones
                    value 									= statistics.mean(buff)

                    #Construye paquete completo de telemetria, basandose en plantilla de configuración
                    telemetry 								= measurement["format"].copy()
                    tipo = ':'.join(telemetry["tipo"].split(':')[:-1])
                    telemetry["medicion:%s"%tipo] 			= telemetry["medicion:%s"%tipo]%(value)
                    telemetry["fecha:%s"%tipo] 				= "%d"%int(unix)

                    #Construye paquete simple de telemetria
                    sensorData 									= dict()
                    sensorData["values"] 						= dict()
                    sensorData["values"][measurement["name"]] 	= telemetry["medicion:%s"%tipo]
                    sensorData["ts"] 							= unix

                    #Coloca la medición en formato simple y completo en la cola para poder ser procesada por los publicadores
                    print("\nSensor: measurement ", measurement)
                    print()
                    q.put({"simple": sensorData, "complete": telemetry, "measurement": measurement})

                    #Verifica estado de las baterias
                    if sensor["name"] == battery["sensor"] and measurement["name"] == battery["field"]:
                        if value >= 4.08:
                            stateQ.put({
                                "attribute": "battery-level",
                                "state": "full"
                            })
                        elif value >= 3.5:
                            stateQ.put({
                                "attribute": "battery-level",
                                "state": "medium"
                            })
                        else:
                            stateQ.put({
                                "attribute": "battery-level",
                                "state": "low"
                            })
                    #Manda mensaje de debugueo
                    logger.info("%s | Data obtenida de %s. %s: %s"%(timestamp, sensor["name"], measurement["name"], telemetry["medicion:%s"%tipo]))
                else:
                    #No hay mediciones
                    measuring = False
            if not measuring:
                #Indica que existe un sensor con al menos una medición que no se está realizando
                stateQ.put({
                                "attribute": "measuring",
                                "state": False,
                                "sensor": sensor["name"]
                            })
        else:
            logger.warning("Configuración para sensor %s no encontrada"%(sensorObj.name))
    else:
        logger.error("No existe cola en la que publicar mediciones")
