#!/usr/bin/env python
# -*- coding: utf-8 -*

# TODO
# Es necesario tomar en cuenta que los dispositivos son dependientes de la energia.
# Que pasa si estamos actualizando y el dispositivo se apaga.

# paquete  |s/a|CMD|LENGTH|MSG
#s/a= sin asignar (3 bits) 
#CMD = comandos (5 bits)
#short_ID= id corta del nodo
#MSG= mensaje a enviar (LENGTH byts)
#FCS=    2 bytes
from Crypto.Cipher import AES
import struct
import time

#       Tipos de paquete
#******************************
# MAX CANT COMANDOS: 31
#
#** Telemetria (nodo a coordinador)
#** Solicitud de ingreso a la red (nodo a coordinador) => se debe informar version de sw del dispositivo
#** Solicitud de ingreso aprobada (coordinador a nodo)
#** PAN ID (coordinador a nodos)
#** ACK (bidireccional) Todas van con la hora fecha actual.
#NO ACK - Shut Up (coordinador a nodo)
#** Polling (nodo a coordinador)
#** Paquete de actualizacion de software (coordinador a nodo)
#** Solicitud de paquete de actualizacion (nodo a coordinador)
#Solicitud de desconexion (nodo a coordinador) 

## polling responses.
#** Nueva version de software(coordinador a nodo)
#** Actualización de configuración
#Solicitud de reinicio
#Obtener estadisticas

#Comandos
CMD_TELEMETRY           = 0x00
CMD_REQ_ASSOCIATE       = 0x01
CMD_ASSOCIATE_ACCEPT    = 0x02
CMD_PAN_ID              = 0x03
CMD_ACK                 = 0x04
CMD_SHUT_UP             = 0x05
CMD_POLLING             = 0x06
CMD_UPDATE_PACKET       = 0x07
CMD_REQ_UPDATE          = 0x08
CMD_NEW_VERSION         = 0x09
CMD_NEW_CONFIG          = 0x0A
CMD_REBOOT              = 0x0B
CMD_STATISTICS          = 0x0C
CMD_POLLING_EMPTY_RSP   = 0x0D      
CMD_UPDATE_VERSION      = 0x0E
CMD_REQ_UPDATE_STREAM   = 0x0F
CMD_NO_FILES_LEFT_UP    = 0x10
CMD_ASK_FILES_LEFT      = 0x11
CMD_DISCOVERY           = 0x12

#Mediciones
MEAS_HPMA115S0_PM2P5    = 0x00 #2 bytes
MEAS_HPMA115S0_PM10     = 0x01 #2 bytes
MEAS_PMS7003_PM2P5      = 0x02 #2 bytes
MEAS_PMS7003_PM10       = 0x03 #2 bytes
MEAS_SPS30_PM2P5        = 0x04 #4 bytes
MEAS_SPS30_PM10         = 0x05 #4 bytes
MEAS_AM2315_TEMP        = 0x06 #4 bytes
MEAS_AM2315_HUM         = 0x07 #4 bytes
MEAS_INA219_CURRENT     = 0x08 #4 bytes
MEAS_INA219_VOLTAGE     = 0x09 #4 bytes
MEAS_INA219_POWER       = 0x0A #4 bytes
MEAS_CPU_TEMP           = 0x0B #4 bytes
MEAS_GPS_LAT            = 0x0C #4 bytes
MEAS_GPS_LON            = 0x0D #4 bytes
MEAS_RSSI               = 0x0E #4 bytes
MEAS_SNR                = 0x0F #4 bytes
END = 0xFF

SENSORS = {
    MEAS_HPMA115S0_PM2P5:   {"sensor": "hpma115s0", "type": "short", "measure": "pm2p5",            "tel": "HPMA_PM2.5"},
    MEAS_HPMA115S0_PM10:    {"sensor": "hpma115s0", "type": "short", "measure": "pm10",             "tel": "HPM_PM10"},
    MEAS_PMS7003_PM2P5:     {"sensor": "pms7003",   "type": "short", "measure": "pm2p5",            "tel": "PMS_PM2.5"},
    MEAS_PMS7003_PM10:      {"sensor": "pms7003",   "type": "short", "measure": "pm10",             "tel": "PMS_PM10"},
    MEAS_SPS30_PM2P5:       {"sensor": "sps30",     "type": "float", "measure": "pm2p5",            "tel": "SPS_PM2.5"},
    MEAS_SPS30_PM10:        {"sensor": "sps30",     "type": "float", "measure": "pm10",             "tel": "SPS_PM10"},
    MEAS_AM2315_TEMP:       {"sensor": "am2315",    "type": "float", "measure": "temperature",      "tel": "AM2315_TEMP"},
    MEAS_AM2315_HUM:        {"sensor": "am2315",    "type": "float", "measure": "humidity",         "tel": "AM2315_HUM"},
    MEAS_INA219_CURRENT:    {"sensor": "ina219",    "type": "float", "measure": "current",          "tel": "INA219_CURR"},
    MEAS_INA219_VOLTAGE:    {"sensor": "ina219",    "type": "float", "measure": "voltage",          "tel": "INA219_VOLT"},
    MEAS_INA219_POWER:      {"sensor": "ina219",    "type": "float", "measure": "power",            "tel": "INA219_POW"},
    MEAS_CPU_TEMP:          {"sensor": "esp32",     "type": "float", "measure": "temperature",      "tel": "INT_TEMP"},
    MEAS_GPS_LAT:           {"sensor": "gps",       "type": "float", "measure": "lat",              "tel": "LAT"},
    MEAS_GPS_LON:           {"sensor": "gps",       "type": "float", "measure": "lon",              "tel": "LON"},
    MEAS_RSSI:              {"sensor": "lora",      "type": "int",   "measure": "rssi",             "tel": "ORIG_RSSI"},
    MEAS_SNR:               {"sensor": "lora",      "type": "float", "measure": "snr",              "tel": "ORIG_SNR"}
}

class Measurement:    
    def __init__(self, meas_id = MEAS_HPMA115S0_PM10, value = 0, timestamp = 0 ):
        self.sensor_name    = SENSORS[meas_id]["sensor"]
        self.value          = value
        self.value_type     = SENSORS[meas_id]["type"]
        self.name           = SENSORS[meas_id]["measure"]
        self.meas_id        = meas_id
        self.timestamp      = timestamp
        self.tel_name       = SENSORS[meas_id]["tel"]
    
    def __str__(self):
        return "Sensor: %s\nMedicion: %s\nValor: %f\n"%(self.sensor_name, self.name, self.value)

def __pack_meas(meas):
    data = []
    data.append(meas.meas_id)
    value = [0]
    timestamp = struct.pack("i", meas.timestamp)
    data.extend(timestamp)
    if meas.value_type == "short":
        value = bytes([(meas.value>>8) & 0xFF, meas.value & 0xFF])
    elif meas.value_type == "float":
        value = struct.pack('f', meas.value)
    elif meas.value_type == "int":
        value = struct.pack('i', meas.value)
    data.extend(value)
    return data

def __unpack_meas(payload):
    i = 0
    
    if len(payload) < 2:
        print("No se puede desempaquetar debido a que el largo es muy corto")
        return
    
    measurements = []
    while True:
        if not len(payload) > i+2:
            return measurements
        meas_id = payload[i]
        meas = Measurement(meas_id = meas_id)
        print("timestamp ",payload[i+1:i+1+4])
        meas.timestamp = struct.unpack("i", bytes(payload[i+1:i+1+4]))[0]
        if meas.value_type == "short":
            meas.value = ((payload[i+1+4] & 0xFF) << 8) | (payload[i+2+4] & 0xFF)
            i += 7
        elif meas.value_type == "float":
            meas.value = struct.unpack("f", bytes(payload[i+1+4:i+1+4+4]))[0]
            i += 9
        elif meas.value_type == "int":
            meas.value = struct.unpack("i", bytes(payload[i+1+4:i+1+4+4]))[0]
            i += 9
        else:
            raise Exception("No es posible desempaquetar. El tipo de dato de la medición no es conocido.")
        measurements.append(meas)

def __checksum(data):
    crc = sum(data)
    if crc & 0xFF == END:
        crc -= 1
    return crc

def __fill(data, length):
    remainder = length - len(data)
    if length < len(data):
        remainder = length - len(data)%length
    if remainder > 0:
        data.extend([0xFF]*remainder)
    return data

def __unfill(data):
    return data[:data.index(END)]

def __bytes_to_str(data):
    return ''.join([chr(c) for c in data])

def __str_to_bytes(data):
    return bytes([ord(c) for c in data])

def __pack_payload(cmd, data, key):
    msg = []

    #Crea el objeto encriptador
    key             = __fill(key, 16)                                                   #Rellena la llave para completar el largo requerido por el encriptador
    cryptor         = AES.new(key, AES.MODE_ECB)                                        #Crea el objeto encriptado

    if cmd == CMD_TELEMETRY:
        
        for meas in data["measurements"]:
            msg.extend(__pack_meas(meas))
    elif cmd == CMD_REQ_ASSOCIATE    :
        ## Debe contener: 
        # MAC o unique id
        # Key proporcionada al usuario
        # Version del sw
        # msg.extend(__str_to_bytes(data["mac"]))
        # msg.extend(bytes([pan_id>>8 &0xFF, pan_id&0xFF]))
        msg.extend(__str_to_bytes(data["key"]))
        msg.extend(__str_to_bytes(data["version"]))
    elif cmd == CMD_ASSOCIATE_ACCEPT :
        #Se adjunta short id
        #Se adjunta fecha hora
        # msg.extend(bytes([pan_id>>8 &0xFF, pan_id&0xFF]))
        msg.extend(bytes([data["short id"]]))
        msg.extend(struct.pack('i', data["date"]))
    elif cmd == CMD_PAN_ID           :
        #Adjunta Identificador de la red
        pan_id = data["pan id"]
        msg.extend(bytes([pan_id>>8 &0xFF, pan_id&0xFF]))
        msg.extend(__str_to_bytes(data["network"]))
    elif cmd == CMD_ACK              :
        #Adjunta identificador del paquete
        packet_id = data["packet id"]
        msg.extend(bytes([packet_id>>8 & 0xFF, packet_id&0xFF]))
    elif cmd == CMD_SHUT_UP          :
        #Sin data
        pass
    elif cmd == CMD_POLLING          :
        #Sin data
        pass
    elif cmd == CMD_POLLING_EMPTY_RSP:
        msg.extend(struct.pack('i', data["date"]))
        msg.extend([0,0])
    elif cmd == CMD_UPDATE_PACKET    :
        #Seccion de archivo en bytes
        _id = data["id"]
        msg.extend(bytes([_id>>8 &0xFF, _id&0xFF]))
        msg.extend(bytes(data["data"]))
    elif cmd == CMD_REQ_UPDATE       :
        _id = data["id"]
        msg.extend(bytes([_id>>8 &0xFF, _id&0xFF]))
        msg.extend(__str_to_bytes(data["filename"]))
    elif cmd == CMD_REQ_UPDATE_STREAM:
        msg.extend(__str_to_bytes(data["filename"]))
        block     = data["block"]
        msg.extend(bytes([block>>8 & 0xFF, block&0xFF]))
    elif cmd == CMD_NEW_VERSION      :
        #Version
        #Cantidad de paquetes
        msg.extend(__str_to_bytes(data["version"]))
        msg.extend(__str_to_bytes(data["filename"]))
        npackets    = data["npackets"]
        nblocks     = data["nblocks"]
        msg.extend(bytes([npackets>>8 & 0xFF, npackets&0xFF]))
        msg.extend(bytes([nblocks>>8 & 0xFF, nblocks&0xFF]))
    elif cmd == CMD_UPDATE_VERSION:
        msg.extend(__str_to_bytes(data["version"]))
        msg.extend(__str_to_bytes(data["filename"]))
    elif cmd == CMD_NO_FILES_LEFT_UP :
        pass
    elif cmd == CMD_ASK_FILES_LEFT   :
        pass
    elif cmd == CMD_NEW_CONFIG       :
        #sin definir
        pass
    elif cmd == CMD_REBOOT           :
        #Sin datos
        pass
    elif cmd == CMD_DISCOVERY        :
        #Sin datos
        pass
    elif cmd == CMD_STATISTICS       :
        #Paquetes enviados
        #Paquetes enviados correctamente
        #Paquetes recibidos
        #Paquetes recibidos correctamente
        #RSSI promedio
        #SNR promedio
        pass
    
    msg             = __pack_slip(msg)
    
    #Encripta el mensaje
    if not cmd == CMD_REQ_ASSOCIATE and not cmd == CMD_PAN_ID and not cmd == CMD_DISCOVERY:
        cryptor         = AES.new(key, AES.MODE_ECB)                                        #Crea el objeto encriptador
        # l = len(msg)
        msg 			= __fill(list(msg), len(key))                                       #Rellena el payload para completar el largo requerido por el encriptador
        # print("prev length: %d. Length: %d. Expected length: %d"%(l, len(msg), len(key)))
        msg_encrypt 	= cryptor.encrypt(bytes(msg))                                          #Crea el objeto encriptador
    else:
        msg_encrypt     = msg
    return (msg, msg_encrypt)

def pack(pan_id = 0xFFFF, packet_id = 0x0000, short_id = 0x00, cmd = 0x00, key = b"1234567890123456", data = None):
    sa  = 0
    if(not(sa<=7 and cmd<=31)):
        raise Exception('CMD fuera de rango')

    #Construye el mensaje
    msg, msg_encrypt  = __pack_payload(cmd, data, key)

    if len(msg_encrypt)>240:
        raise Exception("El largo del mensaje encriptado supera los 240 caracteres")
    
    #Calcula el checksum
    # crc 			= __checksum(msg)                                                   #Calcula el checksum

    #Construye el paquete
    packet          = []
    packet.append((pan_id >> 4) & 0xFF)
    packet.append(pan_id<<4 & 0xF0 | (packet_id >> 8) & 0x0F)
    packet.append(packet_id & 0xFF)
    packet.append(cmd | (sa<<5))
    packet.append(short_id)
    packet.extend(msg_encrypt)
    # packet.extend([(crc >> 8) & 0xff, crc & 0xff])

    #Retorna el paquete
    return  packet

def __pack_slip(data):
    buff = []
    for byte in data:
        if byte == 0xC0:
            buff.extend([0xDB, 0xDC])
        elif byte == 0xDB:
            buff.extend([0xDB, 0xDD])
        else:
            buff.append(byte)
    buff.append(0xC0)
    return buff

def __unpack_slip(data):
    buff = []
    i = 0
    while True:
        if data[i] == 0xDB:
            i += 1
            if data[i] == 0xDC:
                buff.append(0xC0)
            elif data[i] == 0xDD:
                buff.append(0xDB)
        elif data[i] == 0xC0:
            return buff
        else:
            buff.append(data[i])
        i += 1
    return buff

def __unpack_payload(cmd, msg):
    data = dict()
    if cmd == CMD_TELEMETRY:
        data["telemetry"] = {
            "measurements": __unpack_meas(msg)
        }
    elif cmd == CMD_REQ_ASSOCIATE    :
        ## Debe contener: 
        # MAC o unique id
        # Key proporcionada al usuario
        # Version del sw
        data["associate request"] = {
            # "mac": __bytes_to_str(msg[:6]),
            "key": __bytes_to_str(msg[:32]),
            "version": __bytes_to_str(msg[32:])
        }
    elif cmd == CMD_ASSOCIATE_ACCEPT :
        #Se adjunta short id
        #Se adjunta fecha hora
        data["associate accept"] = {
            "short id": int(msg[0]),
            "date": struct.unpack("i", bytes(msg[1:5]))[0]
        }
    elif cmd == CMD_PAN_ID           :
        #Adjunta Identificador de la red
        #Nombre de la red
        data["PAN"] = {
            "pan id": int(msg[0]<<8 | msg[1]),
            "network": __bytes_to_str(msg[2:])
        }
    elif cmd == CMD_ACK              :
        #Adjunta identificador del paquete
        data["ack"] = {
            "packet": int(msg[0] << 8 | msg[1])
        }
    elif cmd == CMD_SHUT_UP          :
        #Sin data
        pass
    elif cmd == CMD_POLLING          :
        #Sin data
        pass
    elif cmd == CMD_POLLING_EMPTY_RSP:
        data = {
            "date": struct.unpack("i", bytes(msg[0:4]))[0],
            "npackets": int(msg[-2]<<8 | msg[-1])            
        }
    elif cmd == CMD_UPDATE_PACKET    :
        #Seccion de archivo en bytes
        data["update packet"] = {
            "id": int(msg[0]<<8 | msg[1]),
            "data": msg[2:]
        }
    elif cmd == CMD_REQ_UPDATE       :
        #Sin datos
        print(msg)
        data["req update packet"] = {
            "id": int(msg[0]<<8 | msg[1]),
            "filename": __bytes_to_str(msg[2:]),
        }
    elif cmd == CMD_NEW_VERSION      :
        #Version
        #Archivo
        #Cantidad de paquetes
        data["new version"] = {
            "version": __bytes_to_str(msg[:5]),
            "filename": __bytes_to_str(msg[5:-4]),
            "npackets": int(msg[-4]<<8 | msg[-3]),
            "nblocks":  int(msg[-2]<<8 | msg[-1])
        }
    elif cmd == CMD_UPDATE_VERSION      :
        data["update version"] = {
            "version": __bytes_to_str(msg[:5]),
            "filename": __bytes_to_str(msg[5:])
        }
    elif cmd == CMD_REQ_UPDATE_STREAM:
        data["req update stream"] = {
            "filename": __bytes_to_str(msg[:-2]),
            "block": int(msg[-2]<<8 | msg[-1])
        }
    elif cmd == CMD_NO_FILES_LEFT_UP :
        pass
    elif cmd == CMD_ASK_FILES_LEFT   :
        pass
    elif cmd == CMD_NEW_CONFIG       :
        #sin definir
        pass
    elif cmd == CMD_REBOOT           :
        #Sin datos
        pass
    elif cmd == CMD_DISCOVERY        :
        #Sin datos
        pass
    elif cmd == CMD_STATISTICS       :
        #Paquetes enviados
        #Paquetes enviados correctamente
        #Paquetes recibidos
        #Paquetes recibidos correctamente
        #RSSI promedio
        #SNR promedio
        pass
    
    return data

def unpack_raw(packet):
    pan_id          = (packet[0] << 4) & 0x0FF0 | (packet[1]>>4 & 0x000F)
    packet_id       = (packet[1] << 8) & 0x0F00 | packet[2] & 0xFF
    sa 				= packet[3] >> 5                                                    #Obtiene el campo "sin asignar"
    cmd 			= packet[3] & 0x1f                                                  #Obtiene el campo "comando"
    short_id        = packet[4]
    msg 			= packet[5:] 
    if cmd == CMD_PAN_ID or cmd == CMD_REQ_ASSOCIATE:
        msg             = __unpack_slip(msg)
        msg             = __unpack_payload(cmd, msg)                                                #Obtiene el payload
    return {"s/a": sa, "cmd": cmd, "pan id": pan_id, "data": msg, "packet id": packet_id, "short id": short_id}            #Retorna el mensaje desenpaquetado

def unpack(packet, key = None):
    if key is None:
        return unpack_raw(packet)
    key 			= __fill(key, 16)                                                   #Rellena la llave para completar el largo requerido por el encriptador
    cryptor 		= AES.new(key, AES.MODE_ECB)                                        #Crea el objeto encriptador
    pan_id          = (packet[0] << 4) & 0x0FF0 | (packet[1]>>4 & 0x000F)
    packet_id       = (packet[1] << 8) & 0x0F00 | packet[2] & 0xFF
    sa 				= packet[3] >> 5                                                    #Obtiene el campo "sin asignar"
    cmd 			= packet[3] & 0x1f                                                  #Obtiene el campo "comando"
    short_id        = packet[4]
    msg 			= packet[5:] 
    if not cmd == CMD_REQ_ASSOCIATE and not cmd == CMD_PAN_ID:
        # print("sin desencriptar", [int(c) for c in msg], type(msg), len(msg))
        msg 			= list(cryptor.decrypt(bytes(msg)))                                        #Desencripta el payload
        # print("desencriptado", [int(c) for c in msg], type(msg), len(msg))
    msg             = __unpack_slip(msg)
    # measurements    = __unpack_meas(msg)
    data            = __unpack_payload(cmd, msg)
    # crc 			= (packet[3+length]<<8) | (packet[3+length])                        #Obtiene el crc
    return {"s/a": sa, "pan id": pan_id, "data": data, "packet id": packet_id, "short id": short_id, "cmd": cmd}            #Retorna el mensaje desenpaquetado

if __name__ == '__main__':
    short_id = 0xC0
    packet_id = 0
    pan_id = 0x0ABC

    ## Medicion
    print("Medicion")
    print("********")
    meas = Measurement(meas_id = MEAS_AM2315_HUM, value = 62.5)
    packet = pack(pan_id = pan_id, packet_id = packet_id, short_id = short_id, cmd = CMD_TELEMETRY, data = {"measurements": [meas]}, key = b'esta es la llave')
    data = unpack(packet, b'esta es la llave')
    print(data)
    for meas in data["data"]["telemetry"]["measurements"]:
        print(meas)

    ## Solicitud de asociacion a la red de nuevo dispositivo
    print("\nReq associate new device into the network")
    print("*********************************************")
    packet_id += 1
    mac = "AABBAA"
    key = "12345678901234567890123456789012"
    version = "1234"
    data = {
        "mac": mac,
        "key": key,
        "version": version
    }
    packet = pack(pan_id = pan_id, packet_id = packet_id, short_id = short_id, cmd = CMD_REQ_ASSOCIATE, data = data, key = b'esta es la llave')
    data = unpack(packet, b'esta es la llave')
    print(data)

    ## Solicitud de asociacion a la red de nuevo dispositivo
    print("\nReq associate new device into the network")
    print("*********************************************")
    packet_id += 1
    data = {
        "short id": 0xFA,
        "date": int(time.time())
    }
    packet = pack(pan_id = pan_id, packet_id = packet_id, short_id = short_id, cmd = CMD_ASSOCIATE_ACCEPT, data = data, key = b'esta es la llave')
    data = unpack(packet, b'esta es la llave')
    print(data)

    ## PAN ID
    print("\nPAN ID")
    print("*********")
    packet_id += 1
    data = {
        "pan id": pan_id,
        "network": "Midgar"
    }
    packet = pack(pan_id = pan_id, packet_id = packet_id, short_id = short_id, cmd = CMD_PAN_ID, data = data, key = b'esta es la llave')
    data = unpack(packet, b'esta es la llave')
    print(data)

    ## ACK
    print("\nACK")
    print("****")
    packet_id += 1
    data = {"packet id": 0x1E4F}
    packet = pack(pan_id = pan_id, packet_id = packet_id, short_id = short_id, cmd = CMD_ACK, data = data, key = b'esta es la llave')
    data = unpack(packet, b'esta es la llave')
    print(data)

    ## Polling
    print("\nPolling")
    print("*********")
    packet_id += 1
    data = None
    packet = pack(pan_id = pan_id, packet_id = packet_id, short_id = short_id, cmd = CMD_POLLING, data = data, key = b'esta es la llave')
    data = unpack(packet, b'esta es la llave')
    print(data)

    ## Polling
    print("\nUpdate packet")
    print("*************")
    packet_id += 1
    data = {
        "id": 0x56F2,
        "data": [100,20,56,20,32,15,00,25,201,51,120,25,76,91,31]*3
    }
    packet = pack(pan_id = pan_id, packet_id = packet_id, short_id = short_id, cmd = CMD_UPDATE_PACKET, data = data, key = b'esta es la llave')
    data = unpack(packet, b'esta es la llave')
    print(data)

    ## Solicitud de actualizacion
    print("\nUpdate request")
    print("*************")
    packet_id += 1
    data = None
    packet = pack(pan_id = pan_id, packet_id = packet_id, short_id = short_id, 
                cmd = CMD_REQ_UPDATE, data = {"id": 0x01, "filename": "util.py"}, key = b'esta es la llave')
    print(packet)
    data = unpack(packet, b'esta es la llave')
    print(data)

    ## Nueva actualizacion
    print("\nNew version")
    print("*************")
    packet_id += 1
    data = {
        "version": "11.2",
        "filename": "config/config.json",
        "npackets": 256
    }
    packet = pack(pan_id = pan_id, packet_id = packet_id, short_id = short_id, cmd = CMD_NEW_VERSION, data = data, key = b'esta es la llave')
    data = unpack(packet, b'esta es la llave')
    print(data)