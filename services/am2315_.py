#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file am2315_.py
# @brief Clase con funciones para monitoreo del sensor AM2315 usando esquema multihilo.
# @see Basado en la siguiente biblioteca de github: https://github.com/xtacocorex/Simple_AM2315
# @see config/config.py
# @see services/sensor.py

import sys
sys.path.insert(0,'..')
import threading, statistics, time, pigpio, traceback
from services.sensor import SensorI2C
from am2315 import AM2315
from collections import deque
import numpy as np

##
# Tabla de conexión del sensor con la placa Raspberry Pi 3 B
#    |Raspberry|AM2315|
#   |---|:---:|
#    |GPIO2|SDA|
#   |GPIO3|SCL|
#   |5V|VCC|
#
# El pin de GND del sensor está conectado al drain de un transistor mosfet. El pin GND de la RPI 3 está conectado al source de dicho transistsor. Esto se observa en la siguiente tabla:
# |Raspberry|2N7000|AM2315|
# |:--:|:--:|:--:|
# |GND|Source|---|
# |---|Drain|GND|
#
# <h4>Codigo de ejemplo:</h4>
# @code{.py}
# from config import config
# import statistics, numpy as np
# def onRead(sensorObj):
#     #Obtiene marca de tiempo en unix y timestamp str
#     unix 		= np.datetime64('now').astype('float64')*1000 #Thingsboard acepta tiempo unix en milisegundos
#     timestamp 	= str(np.datetime64('now')).replace('T', ' ')
#     print(timestamp)
#
#     #Carga configuraciones del sensor
#     sensor = config.getSensor(sensorObj.name)
#
#     if not sensor is None:			
#         for measurement in sensor["measurements"]:
#             #Obtiene valor de la medición en base a mediana
#             buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))
#
#             if len(buff)>0:
#                 value 										= statistics.median(buff)
#
#                 #Manda mensaje de debugueo
#                 print("Data obtenida de %s. %s: %s"%(sensor["name"], measurement["name"], repr(value)))
# #Obtiene configuración para el sensor am2315
# amConfig = config.getSensor("am2315")
# if not amConfig is None:
#     from am2315_ import AM2315_
#     #Crea hilo para lectura del sensor am2315
#     am2315 = AM2315_(buffLen = amConfig["buffLen"], pin=amConfig["enable_gpio"], period = amConfig["period"], sem = None, on_read = onRead)
#     #Comienza lectura del sensor
#     am2315.start()
# @endcode
#
# @class AM2315_
class AM2315_(SensorI2C):

    ## Nombre del sensor.
    name                = "am2315"      
    ## Límite inferior de humedad relativa.
    HUM_LOWER_LIMIT     = 0             
    ## Límite superior de humedad relativa.
    HUM_UPPER_LIMIT     = 100           
    ## Límite inferior de temperatura.
    TEMP_LOWER_LIMIT    = -20           
    ## Límite superior de temperatura.
    TEMP_UPPER_LIMIT    = 80            

    ## @brief Constructor de la clase.
    # @param[in] buffLen Tamaño de los buffers de mediciones.
    # @param[in] pin Pin GPIO de activación del sensor.
    # @param[in] period Período de tiempo entre mediciones.
    # @param[in] on_read Callback a llamar al llenarse los buffers.
    # @param[in] nattempts Cantidad de reintentos por medición.
    # @param[in] maxTimeOffset Cantidad de tiempo máximo entre que termina el período de medición actual y comienza el siguiente.
    # @param[in] kwargs Argumentos extra a ingresar al llamar al callback.
    def __init__(self, buffLen = 11, pin=4, period = 60, on_read = None, nattempts = 3, maxTimeOffset = 15,  **kwargs):
        ## Tamaño del buffer de mediciones.
        self.buffLen        = buffLen                   
        ## Periodo de tiempo entre mediciones.
        self.period         = period                     
        ## Objeto pigpio acceso a gpio.
        self.pi             = pigpio.pi()               
        ## Pin destinado a encender y apagar el sensor transistorizado.
        self.pin            = pin                       
        ## Callback a llamar cada vez que se llenan los buffers de mediciones.
        self.on_read        = on_read                   
        ## Argumentos extra a pasar a la función callback.
        self.kwargs         = kwargs                    
        ## Cantidad de intentos de lectura por medición.
        self.nattempts      = nattempts                         
        ## Cantidad máxima de tiempo extra que puede tardar en tomar todas las muestras.
        self.maxTimeOffset 	= maxTimeOffset             
        self.logger         = kwargs.get("logger", None)
        #Inicializa hilo
        SensorI2C.__init__(self, pin = pin, buffLen = buffLen, on_read = on_read, period = period, maxTimeOffset = maxTimeOffset, **kwargs)
    
    def appendMeasure(self):
        if self.read():
            temp    = list(self.tempBuff)
            hum     = list(self.humBuff)
            if not self.logger is None:
                self.logger.info("AM2315 obtiene mediciones. Temperatura: %.2f °C. Humedad: %.2f %%"%(temp[-1], hum[-1]))
            else:
                print("AM2315 obtiene mediciones. Temperatura: %.2f °C. Humedad: %.2f %%"%(temp[-1], hum[-1]))
        # self.readFake()
    
    def readFake(self):
        self.tempBuff.append(np.random.normal(45, 2, 1)[0])
        self.humBuff.append(np.random.normal(50, 5, 1)[0])

    def read(self):
        ## Objeto am2315 de la libreria Simple_AM2315
        self.am2315         = AM2315.AM2315()           

        #Inicializa cantidad de intentos realizados
        attempt = 0

        while attempt < self.nattempts:
            try:
                #Realiza lectura de temperatura y humedad
                self.tempBuff.append(self.am2315.read_temperature())
                self.humBuff.append(self.am2315.read_humidity())
                return True
            except:
                traceback.print_exc()
                print("Error al leer sensor am2315")

                #Actualiza contador de intentos
                attempt += 1

                #Duerme un segundo hasta el siguiente intento
                time.sleep(1)
        if not self.logger is None:
            self.logger.warn("No se pudo leer el sensor AM2315 luego de %d intentos"%attempt)
        else:
            print("No se pudo leer el sensor AM2315 luego de %d intentos"%attempt)
        #Cierra bus I2C
        self.am2315._device._bus.close()

        return False

    def initBuffers(self):
        ## Buffer de mediciones de temperatura.
        self.tempBuff   = deque([], maxlen=self.buffLen)
        ## Buffer de mediciones de humedad relativa.
        self.humBuff    = deque([], maxlen=self.buffLen)

    @property
    def humidity(self):
        try:
            return statistics.median(list(sorted(self.humBuff)))
        except:
            return None

    @property
    def temperature(self):
        try:
            return statistics.median(list(sorted(self.tempBuff)))
        except:
            return None
    # ## @brief Rutina del sensor.
    # def run(self):
    #     #Activa flag para saber que el hilo se está ejecutando
    #     self.startSensor()

    #     while True:
    #         #Marca tiempo de inicio
    #         st = time.time()
    #         while True and not self.isStopping():
    #             #Inicializa buffers
    #             self.tempBuff   = deque([], maxlen=self.buffLen)
    #             self.humBuff    = deque([], maxlen=self.buffLen)
                
    #             for i in range(self.buffLen):

    #                 #Duerme hasta que sea necesario muestrear
    #                 if time.time()-st <self.period*(i+1):
    #                     time.sleep(self.period*(i+1)-(time.time()-st))
                    
    #                 self.startCritical()

    #                 #Pide recurso I2C
    #                 self.startI2CCritical()

    #                 #Enciende sensor i2c
    #                 val = self.pi.read(self.pin)
    #                 self.pi.write(self.pin, 1)

    #                 ## Objeto am2315 de la libreria Simple_AM2315
    #                 self.am2315         = AM2315.AM2315()           

    #                 #Inicializa cantidad de intentos realizados
    #                 attempt = 1

    #                 while attempt <= self.nattempts:
    #                     try:
    #                         #Realiza lectura de temperatura y humedad
    #                         self.tempBuff.append(self.am2315.read_temperature())
    #                         self.humBuff.append(self.am2315.read_humidity())
    #                         break
    #                     except:
    #                         print("Error al leer sensor am2315")

    #                         #Actualiza contador de intentos
    #                         attempt += 1

    #                         #Duerme un segundo hasta el siguiente intento
    #                         time.sleep(1)
                    
    #                 #Cierra bus I2C
    #                 self.am2315._device._bus.close()

    #                 #Vuelve voltaje sensores i2c a su estado anterior
    #                 self.pi.write(self.pin, val)

    #                 #Libera recurso I2C
    #                 self.stopI2CCritical()

    #                 #Libera control del hilo
    #                 self.stopCritical()

    #                 #Muestra por pantalla los valores obbtenidos
    #                 # print("AM2315 %s: Temperatura: %.2f. Humedad: %.2f"%(str(np.datetime64('now')).replace('T', ' '), self.tempBuff[i], self.humBuff[i]))
                
    #             #Adquiere control del hilo
    #             self.startCritical()

    #             #Actualiza última medición de temperatura
    #             if len(self.tempBuff)>0:
    #                 self.temperature    = statistics.median(list(sorted(self.tempBuff)))
                
    #             #Actualiza última medición de humedad
    #             if len(self.humBuff)>0:
    #                 self.humidity       = statistics.median(list(sorted(self.humBuff)))

    #             if not self.on_read is None:
    #                 #Llama callback para procesamiento de los datos
    #                 self.on_read(self, **self.kwargs)
                
    #             #Libera control del hilo
    #             self.stopCritical()

    #             #Actualiza tiempo inicial
    #             st = st + self.buffLen*self.period

    #             #Espera a siguiente período de muestreo
    #             if(time.time() < st):
    #                 time.sleep(st-time.time())
    #         #Avisa que ha terminado de ejecutarse el hilo
    #         self.endSensor()

    #         #Espera a que se requiera nuevamente la ejecución del hilo
    #         self.waitForActivation()
    
    ## @brief Lee humedad relativa.
    # @param[out] humidity Humedad relativa.
    def readHumidity(self):
        #Verifica que la humidity esté en el rango
        humidity = self.am2315.read_humidity()
        if humidity < AM2315_.HUM_LOWER_LIMIT:
            humidity = AM2315_.HUM_LOWER_LIMIT
        if humidity > AM2315_.HUM_UPPER_LIMIT:
            humidity = AM2315_.HUM_UPPER_LIMIT
        return humidity

    ## @brief Lee temperatura (°C).
    # @param[out] temperature Temperatura.
    def readTemperature(self):
        #Verifica que la temperatura esté en el rango
        temperature = self.am2315.read_temperature()
        if temperature < AM2315_.TEMP_LOWER_LIMIT:
            temperature = AM2315_.TEMP_LOWER_LIMIT
        if temperature > AM2315_.TEMP_UPPER_LIMIT:
            temperature = AM2315_.TEMP_UPPER_LIMIT
        return temperature
if __name__ == '__main__':
    from config import config
    import statistics, numpy as np
    def onRead(sensorObj):
        #Obtiene marca de tiempo en unix y timestamp str
        unix 		= np.datetime64('now').astype('float64')*1000 #Thingsboard acepta tiempo unix en milisegundos
        timestamp 	= str(np.datetime64('now')).replace('T', ' ')
        print(timestamp)

        #Carga configuraciones del sensor
        sensor = config.getSensor(sensorObj.name)

        if not sensor is None:			
            for measurement in sensor["measurements"]:
                #Obtiene valor de la medición en base a mediana
                buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))

                if len(buff)>0:
                    value 										= statistics.median(buff)

                    #Manda mensaje de debugueo
                    print("Data obtenida de %s. %s: %s"%(sensor["name"], measurement["name"], repr(value)))
                else:
                    print("Medicion %s vacía por parte del sensor %s"%(measurement["name"], sensor["name"]))
        else:
            print("No se ha encontrado configuración para el sensor %s"%sensorObj.name)
    #Obtiene configuración para el sensor am2315
    amConfig = config.getSensor("am2315")
    if not amConfig is None:
        # from am2315_ import AM2315_
        #Crea hilo para lectura del sensor am2315
        am2315 = AM2315_(buffLen = amConfig["buffLen"], pin=amConfig["enable_gpio"], period = amConfig["period"], on_read = onRead)
        #Comienza lectura del sensor
        am2315.start()