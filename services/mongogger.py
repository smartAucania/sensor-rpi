import pymongo, traceback

class DB:
    def __init__(self, dbname, table):
        self.client     = pymongo.MongoClient('mongodb://localhost:27017/', connect=False)
        self.db         = self.client[dbname]
        self.collection = self.db[table]
    def add(self, data_):
        data = data_.copy()
        if "_id" in data:
            data.pop("_id", None)
        return self.collection.insert_one(data).inserted_id
    def pop(self):
        data = self.collection.find_one()
        self.collection.remove(data)
        if "_id" in data:
            data.pop("_id", None)
        return data
    def remove(self, row):
        data = self.collection.find_one(row)
        if not data is None:
            self.collection.remove(data)
        else:
            print("No se puede remover este registro de la base de datos mongodb. Registro: %s"%(repr(row)))
    def remove_many(self, query):
        try:
            self.collection.delete_many(query)
        except:
            traceback.print_exc()
            return False
        return True
    def update(self, query, row):
        self.collection.update_one(query, {"$set": row})
    def size(self):
        return self.collection.count()
    def getAll(self):
        return self.collection.find()
    def find(self, data):
        return self.collection.find_one(data)