#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file sps30.py
# @brief Contiene clase con funciones para monitoreo del sensor de material particulado Sensirion SPS30 usando esquema multihilo.
# @see config/config.py
# @see services/sensor.py

import pigpio, crcmod, time, threading, binascii, traceback, sys, statistics
import numpy as np
from struct import unpack, pack
from collections import deque
sys.path.insert(0,'..')

from services.sensor import SensorI2C
from lib.i2c import I2C


## @class SPS30
# @code{.py}
# from config import config
# import statistics, numpy as np
# from services.sensorpool import SensorGuard
# def onRead(sensorObj):
#     #Obtiene marca de tiempo en unix y timestamp str
#     unix 		= np.datetime64('now').astype('float64')*1000 #Thingsboard acepta tiempo unix en milisegundos
#     timestamp 	= str(np.datetime64('now')).replace('T', ' ')
#     print(timestamp)
# 
#     #Carga configuraciones del sensor
#     sensor = config.getSensor(sensorObj.name)
# 
#     if not sensor is None:			
#         for measurement in sensor["measurements"]:
#             #Obtiene valor de la medición en base a mediana
#             buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))
# 
#             if len(buff)>0:
#                 value 										= statistics.median(buff)
# 
#                 #Manda mensaje de debugueo
#                 print("Data obtenida de %s. %s: %s"%(sensor["name"], measurement["name"], repr(value)))
# threads = []
# # #Crea semaforo para utilizar i2c
# sem = threading.Semaphore(1)
# 
# #Obtiene configuración para el sensor am2315
# amConfig = config.getSensor("am2315")
# if not amConfig is None:
#     from services.am2315_ import AM2315_
#     #Crea hilo para lectura del sensor am2315
#     am2315 = AM2315_(buffLen = amConfig["buffLen"], pin=amConfig["enable_gpio"], period = amConfig["period"], sem = sem, on_read = onRead)
#     #Comienza lectura del sensor
#     am2315.start()
#     #Añade sensor a la lista
#     threads.append(am2315)
# #Obtiene configuración para el sensor sps30
# spsConfig = config.getSensor("sps30")
# if not spsConfig is None:
#     from sps30 import SPS30
#     #Crea hilo para lectura del sensor sps30
#     sps = SPS30(pin = spsConfig["enable_gpio"], busNum = spsConfig["bus"]["buss_num"], buffLen = spsConfig["buffLen"], on_read = onRead, 
#         period = spsConfig["period"], sem = sem)
#     #Comienza lectura del sensor
#     sps.start()
#     threads.append(sps)
# #Inicia hilo para proteger los sensores manteniendolos en sus rangos de operación
# sensorGuard = SensorGuard(threads)
# sensorGuard.start()
# @endcode
class SPS30(SensorI2C):
    ## Dirección I2C del sensor.
    ADDRESS = 0x69          
    ## Nombre del sensor.
    name    = "sps30"       
    ## Limite superior rango de medición de PM 2.5 y PM 10.
    LIMIT   = 1000          
    
    ## @brief Constructor de la clase.
    # @param[in] buffLen Tamaño de los buffers de mediciones.
    # @param[in] pin Pin GPIO de activación del sensor.
    # @param[in] bussNum Número del bus I2C.
    # @param[in] period Período de tiempo entre mediciones.
    # @param[in] on_read Callback a llamar al llenarse los buffers.
    # @param[in] nattempts Cantidad de reintentos por medición.
    # @param[in] maxTimeOffset Cantidad de tiempo máximo entre que termina el período de medición actual y comienza el siguiente.
    # @param[in] cleanPeriod Período de tiempo en horas entre ejecuciones de función de autolimpieza del sensor.
    # @param[in] kwargs Argumentos extra a ingresar al llamar al callback.
    def __init__(self, pin = 4, busNum = 1, buffLen = 11, on_read = None, period = 10, sem = None, nattempts = 3, cleanPeriod = 100*60*60, maxTimeOffset = 15, **kwargs):
        ## Objeto con funciones IO para bus i2c.
        self.i2c            = I2C(busNum, SPS30.ADDRESS)                
        self.i2c.close()
        ## Objeto con acceso a GPIO.
        self.pi             = pigpio.pi()                               
        ## Pin GPIO de encendido del sensor por medio de transistor.
        self.gpio           = pin                                       
        ## Semáforo I2C.
        self.sem            = sem                                                                                 
        ## Callback para cuando se llenen los buffers de mediciones.
        self.on_read        = on_read                                   
        ## Cantidad de mediciones a realizar antes de llamar al Callback.
        self.buffLen        = buffLen                                   
        ## Periodo de tiempo en segundos entre mediciones.
        self.period         = period                                    
        ## Muestra temporal de PM10.
        self._pm10          = 0.0                                       
        ## Muestra temporal de PM2.5.
        self._pm2p5         = 0.0                                
        ## Argumentos extra para callback.
        self.kwargs         = kwargs                                    
        ## Cantidad de intentos de lectura por medición.
        self.nattempts      = nattempts                                 
        ## Periodo en horas para limpieza del sensor.
        self.cleanPeriod    = cleanPeriod                               
        ## Cantidad máxima de tiempo extra que puede tardar en tomar todas las muestras.
        self.maxTimeOffset 	= maxTimeOffset          
        self.logger         = kwargs.get("logger", None)                   
        
        SensorI2C.__init__(self, buffLen = buffLen, pin = pin,  on_read = on_read, period = period, maxTimeOffset = maxTimeOffset, **kwargs)

        self.startI2CCritical()
        ## Apaga el sensor por medio del transistor.
        self.powerOff()                                                 
        ## Enciende el sensor.
        self.powerOn()                                                  
        ## Obtiene numero serial.
        self.serialStr      = self.serialNumber()                       
        ## Apaga el sensor.
        self.powerOff()
        self.stopI2CCritical()
        
        #Adquiere control del hilo
        # self.startCritical()

        #Limpia el sensor
        # self.clean()

        #Libera control del hilo
        # self.stopCritical()

        self.every(self.cleanPeriod, self.clean)
        if(self.serialStr is None):
            if not self.logger is None:
                self.logger.error("No fue posible leer el número serial del sensor SPS30")
            else:
                print("No fue posible leer el número serial del sensor SPS30")
        else:
            if not self.logger is None:
                self.logger.info("Sensirion SPS30 N° %s iniciado"%self.serialStr)
            else:
                print("Sensirion SPS30 N° %s iniciado"%self.serialStr)
    
    def appendMeasure(self):
        if(not self.read()):
            if not self.logger is None:
                self.logger.warn("Sensor sensirion sps30 no ha podido realizar la lectura de las mediciones.")
            else:
                print("Sensor sensirion sps30 no ha podido realizar la lectura de las mediciones.")
        else:
            # self.stdtol = 600
            # pm2p5 = list(self.pm2p5Buff) + [self.pm2p5()]
            # pm10 = list(self.pm10Buff) + [self.pm10()]
            # print("len: %d"%len(pm2p5))
            # if len(pm2p5)>1:
            #     std = (statistics.stdev(pm2p5) + statistics.stdev(pm10))/2
            #     if not self.logger is None:
            #         self.logger.warn("Desviación estandar sps30: %.2f."%(std))
            #     else:
            #         print("Desviación estandar sps30: %.2f."%(std))
            #     if std > self.stdtol and (not self.pm2p5() == 1000 or not self.pm10() == 1000):
            #         if not self.logger is None:
            #             self.logger.warn("Mediciones del sensor sps30 han sido descartadas. Desviación estandar: %.2f. PM2.5: %.2f. PM10: %.2f"%(std, self.pm2p5(), self.pm10()))
            #         else:
            #             print("Mediciones del sensor sps30 han sido descartadas. Desviación estandar: %.2f. PM2.5: %.2f. PM10: %.2f"%(std, self.pm2p5(), self.pm10()))
            # else:
            #     print("len: %d"%len(pm2p5))

            #Acumula mediciones en los buffers
            self.pm2p5Buff.append(self.pm2p5())
            self.pm10Buff.append(self.pm10())

            if not self.logger is None:
                self.logger.debug("Sensor sensirion sps30 mide valores: PM2.5: %.2f, PM10: %.2f."%(self.pm2p5(), self.pm10()))
            else:
                print("Sensor sensirion sps30 mide valores: PM2.5: %.2f, PM10: %.2f."%(self.pm2p5(), self.pm10()))
        # self.readFake()
    
    def readFake(self):
        time.sleep(8)
        self.pm10Buff.append(np.random.normal(100, 10, 1)[0])
        self.pm2p5Buff.append(np.random.normal(90, 20, 1)[0])

    def initBuffers(self):
        ## Buffer de mediciones de pm10
        self.pm10Buff 	    = deque([], maxlen = self.buffLen)                 
        ## Buffer de mediciones de pm2.5
        self.pm2p5Buff 	    = deque([], maxlen = self.buffLen)
    
    ## @brief Adquiere control del bus I2C.
    def startI2CCritical(self):
        SensorI2C.startI2CCritical(self)
        try:
            self.i2c.open()
        except:
            pass
        time.sleep(0.1)
    
    ## @brief Libera control del bus I2C.
    def stopI2CCritical(self):
        SensorI2C.stopI2CCritical(self)
        try:
            self.i2c.close()
        except:
            pass
        time.sleep(0.1)
    
    ## @brief Ejecuta función de autolimpieza. Tarda 15s aproximadamente.
    def clean(self):
        if not self.logger is None:
            self.logger.info("Comienza función de autolimpieza del sensor SPS30. Tarda 15s aprox.")

        #Pide recurso I2C
        self.startI2CCritical()
        
        #Enciende el sensor
        self.powerOn()

        #Inicia mediciones
        if(self.startMeasurement()):
            if not self.logger is None:
                self.logger.debug("Duerme 5s despues del encendido del sps30.")

            #Duerme 5 segundos
            time.sleep(5)

            if not self.logger is None:
                self.logger.debug("Realiza función de autolimpieza por 10s.")

            #Comienza limpieza del sensor
            self.startFanCleaning()

            #Duerme 10 segundos
            time.sleep(10)

            #Detiene mediciones
            self.stopMeasurement()

        #Apaga sensor
        self.powerOff()

        #Retorna recurso I2c
        self.stopI2CCritical()
    
    # ## @brief Rutina del sensor.
    # def run(self):
    #     #Marca tiempo de inicio del hilo
    #     lastCleanTime = time.time()

    #     #Activa flag para saber que el hilo se está ejecutando
    #     self.startSensor()
        
    #     while True:
    #         #Marca tiempo de inicio
    #         st = time.time()

    #         while True and not self.isStopping():
    #             #Limpia ventilador cuando sea tiempo
    #             if(time.time() - lastCleanTime > self.cleanPeriod*60*60):
    #                 lastCleanTime = time.time()
    #                 self.clean()

    #             #Inicializa contador de mediciones
    #             nreads = 1

    #             #Inicializa buffers de mediciones
    #             self.pm2p5Buff  = deque()
    #             self.pm10Buff   = deque()

    #             while(nreads<=self.buffLen and time.time()-st <= self.period*self.buffLen + self.maxTimeOffset):
    #                 #Duerme tiempo necesario hasta la siguiente medición
    #                 if time.time()-st < self.period*nreads:
    #                     time.sleep(self.period*nreads-(time.time()-st))
                    
    #                 #Pide el recurso i2c
    #                 self.startI2CCritical()

    #                 #Enciende el sensor
    #                 self.powerOn()

    #                 #Adquiere control del hilo
    #                 self.startCritical()
    #                 try:
    #                     #Envía comando para iniciar mediciones
    #                     if(self.startMeasurement()):
    #                         #Espera tiempo suficiente para una lectura válida
    #                         time.sleep(8)

    #                         #Inicializa contador de intentos de lectura
    #                         attempt = 1

    #                         #Realiza lectura
    #                         while(attempt <= self.nattempts):

    #                             if(self.read()):
    #                                 #Acumula mediciones en los buffers
    #                                 self.pm2p5Buff.append(self.pm2p5())
    #                                 self.pm10Buff.append(self.pm10())
    #                                 break
                                
    #                             #Actualiza contador de intentos
    #                             attempt += 1

    #                         #Actualiza contador de lecturas
    #                         nreads = nreads + 1
                            
    #                         #Envía comando para detener las mediciones
    #                         self.stopMeasurement()
    #                 except Exception as e:
    #                     print(e)

    #                 #Libera control del hilo
    #                 self.stopCritical()

    #                 #Apaga el sensor
    #                 self.powerOff()

    #                 #Muestra por consola los últimos datos leidos
    #                 # if len(self.pm2p5Buff) > 0:
    #                     # print("SPS: %s PM2.5 = %.2f. PM10: %.2f"%(str(np.datetime64('now')).replace('T', ' '), self.pm2p5(), self.pm10()))

    #                 #Libera el I2C
    #                 self.stopI2CCritical()
                
    #             #Adquiere control del hilo
    #             self.startCritical()
    #             if not self.on_read is None:
    #                 self.on_read(self, **self.kwargs)
                
    #             #Libera control del hilo
    #             self.stopCritical()

    #             #Actualiza tiempo inicial
    #             st = st + self.buffLen*self.period

    #             #Espera a siguiente período de muestreo
    #             if(time.time() < st):
    #                 time.sleep(st-time.time())
            
    #         #Avisa que ha terminado de ejecutarse el hilo
    #         self.endSensor()

    #         #Espera a que se requiera nuevamente la ejecución del hilo
    #         self.waitForActivation()
    
    ## @brief Obtiene último valor de PM 2.5.
    # @param[out] _pm2p5 Último valor de PM 2.5.
    def pm2p5(self):
        #Limita valores del pm 2.5 al rango indicado en el datasheet
        if self._pm2p5<0:
            self._pm2p5 = 0
        if self._pm2p5 > SPS30.LIMIT:
            self._pm2p5 = SPS30.LIMIT
        return self._pm2p5

    ## @brief Obtiene último valor de PM 10.
    # @param[out] _pm10 Último valor de PM 10.
    def pm10(self):
        #Limita valores del pm 10 al rango indicado en el datasheet
        if self._pm10 < 0:
            self._pm10 = 0
        if self._pm10 > SPS30.LIMIT:
            self._pm10 = SPS30.LIMIT
        return self._pm10
    
    ## @brief Obtiene número serial del sensor.
    # @param[out] snr Número serial.
    def serialNumber(self):
        data = self.i2c.readBlockFrom([0xD0,0x33], 47)
        if data is None:
            print('serialNumber Falla lectura')
            return None
        snr = ''
        for i in range(47):
            if (i % 3) != 2:
                currentByte = data[i]
                if currentByte == 0:
                    break
                if i != 0:
                    snr += '-'
                snr += chr(currentByte)
        return snr
    
    ## @brief Código del sensor.
    # @param[out] código.
    def articleCode(self):
        data = self.i2c.readBlockFrom([0xD0,0x25], 47)
        if data is None:
            print('articleCode Ha fallado en su lectura')
            return None

        acode = ''
        crcs = ''
        for i in range(47):
            currentByte = data[i]
            if currentByte == 0:
                break
            if (i % 3) != 2:
                acode += chr(currentByte) + '|'
            else:
                crcs += str(currentByte) + '.'
        #print('Article code: "' + acode + '"')
        return acode
    
    ## @brief Configura o retorna el intervalo de tiempo entre autolimpiezas.
    # @todo Aún no se ha implementado esta función. De momento se realiza la limpieza manualmente. @see sps30.SPS30.clean
    # @param[in] interval Intervalo de autolimpiezas.
    # @param[out] interval Intervalo de autolimpiezas.
    def cleaningInterval(self, interval = None):
        if interval is None:
            data = self.i2c.readBlockFrom([0x80, 0x04], 6)
            if not data is None and len(data):
                interval = self._calcInteger(data)
                return interval
            return None
        else:
            pass
    
    ## @brief Ejecuta comando de comienzo de mediciones.
    # @param[out] res Bandera que indica si pudo ejecutar el comando correctamente.
    def startMeasurement(self):
        return self.i2c.writeBlock([0x00, 0x10, 0x03, 0x00, self._calcCRC([0x03,0x00])])
    
    ## @brief Ejecuta comando de fin de mediciones.
    # @param[out] res Bandera que indica si pudo ejecutar el comando correctamente.
    def stopMeasurement(self):
        return self.i2c.writeBlock([0x01, 0x04])
    
    ## @brief Ejecuta comando de autolimpieza.
    # @param[out] res Bandera que indica si pudo ejecutar el comando correctamente.
    def startFanCleaning(self):
        return self.i2c.writeBlock([0x56, 0x07])
    
    ## @brief Reinicia el sensor.
    def reset(self):
        self.powerOff()
        time.sleep(1)
        self.powerOn()
        # for i in range(5):
        #     ret = self.i2c.writeBlock([0xd3, 0x04])
        #     if ret == True:
        #         return True
        #     print('reset sin exito. Siguiente intento en %.2fs'%(0.2*i))
        #     time.sleep(0.2 * i)
        # print('reset sin exito 5 veces')
        # return False
    
    ## @brief Verifica que hayan datos.
    # @param[out] res Bandera que indica si existen datos.
    def dataAvailable(self):
        data = self.i2c.readBlockFrom([0x02, 0x02], 3)
        if data is None:
            print("dataAvailable lectura sin exito")
            return False
        return not data is None and data[1]==1
    
    ## @brief Realiza una lectura.
    # @param[out] res Bandera que indica si la lectura fue exitosa.
    def readOne(self):
        data = self.i2c.readBlockFrom([0x03, 0x00], 59)
        if ((data is None) or (self._calcFloat(data[18:24]) == 0)):
            return False
        self._pm0p5Count    = self._calcFloat(data[24:30])
        self._pm1Count      = self._calcFloat(data[30:36])
        self._pm2p5Count    = self._calcFloat(data[36:42])
        self._pm4Count      = self._calcFloat(data[42:48])
        self._pm10Count     = self._calcFloat(data[48:54])
        self._pm1           = self._calcFloat(data)
        self._pm2p5         = self._calcFloat(data[6:12])
        self._pm4           = self._calcFloat(data[12:18])
        self._pm10          = self._calcFloat(data[18:24])
        return True
    
    def read(self):
        succesfull = False
        try:
            #Envía comando para iniciar mediciones
            if(self.startMeasurement()):
                #Espera tiempo suficiente para una lectura válida
                time.sleep(8)

                #Inicializa contador de intentos de lectura
                attempt = 1

                #Realiza lectura
                while(attempt <= self.nattempts):

                    if(self.readOne()):
                        succesfull = True
                        if not self.logger is None:
                            self.logger.debug("Sensirion SPS30 realiza medición luego de %d intentos. PM2.5: %.2f. PM10: %.2f."%(attempt, self.pm2p5(), self.pm10()))
                        break
        
                    #Actualiza contador de intentos
                    attempt += 1

                #Envía comando para detener las mediciones
                self.stopMeasurement()
                
        except Exception as e:
            if not self.logger is None:
                self.logger.exception("Error al intentar leer sensor SPS30")
            traceback.print_exc()
        return succesfull

    ## @brief Reinicia tanto el bus como el sensor.
    def bigReset(self):
        self.i2c.reset()
        self.reset()
        time.sleep(0.1) # note: needed after reset
    
    ## @brief Transforma arreglo de 4 bytes a un entero.
    # @param[in] data Arreglo de 4 bytes.
    # @param[out] integer Entero construido a partir del arreglo de bytes.
    def _calcInteger(self, data):
        integer = data[4] + (data[3] << 8) + (data[1] << 16) + (data[0] << 24)
        return integer
    
    ## @brief Transforma un arreglo de 4 bytes en un flotante.
    # @param[in] data Arreglo de 4 bytes.
    # @param[out] first Flotante obtenido del arreglo de bytes.
    def _calcFloat(self, data):
        struct_float = pack('>BBBB', data[0], data[1], data[3], data[4])
        float_values = unpack('>f', struct_float)
        first = float_values[0]
        return first
    
    ## @brief Calcula CRC dado un par de bytes.
    # @param[in] TwoBdataArray Arreglo de 2 bytes.
    # @param[out] crc CRC Calculado a partir del arreglo de bytes.
    def _calcCRC(self, TwoBdataArray):
        f_crc8 = crcmod.mkCrcFun(0x131, 0xFF, False, 0x00)
        byteData = ''.join(chr(x) for x in TwoBdataArray)
        return f_crc8(byteData.encode('utf-8'))

if __name__ == '__main__':
    from config import config
    import statistics, numpy as np
    from services.sensorpool import SensorGuard
    def onRead(sensorObj):
        #Obtiene marca de tiempo en unix y timestamp str
        unix 		= np.datetime64('now').astype('float64')*1000 #Thingsboard acepta tiempo unix en milisegundos
        timestamp 	= str(np.datetime64('now')).replace('T', ' ')
        print(timestamp)

        #Carga configuraciones del sensor
        sensor = config.getSensor(sensorObj.name)

        if not sensor is None:			
            for measurement in sensor["measurements"]:
                #Obtiene valor de la medición en base a mediana
                buff = list(sorted(getattr(sensorObj, measurement["obj_attr_name"])))

                if len(buff)>0:
                    value 										= statistics.median(buff)

                    #Manda mensaje de debugueo
                    print("Data obtenida de %s. %s: %s"%(sensor["name"], measurement["name"], repr(value)))
                else:
                    print("Sensor %s Buffer de mediciones vacio"%sensor["name"])
        else:
            print("%s no existe"%sensorObj.name)
    threads = []

    #Obtiene configuración para el sensor am2315
    # amConfig = config.getSensor("am2315")
    # if not amConfig is None:
    #     from services.am2315_ import AM2315_
    #     #Crea hilo para lectura del sensor am2315
    #     am2315 = AM2315_(buffLen = amConfig["buffLen"], pin=amConfig["enable_gpio"], period = amConfig["period"], on_read = onRead)
    #     #Comienza lectura del sensor
    #     am2315.start()
    #     #Añade sensor a la lista
    #     threads.append(am2315)
    #Obtiene configuración para el sensor sps30
    spsConfig = config.getSensor("sps30")
    if not spsConfig is None:
        # from sps30 import SPS30
        #Crea hilo para lectura del sensor sps30
        sps = SPS30(pin = spsConfig["enable_gpio"], busNum = spsConfig["bus"]["buss_num"], buffLen = spsConfig["buffLen"], on_read = onRead, 
            period = spsConfig["period"])
        #Comienza lectura del sensor
        sps.start()
        threads.append(sps)
    #Inicia hilo para proteger los sensores manteniendolos en sus rangos de operación
    sensorGuard = SensorGuard(threads)
    sensorGuard.start()
