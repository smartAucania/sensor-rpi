TELEMETRY_TEMPLATE = {
    "sps30_pm2p5": {
        "name"              : "sps30_pm2p5",
        "obj_attr_name"     : "pm2p5Buff",
        "format":{
            "id"                            : "s53f12:contaminante:mp2_5",
            "marca"                         : "sensirion",
            "modelo"                        : "sps30",
            "tipo"                          : "contaminante:mp2_5:ugm3",
            "medicion:contaminante:mp2_5"   : "%.2f",
            "fecha:contaminante:mp2_5"      : "",
            "info-extra"                    : "{\"selected\": \"true\"}"
        }
    },
    "sps30_pm10": {
        "name"              : "sps30_pm10",
        "obj_attr_name"     : "pm10Buff",
        "format":{
            "id"                            : "s53f12:contaminante:mp10",
            "marca"                         : "sensirion",
            "modelo"                        : "sps30",
            "tipo"                          : "contaminante:mp10:ugm3",
            "medicion:contaminante:mp10"    : "%.2f",
            "fecha:contaminante:mp10"       : "",
            "info-extra"                    : "{\"selected\": \"false\"}"
        }
    },
    "am2315_temperature": {
        "name"          : "am2315_temperature",
        "obj_attr_name" : "tempBuff",
        "format":{
            "id"                                : "r34p2i:meteorologia:temperatura",
            "marca"                             : "aosong",
            "modelo"                            : "am2315",
            "tipo"                              : "meteorologia:temperatura:°C",
            "medicion:meteorologia:temperatura" : "%.2f",
            "fecha:meteorologia:temperatura"    : "%s",
            "info-extra"                        : "{\"selected\": \"true\"}"
        }
    },
    "am2315_humidity": {
        "name"          : "am2315_humidity",
        "obj_attr_name" : "humBuff",
        "format":{
            "id"                            : "r34p2i:meteorologia:humedad",
            "marca"                         : "aosong",
            "modelo"                        : "am2315",
            "tipo"                          : "meteorologia:humedad:%",
            "medicion:meteorologia:humedad" : "%.2f",
            "fecha:meteorologia:humedad"    : "%s",
            "info-extra"                    : "{\"selected\": \"true\"}"
        }
    },
    "esp32_temperature": {
        "name"          : "internal-temperature",
        "obj_attr_name" : "tempBuff",
        "format":{
            "id"                        : "e3jp2i:otro:temperatura",
            "marca"                     : "espressif",
            "modelo"                    : "esp32",
            "tipo"                      : "otro:temperatura:°C",
            "medicion:otro:temperatura" : "%.2f",
            "fecha:otro:temperatura"    : "%s",
            "info-extra"                : "{\"selected\": \"true\"}"
        }
    },
    "hpma115s0_pm2p5": {
        "name"              : "hpma115s0_pm2p5",
        "obj_attr_name"     : "pm2p5Buff",
        "format":{
            "id"                            : "ca4ag2:contaminante:mp2_5",
            "marca"                         : "honeywell",
            "modelo"                        : "hpma115s0",
            "tipo"                          : "contaminante:mp2_5:ugm3",
            "medicion:contaminante:mp2_5"   : "%d",
            "fecha:contaminante:mp2_5"      : "",
            "info-extra"                    : "{\"selected\": \"false\"}"
        }
    },
    "hpma115s0_pm10": {
        "name"              : "hpma115s0_pm10",
        "obj_attr_name"     : "pm10Buff",
        "format":{
            "id"                            : "ca4ag2:contaminante:mp10",
            "marca"                         : "honeywell",
            "modelo"                        : "hpma115s0",
            "tipo"                          : "contaminante:mp10:ugm3",
            "medicion:contaminante:mp10"    : "%d",
            "fecha:contaminante:mp10"       : "",
            "info-extra"                    : "{\"selected\": \"false\"}"
        }
    },
    "pms7003_pm2p5": {   
        "name"              : "pms7003_pm2p5",
        "obj_attr_name"     : "pm2p5Buff",
        "format":{
            "id"                            : "ewgh31s:contaminante:mp2_5",
            "marca"                         : "plantower",
            "modelo"                        : "pms7003",
            "tipo"                          : "contaminante:mp2_5:ugm3",
            "medicion:contaminante:mp2_5"   : "%d",
            "fecha:contaminante:mp2_5"      : "",
            "info-extra"                    : "{\"selected\": \"false\"}"
        }
    },
    "pms7003_pm10": {
        "name"              : "pms7003_pm10",
        "obj_attr_name"     : "pm10Buff",
        "format":{
            "id"                            : "ewgh31s:contaminante:mp10",
            "marca"                         : "plantower",
            "modelo"                        : "pms7003",
            "tipo"                          : "contaminante:mp10:ugm3",
            "medicion:contaminante:mp10"    : "%d",
            "fecha:contaminante:mp10"       : "",
            "info-extra"                    : "{\"selected\": \"false\"}"
        }
    },
    "ina219_voltage": {
        "name"          : "ina219_voltage",
        "obj_attr_name" : "voltBuff",
        "format":{
            "id"                            : "gh3b2:otro:voltaje",
            "marca"                         : "texas-instruments",
            "modelo"                        : "ina219",
            "tipo"                          : "otro:voltaje:mV",
            "medicion:otro:voltaje"         : "%.2f",
            "fecha:otro:voltaje"            : "%s",
            "info-extra"                    : "{\"selected\": \"true\"}"
        }
    },
    "ina219_current": {
        "name"          : "ina219_current",
        "obj_attr_name" : "currBuff",
        "format":{
            "id"                            : "gh3b2:otro:corriente",
            "marca"                         : "texas-instruments",
            "modelo"                        : "ina219",
            "tipo"                          : "otro:corriente:mA",
            "medicion:otro:corriente"       : "%.2f",
            "fecha:otro:corriente"          : "%s",
            "info-extra"                    : "{\"selected\": \"true\"}"
        }
    },
    "ina219_power": {
        "name"          : "ina219_power",
        "obj_attr_name" : "powBuff",
        "format":{
            "id"                            : "gh3b2:otro:potencia",
            "marca"                         : "texas-instruments",
            "modelo"                        : "ina219",
            "tipo"                          : "otro:potencia:mW",
            "medicion:otro:potencia"        : "%.2f",
            "fecha:otro:potencia"           : "%s",
            "info-extra"                    : "{\"selected\": \"true\"}"
        }
    }
}

def template(sensor, measurement):
    return TELEMETRY_TEMPLATE["%s_%s"%(sensor, measurement)].copy()