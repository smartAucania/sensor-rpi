#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file uart.py
# @brief Contiene funcionalidades para acceder a puerto Serial.
# @see config/config.py
# @see services/sensor.py

import sys
sys.path.insert(0,'..')
import serial as ser
import pigpio, threading, time
from threading import Thread
from services.sensor import Sensor
from collections import deque

## @brief Abre puerto serial cerrando cualquier conexión existente.
# @param[in] port Puerto serial.
# @param[in] baudrate Velocidad del bus en baudios.
# @param[in] timeout Tiempo máximo de espera para lecturas.
# @param[in] logger Logger.
# @param[out] s Objeto con acceso a puerto serial.
def forceOpenSerial(port, baudrate = 9600, timeout = 5, logger = None):
    s = None
    try:
        #Abre puerto serial
        s = ser.Serial(port, baudrate, timeout=timeout, inter_byte_timeout = 5, write_timeout=1)
        s.open()
    except ser.serialutil.SerialException:
        if not logger is None:
            logger.warning("El puerto %s está en uso. Se cierra el puerto y se abre nuevamente."%port)
        #Cierra el puerto serial
        s.close()
        #Abre el puerto serial
        s.open()
    return s

## @brief Abre puerto serial virtual cerrando cualquier conexión existente.
# @param[in] pi Objeto pigpio.
# @param[in] pin Pin RX.
# @param[in] baudrate Velocidad del bus en baudios.
# @param[in] logger Logger.
# @param[out] res Bandera que indica si se logró abrir el puerto no.
def openVirtualSerial(pi, pin, baudrate=9600, logger = None):
    try:
        #Abre puerto serial virtual
        pi.set_mode(pin, pigpio.INPUT)
        pi.bb_serial_read_open(pin, 9600)
        return True
    except:
        if not logger is None:
            logger.warning("El puerto virtual está en uso. Se cierra el puerto y se abre nuevamente.")
        #Cierra puerto serial virtual
        pi.bb_serial_read_close(pin)
        return False

## @class Serial_
class Serial_(Sensor):
    name = "uart"
    ## @brief Constructor de la clase.
    # @param[in] port Puerto serial a utilizar.
    # @param[in] baudrate Velocidad del bus en baudios.
    # @param[in] timeout Tiempo máximo a esperar por lectura.
    # @param[in] logger Logger.
    # @param[in] ready Evento usado para indicar cuando se finaliza un comando de lectura.
    def __init__(self, port, baudrate = 9600, timeout = 5, logger = None, ready = None):
        ## Puerto serial.
        self.port       = port
        ## Velocidad del puerto serial en baudios.
        self.baudrate   = baudrate
        ## Timeout de lectura del puerto serial.
        self.timeout    = timeout
        ## Logger.
        self.logger     = logger
        ## Objeto serial con acceso al puerto serial.
        self.ser        = forceOpenSerial(port, baudrate = baudrate, timeout = timeout, logger = logger)
        ## Cola con comandos a realizar sobre el puerto serial.
        self.cmdStack   = deque()
        ## Buffer temporal de lectura.
        self.buff       = None
        ## Evento que indica cuando finaliza una lectura serial.
        self.ready      = ready
        Sensor.__init__(self)
    
    ## @brief Rutina del objeto.
    def run(self):
        #Activa flag para saber que el hilo se está ejecutando
        self.startSensor()
        while True:
            while True and not self.isStopping():
                while len(self.cmdStack) > 0:
                    cmd = self.cmdStack.popleft()
                    if(cmd['cmd'] == 'resetInput'):
                        #Limpia buffer serial de entrada
                        #print("UART: Limpia buffer de entrada")
                        self.ser.reset_input_buffer()
                    elif(cmd['cmd'] == 'write'):
                        try:
                            self.ser.write(cmd['buff'])
                            self.ser.flush()
                        except:
                            if not self.logger is None:
                                self.logger.error("Error al escribir en el puerto %s"%self.port)
                        #print("UART: Escribe %s"%(repr(cmd['buff'])))
                    elif(cmd['cmd'] == 'read'):
                        #print("UART: CMD read")
                        try:
                            self.buff = self.ser.read(cmd['len'])
                            # print("UART: Lee %s"%(repr(self.buff)))
                            if not self.buff == b'':
                                self.ready.set()
                        except:
                            if not self.logger is None:
                                self.logger.error("Error al leer puerto %s"%self.port)
                    elif(cmd['cmd'] == 'flush'):
                        self.ser.flush()
                # print("Termina de quitar elementos de la cola")
                # print("left: %d"%(len(self.cmdStack)))
                
                time.sleep(0.5)

            #Avisa que ha terminado de ejecutarse el hilo
            self.endSensor()

            #Espera a que se requiera nuevamente la ejecución del hilo
            self.waitForActivation()
    
    ## @brief Reabre el puerto serial.
    def reopen(self):
        self.ser = forceOpenSerial(self.port, baudrate = self.baudrate, timeout = self.timeout, logger = self.logger)
    
    ## @brief Obtiene el buffer con la lectura.
    # @param[out] buff Buffer de lectura.
    def getBuffer(self):
        buff = self.buff
        self.buff = None
        return buff
    
    ## @brief Añade tarea de limpieza de buffer de entrada serial.
    def resetInput(self):
        self.cmdStack.append({'cmd':'resetInput'})
    
    ## @brief Añade tarea de escritura en serial.
    # @param[in] buff Buffer con comando serial.
    def write(self, buff):
        self.cmdStack.append({'cmd':'write','buff':buff})
    
    ## @brief Añade tarea de flush.
    def flush(self):
        self.cmdStack.append({'cmd':'flush'})
    
    ## @brief Añade tarea de lectura del puerto serial.
    # @param[in] n Número de bytes a leer.
    def read(self, n):
        #print("UART: read cmd appended")
        self.cmdStack.append({'cmd':'read', 'len': n})