#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file git_monitor.py
# @brief Realiza un chequeo periódico del repositorio local, para detectar un cambio de commit y realizar un reinicio del sistema.
# @see config/config.json
import git, os, time, config
from config import config

## @var dir_path
# @brief Directorio al que pertenece este script
dir_path = os.path.dirname(os.path.realpath(__file__))

repo = git.Repo(dir_path)
## @var current_commit
# @brief Commit actual
current_commit = repo.head.commit

print("dir path: %s"%dir_path)

## @brief Actualiza el repositorio local.
def pull():
    os.system("rm %s.git/index.lock"%dir_path)
    os.system("git pull origin master")

#Loop infinito
while True:
    print("Durmiendo...")
    #Duerme un periodo
    time.sleep(config.getGitUpdatePeriod())

    #Intenta actualizar el repositorio
    pull()

    #Verifica si ha habido una actualización
    if current_commit == repo.head.commit:
        print("No se han realizado nuevas actualizaciones")
    else:
        #Reinicia el sistema
        print("Nueva actualización realizada. Se dispone a reiniciar el dispositivo")
        os.system("sudo reboot")
