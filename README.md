# Monosensor-Raspberry-pi-3-b

Dispositivo monosensor programado en python 3.6. Cuenta con sensores de material particulado mp 2.5 y mp 10, y un sensor de humedad y temperatura. Los sensores utilizados son el hpma115s0, pms7003, sps30 y am2315. Integración con Thingsboard y datalogger.

## Documentación en Doxygen
[Documentación](https://gitlab.com/smart-city-ufro/monosensor-raspberry-pi-3-b/blob/Desarrollo/documentation/html/index.html)