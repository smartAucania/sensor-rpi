#TODO
#Almacenar dispositivos conectados en mongodb en caso de reinicio
#El ack va con packet id 0
#Actualizar date en paquetes que no han podido ser enviados al tiempo
import time, traceback, threading, glob, os, math, json
from datetime import datetime
import numpy as np
from packaging import version
from collections import deque
import services.packager as packager
from threading import Semaphore
from services.thread_ import Thread_
from services.lora import Lora
from services.node import Node, NodeGroup
from services.scinadmin import ScinaboxAdmin
from services.mongogger import DB
import services.tel_template as tel_template
import threading
from services.publisher import ThingsboardPublisher

class Coordinator(Thread_):
    def __init__(self, lora, pan_id = 0x00, network = "net_test", gateway = "gateway-lab", user= "", password=""):
        self.lora               = lora
        self.pan_id             = pan_id
        self.network            = network
        self.st                 = time.time()
        self.pan_id_period      = 60
        self.tel_buff           = deque([])
        self.lora_sem           = Semaphore(1)
        self.nodes              = NodeGroup([Node(short_id = 0x00)])
        self.db_nodes           = DB("scinabox", "nodes")
        self.gateway_name       = gateway
        if self.db_nodes.size() > 0:
            self.nodes = NodeGroup([Node.from_dict(d) for d in self.db_nodes.getAll()])
        else:
            self.db_nodes.add(Node(short_id = 0x00).dict())
        self.lora.set_nodes(self.nodes)
        self.lora.rx_on_idle    = True
        self.admin              = ScinaboxAdmin(host="146.83.206.131", port=443, email=user, password=password)
        self.sw_ver_thread  	= threading.Thread(target = self.sw_run)
        
        if not self.admin.login():
            raise Exception("Falla al loguearse en servicio de administración")
        
        self.gateway = self.admin.get_gateway({"gateway": {"name": self.gateway_name}})
        if self.gateway is None:
            raise Exception("Error al buscar gateway")
        self.sw_ver_thread.start()
        Thread_.__init__(self)
    
    def sw_run(self):
        while True:
            for node in [n for n in self.nodes.nodes if not n.short_id == 0x00]:
                if node.new_sw_available:
                    continue
                try:
                    self.admin.login()
                    dev                     = self.admin.get_device({"auth_key": node.auth_key})
                    node.dev                = dev
                    print("short id: %d. Version local: %s. Version remota: %s"%(node.short_id, node.sw_version, dev["version"]))
                    version.parse(node.sw_version) < version.parse(dev["version"])

                    if version.parse(node.sw_version) >= version.parse(dev["version"]) or version.parse(node.new_sw_version) >= version.parse(dev["version"]):
                        continue

                    # if version.parse(node.sw_version) < version.parse(dev["version"]) and version.parse(node.new_sw_version) < version.parse(dev["version"]):
                    print("La versión es más nueva!")
                    path = "/tmp/devices/%s"%dev["name"]
                    [os.remove(f) for f in glob.glob(path + "/**/*.py", recursive=True)]
                    [os.remove(f) for f in glob.glob(path + "/**/*.mpy", recursive=True)]
                    
                    if self.admin.get_device_sw({"auth_key": node.auth_key, "name": dev["name"]}) is None:
                        continue
                    py_files = [f for f in glob.glob(path + "/**/*.py", recursive=True)]
                    mpy_files = [f for f in glob.glob(path + "/**/*.mpy", recursive=True)]
                    py_files.extend(mpy_files)
                    for filepath in py_files:
                        stat        = os.stat(filepath)
                        file_size   = stat.st_size
                        packet_size = 50
                        npackets    = math.ceil(file_size/packet_size)
                        nchunks     = math.ceil(npackets/10)
                        print("Archivo %s de tamaño %d se divide en %d paquetes y %d bloques de 10 paquetes"%(filepath, file_size, npackets, nchunks))
                        msg = {
                            "data":{
                                "version":  dev["version"],
                                "filename": filepath.replace(path+"/", ""),
                                "npackets": npackets,
                                "nblocks": nchunks
                            },
                            "cmd": packager.CMD_NEW_VERSION,
                            "key": dev["key"],
                            "packet id": node.generate_packet_id(),
                            "short id": node.short_id
                        }
                        # node.remove_polling_update_packet(msg)
                        node.polling_queue.append(msg)
                    msg = {
                        "data":{},
                        "cmd": packager.CMD_NO_FILES_LEFT_UP,
                        "key": dev["key"],
                        "short id": node.short_id,
                        "packet id": node.generate_packet_id()
                    }
                    node.polling_queue.append(msg)
                    node.new_sw_available = True
                    node.new_sw_version = dev["version"]
                    node.new_sw_files   = py_files
                except:
                    traceback.print_exc()
            time.sleep(60)

    def _run(self):
        # #Envía pan id
        # if time.time() - self.st >= self.pan_id_period:
        #     self.st = time.time()
        #     print("Sending pan id...")
        #     self.send_pan_id()

        #Falta mandar configuracion periodicamente     
        try:            
            raw, msg = self.lora.receive(self.pan_id_period*1000)
            if not msg is None and (msg["pan id"] == self.pan_id or msg["cmd"] == packager.CMD_DISCOVERY):
                #Verificar si el identificador de paquete ya ha sido recibido
                #Basta con guardar el ultimo paquete de cada dispositivo, tanto enviado como recibido para asi compararlo
                try:
                    self.process_msg(raw, msg)
                except:
                    traceback.print_exc()
            else:
                print("No llega nada ...")
        except:
            traceback.print_exc()    

        #Verifica timer para eliminar nodos inactivos
        self.nodes.drop_inactive_nodes(timeout = 60*60, cb = lambda node: self.db_nodes.remove({"auth key": node.auth_key}))

        # time.sleep(0.01)
        #Estar preguntando por nuevas actualizaciones de software para los dispositivos
        #Recibir peticiones cambio de configuracion de un dispositivo, actualizacion de software o 

    #Falta metodo para mandar señal de reinicio
    def process_msg(self, raw, msg):
        cmd = msg["cmd"]
        if not (cmd == packager.CMD_ACK or cmd == packager.CMD_REQ_ASSOCIATE):
            self.nodes.update_last_received_packet(msg)
        if cmd == packager.CMD_TELEMETRY          :
            print("telemetry")
            self.process_telemetry(raw, msg)
        elif cmd == packager.CMD_REQ_ASSOCIATE    :
            print("req associate")
            self.process_req_assoc(msg)
        elif cmd == packager.CMD_ACK              :
            print("ack")
            self.process_ack(raw, msg)
        elif cmd == packager.CMD_POLLING          :
            print("polling")
            self.process_polling(msg)
        elif cmd == packager.CMD_REQ_UPDATE       :
            print("Req update")
            self.process_req_update(raw, msg)
        elif cmd == packager.CMD_UPDATE_VERSION   :
            print("updated version")
            self.process_update_ver(raw, msg)
        elif cmd == packager.CMD_REQ_UPDATE_STREAM:
            print("Req update stream")
            self.process_update_stream(raw, msg)
        elif cmd == packager.CMD_ASK_FILES_LEFT   :
            print("Ask files left")
            self.process_ask_files_left(raw, msg)
        elif cmd == packager.CMD_DISCOVERY        :
            print("discovery")
            self.process_discovery(raw, msg)
        else:
            print("Comando desconocido: %d"%cmd)
    
    def process_discovery(self, raw, msg):
        print("Sending pan id...")
        self.send_pan_id()

    def process_telemetry(self, raw, msg):
        node = self.nodes.find_node(attr = "short_id", value = msg["short id"])
        if not node is None:
            msg = packager.unpack(raw, node.key)
            print("Recibe telemetria ", msg)
            self.tel_buff.append({
                "short id":     msg["short id"],
                "node":         self.nodes.find_node(attr = "short_id", value = msg["short id"]),
                "measurements": msg["data"]["telemetry"]["measurements"],
                "rssi":         self.lora.rssi()
            })
            self.nodes.update_last_packet_sent(msg)
            self.lora_sem.acquire()
            self.lora.send_ack(packet_id = 0x00, short_id = msg["short id"], npacket = msg["packet id"])
            self.lora_sem.release()

            if self.admin.login_if_needed():
                del msg["data"]
                t  	= threading.Thread(target = lambda: self.admin.set_last_packet_sent(self.gateway_name, node.auth_key, json.dumps(msg), str(np.datetime64("now"))+"z"))
                t.start()
    
    def process_req_assoc(self, msg):
        #Obtiene key del satelite
        #Pregunta por la key al servidor
        #Obtiene la key privada
        #Asigna short id
        #Agrega el disposistivo a la lista
        #Envía mensaje con short id y la fecha ("date")
        #Agrega el mensaje enviado a lista de mensajes con ack pendiente
        #En caso de respuesta negativa, no hacer nada
        
        #Extrae llave de autenticación
        auth_key     = msg["data"]["associate request"]["key"]

        #Extrae versión de código
        sw_version   = msg["data"]["associate request"]["version"]

        #Verifica si el nodo ya existe
        node = self.nodes.find_node(attr = 'auth_key', value = auth_key)
        if not node is None:
            print("Nodo con clave de autenticacion %s ya existe. Se manda short id"%auth_key)
            short_id    = node.short_id
        else:
            #Genera short id
            short_id    = self.nodes.generate_short_id()
            if short_id is None:
                #Hay que mandar mensaje rechazando la union a la red
                return None

        #Consulta llave de encriptación
        self.admin.login()
        device = self.admin.get_device({"auth_key": auth_key})
        if device is None:
            return None
        if not self.admin.assign_gateway(self.gateway_name, {"auth_key": auth_key, "short_id": short_id}):
            return None
        sat_key = device["key"]

        if sat_key is None:
            #Hay que mandar mensaje rechazando la union a la red
            return None
        sat_key     = sat_key.encode()        
        
        #Envia short id asignada y fecha actual
        packet_id   = 0
        data        = {
            "short id"  : short_id,
            "date"      : int(time.time())
        }
        packet      = packager.pack(pan_id = self.pan_id, packet_id = packet_id, short_id = 0x00, cmd = packager.CMD_ASSOCIATE_ACCEPT, data = data, key = sat_key)
        print("Accept node with auth key %s as short id %s. SW version: %s"%(auth_key, hex(short_id), sw_version))
        self.lora_sem.acquire()
        self.lora.send(packet, csma=False)
        self.lora_sem.release()

        #Agrega dispositivo a la lista de conectados
        node        = self.nodes.find_node(attr="auth_key", value=auth_key)
        if node is None:
            node        = Node(short_id = short_id, key = sat_key, auth_key = auth_key, sw_version = sw_version)
            node.dev = device
            print("New node.. Auth key: %s. Key: %s"%(auth_key, sat_key))
            self.db_nodes.remove({"auth key": node.auth_key})
            self.add_node(node)
        else:
            #Modifica el nodo
            updated_node = Node(short_id = short_id, key = sat_key, auth_key = auth_key, sw_version = sw_version)
            updated_node.dev = device
            self.db_nodes.remove_many({"auth key": node.auth_key})
            print(node.dict())
            print(updated_node.dict())

            self.nodes.remove(node.short_id)
            self.add_node(updated_node)

            #Elimina el nodo
            del node

            print("Updated node.. Auth key: %s. Key: %s"%(auth_key, sat_key))

        #Agrega mensaje a lista de mensajes sin ack recibido
        msg         = packager.unpack(packet, sat_key)
        self.nodes.append_unack_msg(msg)
    
    def add_node(self, node):
        self.nodes.append(node)
        self.db_nodes.add(node.dict())
    
    def process_ack(self, raw, msg):
        #Obtiene el identificador de paquete del paquete
        #Busca el identificador de paquete en la lista de paquetes enviados
        #Quita el paquete de la lista
        print("processing ack")

        node = self.nodes.find_node(attr="short_id", value = msg["short id"])
        if not (not node is None and not node.dev is None):
            return 
        msg  = packager.unpack(bytes(raw), node.key)
        msg  = {"short id": msg["short id"], "packet id": msg["data"]["ack"]["packet"]}
        self.nodes.remove_unack_msg(msg)
        poll_msg = self.nodes.remove_polling_msg(msg)

        if not poll_msg is None and self.admin.login_if_needed():
            t  	= threading.Thread(target = lambda: self.admin.set_last_packet_received(self.gateway_name, node.auth_key, json.dumps(msg), str(np.datetime64("now"))+"z"))
            t.start()            

        if self.admin.login_if_needed():
            t  	= threading.Thread(target = lambda: self.admin.set_last_packet_sent(self.gateway_name, node.auth_key, json.dumps(msg), str(np.datetime64("now"))+"z"))
            t.start()
    
    def process_polling(self, msg):
        #Obtiene el short id del dispositivo
        #Busca en la lista de polling para saber si hay un mensaje para el dispositivo
        #En caso de haber, envía dicho mensaje al dispositivo, informandole además de la cantidad de mensajes faltantes por leer
        #El mensaje se agrega a la cola de mensajes sin ack
        short_id    = msg["short id"]
        node = self.nodes.find_node(attr = "short_id", value = short_id)
        if node is None:
            return
        self.nodes.update_last_packet_sent(msg)
        polling_msg = self.nodes.first_polling_msg(short_id)
        
        date 	= np.datetime64(datetime.utcnow(), dtype='datetime64[ms]').astype('int64').item()/1e6
        date    = int(date)

        if polling_msg is None:
            packet_id   = self.nodes.generate_packet_id(short_id)
            polling_msg = {"cmd": packager.CMD_POLLING_EMPTY_RSP, "data": {"date": date}, "key": node.key, "packet id": packet_id, "short id": msg["short id"]}
            node.polling_queue.append(polling_msg)

        packet  = packager.pack(packet_id = polling_msg["packet id"], short_id = short_id, 
            cmd = polling_msg["cmd"], data = polling_msg["data"], key = polling_msg["key"])

        print("Transmite paquete de polling. ", polling_msg["data"])
        self.lora_sem.acquire()
        self.lora.send(packet, csma=False)
        self.lora_sem.release()
        polling_msg["short id"] = short_id
        self.nodes.append_unack_msg(polling_msg)
        self.nodes.update_last_received_packet(msg)

        if self.admin.login_if_needed():
            t  	= threading.Thread(target = lambda: self.admin.set_last_packet_sent(self.gateway_name, node.auth_key, json.dumps(msg), str(np.datetime64("now"))+"z"))
            t.start()
        # self.polling_msgs.remove(i)
    
    def process_req_update(self, raw, msg):
        node = self.nodes.find_node(attr="short_id", value = msg["short id"])
        if not node is None and not node.dev is None:
            msg         = packager.unpack(bytes(raw), node.key)
            npacket     = msg["data"]["req update packet"]["id"]
            self.send_update_packet(node, msg["data"]["req update packet"]["filename"], npacket)
            if self.admin.login_if_needed():
                t  	= threading.Thread(target = lambda: self.admin.set_last_packet_sent(self.gateway_name, node.auth_key, json.dumps(msg), str(np.datetime64("now"))+"z"))
                t.start()
    
    def send_update_packet(self, node, filename, npacket):
        name        = node.dev["name"]
        path        = "/tmp/devices/%s"%name
        filepath    = "%s/%s"%(path, filename)
        stat        = os.stat(filepath)
        file_size   = stat.st_size
        packet_size = 50
        npackets    = math.ceil(file_size/packet_size)
        if npacket>=npackets:
            return
        with open(filepath, "rb") as f:
            f.seek(npacket*packet_size)
            if npacket==npackets-1:
                buff = f.read(file_size%packet_size)
            else:
                buff = f.read(packet_size)
            packet      = packager.pack(pan_id = self.pan_id, packet_id = 0, short_id = node.short_id, 
                cmd = packager.CMD_UPDATE_PACKET, data = {"id": npacket, "data": buff}, 
                key = node.key)
            time.sleep(0.1)
            # print()
            # print("Sending.. ", packet)
            self.lora_sem.acquire()
            self.lora.send(packet, csma=False)
            self.lora_sem.release()

            if self.admin.login_if_needed():
                msg = packager.unpack(packet)
                t  	= threading.Thread(target = lambda: self.admin.set_last_packet_received(self.gateway_name, node.auth_key, json.dumps(msg), str(np.datetime64("now"))+"z"))
                t.start()
            # print("Termina envio")
    
    def process_update_ver(self, raw, msg):
        node = self.nodes.find_node(attr="short_id", value = msg["short id"])
        if not node is None and not node.dev is None:
            msg         = packager.unpack(bytes(raw), node.key)
            self.lora_sem.acquire()
            self.lora.send_ack(packet_id = 0, npacket=msg["packet id"], short_id=node.short_id)
            self.lora_sem.release()
            data        = msg["data"]["update version"]
            version     = data["version"]
            path        = "/tmp/devices/%s"%node.dev["name"]
            filename    = "%s/%s"%(path, data["filename"])
            
            if filename in node.new_sw_files:                    
                # node.mark_sw_updated()
                print("Archivos antes del update: ", node.new_sw_files)
                node.mark_sw_file(filename)
                print("Archivos despues del update: ", node.new_sw_files)

                print("Dispositivo %d ha actualizado archivo %s a versión %s"%(node.short_id, filename, version))
            else:
                print("Dispositivo %d vuelve a informar actualizacioń de archivo %s a version %s"%(node.short_id, filename, version))
                print("Version %s"%node.sw_version)
                print("Archivos despues del update repetitivo: ", node.new_sw_files)
            if len(node.new_sw_files) == 0:
                node.mark_sw_updated()
            
            if self.admin.login_if_needed():
                t  	= threading.Thread(target = lambda: self.admin.set_last_packet_sent(self.gateway_name, node.auth_key, json.dumps(msg), str(np.datetime64("now"))+"z"))
                t.start()
            
    def send_pan_id(self):
        data = {
            "network":  self.network,
            "pan id":   self.pan_id
        }
        packet = packager.pack(pan_id = 0xFFF, packet_id = 0x00, short_id = 0xFF, 
            cmd = packager.CMD_PAN_ID, data = data, key = b"sin usar90123456")
        self.lora_sem.acquire()
        self.lora.send(packet)
        self.lora_sem.release()
        # time.sleep(0.5)
    
    def process_update_stream(self, raw, msg):
        #Busca el nodo
        node = self.nodes.find_node(attr="short_id", value = msg["short id"])
        if not node is None and not node.dev is None:
            #Extrae información restante del paquete
            msg         = packager.unpack(bytes(raw), node.key)

            #Manda ack
            self.lora_sem.acquire()
            self.lora.send_ack(packet_id = 0, npacket=msg["packet id"], short_id=node.short_id)
            self.lora_sem.release()

            #Obtiene el peso del archivo
            filename    = msg["data"]["req update stream"]["filename"]
            name        = node.dev["name"]
            path        = "/tmp/devices/%s"%name
            filepath    = "%s/%s"%(path, filename)
            stat        = os.stat(filepath)
            file_size   = stat.st_size
            packet_size = 50
            npackets    = math.ceil(file_size/packet_size)
            block       = msg["data"]["req update stream"]["block"]

            poll_msg = node.remove_polling_msg(msg)

            if not poll_msg is None and self.admin.login_if_needed():
                t  	= threading.Thread(target = lambda: self.admin.set_last_packet_received(self.gateway_name, node.auth_key, json.dumps(msg), str(np.datetime64("now"))+"z"))
                t.start()  

            # Manda los paquetes
            npacket     = block*10

            print("Dispositivo %d solicita streaming del archivo %s bloque %d. Se estima que parte del paquete %d."%(node.short_id, filename, block, npacket))
            for i in range(10):
                if(i+npacket==npackets):
                    break
                print("Se manda paquete %d del archivo %s a dispositivo %d"%(i+npacket, filename, node.short_id))
                self.send_update_packet(node, filename, i+npacket)
    
    def process_ask_files_left(self, raw, msg):
        node = self.nodes.find_node(attr="short_id", value = msg["short id"])
        if not node is None:
            #Extrae información restante del paquete
            msg         = packager.unpack(bytes(raw), node.key)
            if len(node.new_sw_files)>0:
                #Manda informacion sobre un archivo a actualizar
                path        = "/tmp/devices/%s"%node.dev["name"]
                filepath    = node.new_sw_files[0]
                stat        = os.stat(filepath)
                file_size   = stat.st_size
                packet_size = 50
                npackets    = math.ceil(file_size/packet_size)
                nchunks     = math.ceil(npackets/10)
                print("Archivo %s de tamaño %d se divide en %d paquetes y %d bloques de 10 paquetes"%(filepath, file_size, npackets, nchunks))
                data = {
                    "version":  node.new_sw_version,
                    "filename": filepath.replace(path+"/", ""),
                    "npackets": npackets,
                    "nblocks": nchunks
                }
                packet = packager.pack(pan_id = self.pan_id, cmd = packager.CMD_NEW_VERSION, data = data, key = node.key, packet_id = node.generate_packet_id(), short_id = node.short_id)                
                self.lora.send(packet, csma=False)
            else:
                #Informa que ya no resta ningun archivo por actualizar
                packet = packager.pack(pan_id = self.pan_id, cmd = packager.CMD_NO_FILES_LEFT_UP, key = node.key, short_id = node.short_id, packet_id = node.generate_packet_id())
                self.lora.send(packet, csma=False)
            if self.admin.login_if_needed():
                t  	= threading.Thread(target = lambda: self.admin.set_last_packet_sent(self.gateway_name, node.auth_key, json.dumps(msg), str(np.datetime64("now"))+"z"))
                t.start()
def publish(publishers, tel, attr):
    if len(publishers) < 1:
        return

    # publishers[0].logger.debug("Medición %s recibida. Se almacenan los datos en tabla historica."%measurement["measurement"]["name"])

    #Inicializa flag de publicación
    publishing = True

    #publica la medición
    for pub in publishers:
        if not pub is None:
            try:
                if not pub.publish(tel, attr):
                    publishing = False                  
            except:
                traceback.print_exc()
                # pub.logger.exception()
                publishing = False
        if publishing:
            try:
                pub.publishUnsent(attr)
            except:
                traceback.print_exc()
                # pub.logger.exception("Error al publicar telemetrias almacenadas anteriormente")

def measToTel(meas, format_ = "simple"):
    print("Format: %s"%format_)
    #Construye paquete simple de telemetria
    if format_ == "simple":
        print("El formato es simple")
        tel_name = meas.tel_name.replace(".", "_")
        simple_tel 							= dict()
        simple_tel["values"] 				= dict()
        simple_tel["values"][tel_name] 	    = meas.value
        ts                                  = meas.timestamp*1000
        print(simple_tel)
        print("ts: ", ts)
        if ts/1000 < 946684800+86400*365:
            ts                              = unix
        simple_tel["ts"] 					= ts
        print("ts after: ", ts)
        return simple_tel
    else:
        #Construye paquete completo de telemetria
        complete_tel                            = tel_template.template(meas.sensor_name, meas.name)["format"].copy()
        tipo                                    = ':'.join(complete_tel["tipo"].split(':')[:-1])
        print("Tipo: %s"%(repr(tipo)))
        medicion                                = "medicion:%s"%tipo
        print(medicion, complete_tel[medicion])
        complete_tel[medicion]                  = complete_tel[medicion]%(meas.value)
        # complete_tel["medicion:%s"%tipo]        = complete_tel["medicion:%s"%tipo]%(meas.value)
        complete_tel["fecha:%s"%tipo] 			= "%d"%int(meas.timestamp*1000)
        return complete_tel
if __name__ == "__main__":
    import sys
    import os
    sys.path.append(os.getcwd() + '/..')
    from lib.thingsboard import ThingsBoardHTTP, Logger
    from lib.logger_ import RotatingLogger
    import config.config as cfg

    lora_conf = cfg.getLora()

    #Configura logger de mensajes del sistema
    rotLogger 		= RotatingLogger("debug_lora.log", maxBytes=20*1024*1024, backupCount=2)
    chatId 			= 480097956
    telegramToken 	= "851377220:AAGCgbsdg8tjMQnxgAgW7oNQVr1RulbLHGE"
    logger          = Logger(logger = rotLogger, telegramToken = telegramToken, chatId = chatId, level = Logger.WARNING, name = "esp32lablora")

    #Create object for lora
    short_id = 0x00
    key = b"123456789012345D"
    pan_id = lora_conf["pan_id"]
    nodes = []
    node_group = NodeGroup(nodes)

    #Create object for lora
    lora = Lora(short_id = short_id, nodes = node_group, tx_power = lora_conf["tx_power"], pan_id = pan_id, sf=lora_conf["spreading_factor"])
    lora.rx_on_idle = True

    #Print lora configuration
    print()
    print("LORA INFORMATION")
    print("****************")
    print(lora)
    coordinator = Coordinator(lora = lora, pan_id = pan_id, network = lora_conf["network"], gateway = lora_conf["gateway"], user=lora_conf["user"], password=lora_conf["password"])
    coordinator.start()
    while True:
        try:
            if len(coordinator.tel_buff) >0:
                #Obtiene telemetria
                tel = coordinator.tel_buff.popleft()
                measurements = tel["measurements"]
                print("short id: %s"%(hex(tel["short id"])))
                print("RSSI: %d"%tel["rssi"])
                name = tel["node"].dev["name"]
                print("Dev name: %s"%name)

                #Obtiene parametros de configuracion
                config = tel["node"].dev["config"]
                publishers_conf = config["publishers"]
                attr_conf = config["attributes"]

                #Construye attributos
                attr = attr_conf

                #Construye publicadores en base a configuraciones config.json
                publishers = deque()
                for publisher in publishers_conf:
                    if publisher["method"] == "http":
                        try:
                            #Añade publicador
                            publishers.append(
                                #Construye publicador
                                ThingsboardPublisher(
                                    tableName 	= "t"+publisher["token"],
                                    format_ 	= publisher["format"],
                                    logger 		= logger,
                                    thingsBoard = ThingsBoardHTTP(
                                        token		= publisher["token"],
                                        host		= publisher["host"], 
                                        port		= publisher["port"],
                                        timeout		= publisher["timeout"]
                                    )
                                )
                            )
                        except:
                            traceback.print_exc()
                            print("No se ha conseguido conectar con la plataforma Thingsboard. Conf: %s"%repr(publisher))
                            publishers = []
                    publishers = list(publishers)
                
                for pub in publishers:
                    print(pub)
                #Construye paquete simple de telemetria para el RSSI
                simple_tel 							= dict()
                simple_tel["values"] 				= dict()
                simple_tel["values"]["rssi"]		= tel["rssi"]
                unix 		                        = np.datetime64(datetime.utcnow(), dtype='datetime64[ms]').astype('float64')/1000 
                simple_tel["ts"] 					= unix
                print("Publicando telemetrias lora...")
                print("tel: %s", repr(simple_tel))
                for pub in publishers:
                    if pub.format == "simple":
                        pub.publish(simple_tel, attr)

                #Envia mediciones con el formato adecuado de telemetria
                for meas in measurements:
                    for pub in publishers:
                        #Construye telemetria
                        tel = measToTel(meas, format_ = pub.format)
                        publish([pub], tel, attr)
                        # threading.Thread(target = publish, args= (publishers, tel, attr)).start()
                        print(meas)
        except:
            traceback.print_exc()    
        time.sleep(0.01)
    # time.sleep(3)
    # coordinator.pause()

    # time.sleep(3)
    # coordinator.resume()

    # time.sleep(3)
    coordinator.stop()
