import requests, json
import threading, os

def getURL():
    try:
        r = requests.get('http://127.0.0.1:4040/api/tunnels')
        return r.json()["tunnels"][0]["public_url"].split('tcp://')[1]
    except:
        print("Servidor local ngrok no responde")

def ssh():
    '''Contar hasta cien'''
    os.system('/home/pi/ngrok tcp 22')

def start():
    hilo1 = threading.Thread(target=ssh)
    hilo1.start()