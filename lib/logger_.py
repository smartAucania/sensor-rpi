import logging
from logging.handlers import RotatingFileHandler

class RotatingLogger:
	def __init__(self, filename, maxBytes = 10240, backupCount = 2):
		self.filename 		= filename
		self.maxBytes 		= maxBytes
		self.backupCount 	= backupCount

		#Configura logger de mensajes del sistema. Este solo escribe en archivo.
		self.logger = logging.getLogger(__name__)
		fh = RotatingFileHandler(self.filename, mode = 'a', maxBytes = self.maxBytes, 
										backupCount = self.backupCount, encoding = None, delay = 0)
		
		fh.setLevel(logging.DEBUG)
		formatter = logging.Formatter('%(asctime)s - %(threadName)s - %(levelname)s - %(message)s')
		fh.setFormatter(formatter)

		# create console handler and set level to debug
		ch = logging.StreamHandler()
		ch.setLevel(logging.INFO)

		# create formatter
		formatter = logging.Formatter('%(asctime)s - %(threadName)s - %(levelname)s - %(message)s')

		# add formatter to ch
		ch.setFormatter(formatter)

		self.logger.addHandler(fh)
		self.logger.addHandler(ch)
		self.logger.setLevel(logging.DEBUG)
	
	def debug(self, msg):
		self.logger.debug(msg)

	def info(self, msg):
		self.logger.info(msg)
	
	def critical(self, msg):
		self.logger.critical(msg)
	
	def error(self, msg):
		self.logger.error(msg)
	
	def exception(self, msg):
		self.logger.exception(msg)
	
	def warning(self, msg):
		self.logger.warn(msg)