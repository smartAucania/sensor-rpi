#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file rgbled.py
# @brief Funcionalidades para manejar un led RGB.

import sys
sys.path.insert(0,'..')
import pigpio, time, traceback
from services.sensor import Sensor
from collections import deque

## @class Color
class Color:
    #Se definen los posibles colores del LED RGB
    ## ID color rojo.
    RED         = 1

    ## ID color azul.
    BLUE        = 3

    ## ID color verde.
    GREEN       = 2

    ## ID color amarillo.
    YELLOW      = 4

    ## ID color morado.
    PURPLE      = 5

    ## ID color naranjo.
    ORANGE      = 6

    ## ID color azul.
    LIGHT_BLUE  = 7

## @class RGBLed
# @code{.py}
# rgb = RGBLed(20, 16, 21)
# rgb.setColor(Color.RED)
# while True:
#     time.sleep(1)
#     rgb.blink(Color.LIGHT_BLUE, 1)
#     time.sleep(1)
#     rgb.blink(Color.PURPLE, 1)
#     time.sleep(1)
#     rgb.blink(Color.BLUE, 1)
#     time.sleep(1)
#     rgb.blink(Color.YELLOW, 1)
#     time.sleep(1)
#     rgb.blink(Color.GREEN, 1)
# @endcode
class RGBLed():
    ## Se definen la paleta de colores en RGB.
    COLORS = {
        Color.RED:              [255, 0, 0],
        Color.BLUE:             [0, 0, 255],
        Color.GREEN:            [0, 255, 0],
        Color.YELLOW:           [255, 60, 0],
        Color.ORANGE:           [255, 20, 0],
        Color.PURPLE:           [150, 0, 125],
        Color.LIGHT_BLUE:       [0, 170, 125]
    }

    ## @brief Constructor de la clase.
    # @param[in] pinR Pin GPIO conectado al color rojo.
    # @param[in] pinG Pin GPIO conectado al color verde.
    # @param[in] pinB Pin GPIO conectado al color azul.
    def __init__(self, pinR, pinG, pinB):
        ## Pin GPIO a color rojo.
        self.pinR           = pinR          
        ## Pin GPIO a color verde.
        self.pinG           = pinG          
        ## Pin GPIO a color azul.
        self.pinB           = pinB          
        ## Controlador de GPIO.
        self.pi             = pigpio.pi()   

    ## @brief Configura el dutycycle de la señal pwm dirigida al led RGB.
    # @param[in] dutyCycle Arreglo de tres elementos representando dutcycle de los tres colores.
    def setDutyCycle(self, dutyCycle):
        #Configura dutycycle de pines RGB del led
        self.pi.set_PWM_dutycycle(self.pinR, dutyCycle[0])
        self.pi.set_PWM_dutycycle(self.pinG, dutyCycle[1])
        self.pi.set_PWM_dutycycle(self.pinB, dutyCycle[2])

    ## @brief Configura el color del led.
    # @param[in] color Color escogido.
    def setColor(self, color):
        #Elimina el color si es None
        if color is None:
            self.clean()
            return
        #Comprueba que el color exista
        if color in RGBLed.COLORS:
            #Configura color
            self.setDutyCycle(RGBLed.COLORS[color])
            
            #Actualiza color actual
            self.current_color = color

    ## @brief Parpadea el led RGB.
    # @param[in] color Color escogido.
    # @param[in] t Duración del parpadeo.
    def blink(self, color, t):
        #Comprueba que el color exista
        if color in RGBLed.COLORS:
            #Almacena color actual
            prev_color = self.current_color

            #Configura nuevo color
            self.setColor(color)

            #Mantiene el color durante t segundos
            time.sleep(t)

            #Configura color anterior
            self.setColor(prev_color)

    ## @brief Apaga el led RGB.
    def clean(self):
        #Apaga el led
        self.setDutyCycle([0, 0, 0])

        #Actualiza color actual
        self.current_color = None

if __name__ == "__main__":
    rgb = RGBLed(20, 16, 21)
    rgb.setColor(Color.RED)
    while True:
        time.sleep(1)
        rgb.blink(Color.LIGHT_BLUE, 1)
        time.sleep(1)
        rgb.blink(Color.PURPLE, 1)
        time.sleep(1)
        rgb.blink(Color.BLUE, 1)
        time.sleep(1)
        rgb.blink(Color.YELLOW, 1)
        time.sleep(1)
        rgb.blink(Color.GREEN, 1)