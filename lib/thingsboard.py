#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file thingsboard.py
# @brief Contiene clases para interactuar con servidor Thingsboard.
# @see config/config.py

import paho.mqtt.client as mqtt
import json, requests, threading, time
import numpy as np
import traceback

## @class ThingsBoardMQTT
class ThingsBoardMQTT:
	## @brief Constructor de la clase.
	# @param[in] token Token de acceso por el dispositivo.
	# @param[in] host URL del host con Thingsboard.
	# @param[in] port Puerto del servidor Thingsboard.
	# @param[in] timeout Timeout para mqtt.
	def __init__(self, token = None, host = 'smartemuco.ceisufro.cl', port = 1883, timeout = 30):
		#Almacena parametros de configuración
		## Token de acceso.
		self.token 		= token
		## Host Thingsboard.
		self.host 		= host
		## Puerto MQTT Thingsboard.
		self.port 		= port
		## Timeout MQTT
		self.timeout 	= timeout

		#Crea cliente mqtt
		## Cliente MQTT.
		self.client 				= mqtt.Client()
		self.client.username_pw_set(self.token)
		self.client.connected_flag	= False
		self.client.disconnect_flag	= True
		self.client.connect(self.host, self.port, self.timeout)
		# while not self.client.connected_flag: #wait in loop
		# 	print("In wait loop")
		# 	time.sleep(1)
		self.client.loop_start()
		self.client.on_disconnect 	= self.on_disconnect
		self.client.on_connect 		= self.on_connect
	
	## @brief Publica telemetría.
	# @param[in] data Telemetría a publicar.
	def publishTelemetry(self, data):
		self.client.publish('v1/devices/me/telemetry', json.dumps(data), 1)
	
	## @brief Callback al desconectarse del servidor MQTT.
	# @param[in] client Cliente MQTT.
	# @param[in] userdata 
	# @param[in] rc
	def on_disconnect(self, client, userdata, rc):
		# logging.info("disconnecting reason  "  +str(rc))
		self.client.connected_flag	= False
		self.client.disconnect_flag	= True
	
	## @brief Callback al conectarse al servidor MQTT.
	# @param[in] client Cliente MQTT.
	# @param[in] userdata
	# @param[in] flags
	# @param[in] rc
	def on_connect(self,client, userdata, flags, rc):
		if rc==0:
			self.client.connected_flag = True #set flag
			print("connected OK")
		else:
			print("Bad connection Returned code=",rc)
		
	## @brief Chequea si está conectado por MQTT.
	# @param[out] connected_flag Bandera que indica si el dispositivo sigue conectado.
	def isConnected(self):
		return self.client.connected_flag

## @class ThingsBoardHTTP
class ThingsBoardHTTP:
	## @brief Constructor de la clase.
	# @param[in] token Token de acceso del dispositivo.
	# @param[in] host Host con plataforma thingsboard.
	# @param[in] port Puerto de acceso MQTT.
	# @param[in] timeout Tiempo de espera máximo para las peticiones.
	def __init__(self, token, host, port = 8080, timeout = 5):
		## Token asociado al dispositivo.
		self.token 		= token		
		## Host con plataforma thingsboard.
		self.host 		= host		
		## Puerto de acceso a plataforma en el host.
		self.port 		= port		
		## Timeout para peticion http.
		self.timeout 	= timeout	
		self.userToken 	= ""
	
	## @brief Publica telemetría.
	# @param[in] data Paquete de telemetría a publicar.
	# @param[out] res Bandera que indica si la publicación fue exitosa.
	def publishTelemetry(self, data):
		headers = {'content-type': 'application/json'}
		try:
			try:
				#Publica telemetria
				r = requests.post(
					url 	= "http://%s:%d/api/v1/%s/telemetry"%(self.host, self.port, self.token), 
					data 	= json.dumps(data), 
					headers	= headers,
					timeout = self.timeout
				)
				return r.status_code == 200
			except:
				traceback.print_exc()
		except:
			traceback.print_exc()
		return False

	## @brief Publica los atributos del dispositivo.
	# @param[in] data Atributos del dispositivo.
	# @param[out] res Bandera que indica si la publicación fue exitosa.
	def publishAttributes(self, data):
		headers = {'content-type': 'application/json'}
		try:
			try:
				#Publica atributos del dispositivo
				r = requests.post(
					url 	= "http://%s:%d/api/v1/%s/attributes"%(self.host, self.port, self.token), 
					data 	= json.dumps(data), 
					headers = headers,
					timeout = self.timeout
				)
				return r.status_code == 200
			except:
				traceback.print_exc()
		except:
			traceback.print_exc()
		return False

	## @brief Autentica al usuario en thingsboard.
	# @param[in] username Usuario.
	# @param[in] password Contraseña.
	# @param[out] res Bandera que indica si la publicación fue exitosa.
	def login(self, username, password):
		headers = {'Content-Type': 'application/json','Accept': 'application/json'}
		data 	= {"username": username, "password": password}
		url 	= 'http://%s:%d/api/auth/login'%(self.host, self.port)
		try:
			r = requests.post(url = url, data = json.dumps(data), headers = headers, timeout = self.timeout)
			resp = r.json()
			self.userToken = "Bearer %s"%resp["token"]
			return r.status_code == 200
		except:
			return False
	
	## @brief Autentica al usuario en thingsboard.
	# keys - comma separated list of telemetry keys to fetch.
	# startTs - unix timestamp that identifies start of the interval in milliseconds.
	# endTs - unix timestamp that identifies end of the interval in milliseconds.
	# interval - the aggregation interval, in milliseconds.
	# agg - the aggregation function. One of MIN, MAX, AVG, SUM, COUNT, NONE.
	# limit - the max amount of data points to return or intervals to process.
	def getTimeSeries(self, deviceID, keys, startTs, endTs, interval = 1000, limit = 10000, agg = None):
		headers = {'Content-Type': 'application/json','X-Authorization': self.userToken}
		keyss = ','.join(keys)
		url = "http://%s:%d/api/plugins/telemetry/DEVICE/%s/values/timeseries?keys=%s&startTs=%d&endTs=%d&interval=%d&limit=%d"%(
			self.host, self.port, deviceID, keyss, startTs, endTs, interval, limit
		)
		# url = "http://%s:%d/api/plugins/telemetry/DEVICE/%s/values/timeseries?keys=%s&startTs=%d&endTs=%d&limit=%d"%(
		# 	self.host, self.port, deviceID, keyss, startTs, endTs, limit
		# )
		print(url)
		if not agg is None:
			url = url + "&agg=%s"%agg

		try:
			r = requests.get(url = url, headers = headers, timeout = self.timeout)
			print(r.text)
			if r.status_code == 200:
				return r.json()
			
		except:
			pass
		return None
		
		# curl -v -X GET "http://localhost:8080/api/plugins/telemetry'
		# '/DEVICE/ac8e6020-ae99-11e6-b9bd-2b15845ada4e/values
		# '/timeseries?keys=gas,temperature'
		# '&startTs=1479735870785&endTs=1479735871858&interval=60000&limit=100&agg=AVG" \
		# --header "Content-Type:application/json" \
		# --header "X-Authorization: $JWT_TOKEN"

class Logger:
	DEBUG 		= 0
	INFO 		= 1
	WARNING 	= 2
	ERROR  		= 3
	CRITICAL 	= 4
	EXCEPTION 	= 5

	def __init__(self, logger = None, telegramToken = None, chatId = None, name = "unnamed", level = DEBUG):
		self.logger 		= logger
		self.telegramToken 	= telegramToken
		self.name			= name
		self.level 			= level
		self.chatId 		= chatId
	
	def publish(self, msg, level):
		text = "%s - %s - %s - %s"%(self.name, str(np.datetime64('now')), level, msg)
		# tel = {
		# 	"text" 	: "%s - %s - %s - %s"%(self.name, str(np.datetime64('now')), level, msg),
		# 	"type" 	: "log"
		# }
		# th = threading.Thread(target=self.thingsboard.publishTelemetry, args=(tel,))
		# th.start()

		headers = {'Content-Type': 'application/json','Accept': 'application/json'}
		data 	= {"chat_id": self.chatId, "text": text}
		url 	= 'https://api.telegram.org/bot%s/sendMessage'%(self.telegramToken)
		try:
			r = requests.post(url = url, data = json.dumps(data), headers = headers)
			return r.status_code == 200
		except:
			import traceback
			traceback.print_exc()
			return False

	def debug(self, msg):
		self.logger.debug(msg)
		if self.level < 1:
			self.publish(msg, "debug")

	def info(self, msg):
		self.logger.info(msg)
		if self.level < 2:
			self.publish(msg, "info")
	
	def critical(self, msg):
		self.logger.critical(msg)
		if self.level < 5:
			self.publish(msg, "critical")
	
	def error(self, msg):
		self.logger.error(msg)
		if self.level < 4:
			self.publish(msg, "error")
	
	def exception(self, msg):
		self.logger.exception(msg)
		self.publish(msg, "exception")
	
	def warning(self, msg):
		self.logger.warning(msg)
		if self.level < 3:
			self.publish(msg, "warning")
	
	def warn(self, msg):
		self.logger.warning(msg)
		if self.level < 3:
			self.publish(msg, "warning")