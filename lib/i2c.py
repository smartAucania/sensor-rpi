#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## @file i2c.py
# @brief Contiene clase que actua como interfaz para puerto I2C.

import pigpio
import time
import traceback

## @class I2C
class I2C:
    ## @brief Constructor de la clase.
    # @param[in] busNum Número del bus I2C.
    # @param[in] address Dirección del dispositivo I2C.
    def __init__(self, busNum, address):
        ## Numero de bus.
        self.busNum     = busNum                                        
        ## Dirección del dispositivo i2c.
        self.address    = address                                       
        ## Objeto pigpio para acceder a bus i2c.
        self.pi         = pigpio.pi()                                   
        ## Apuntador a dispositivo i2c.
        self.device     = self.pi.i2c_open(self.busNum, self.address)   
    
    ## @brief Lee un bloque de datos. Retorna None en caso de fallar.
    # @param[in] size Tamaño del bloque en bytes.
    # @param[out] data Bloque de datos.
    def readBlock(self, size):
        try:
            (count, data) = self.pi.i2c_read_device(self.device, size)
        except:
            print("readBlock i2c falló")
            # traceback.print_exc()
            return None

        if count == size:
            return data
        else:
            print("readBlock leyó menos bytes de los esperados")
            return None

    ## @brief Escribe un bloque de datos.
    # @param[in] block Bloque de datos.
    # @param[out] res Bandera que indica una escritura exitosa.
    def writeBlock(self, block):
        try:
            self.pi.i2c_write_device(self.device, block)
        except:
            print("writeBlock error al escribir bloque de datos")
            # traceback.print_exc()
            return False
        return True

    ## @brief Lee un bloque de datos desde una dirección en específico. Retorna None en caso de fallar.
    # @param[in] address8List Dirección de un byte desde donde leer.
    # @param[in] size Tamaño del bloque a leer.
    # @param[out] data Bloque de datos.
    def readBlockFrom(self, address8List, size):
        for amount_tries in range(3):
            ret = self.writeBlock(address8List)
            if ret != True:
                print("readBlockFrom falla al apuntar a memoria")
                continue
            data = self.readBlock(size)
            if data:
                return data
            print("readBlockFrom La dirección propuesta no apunta a ningun dato")
        print("readBlockFrom falló al intentar 3 veces ejecutarse")
        return None
    
    ## @brief Reinicia puerto I2C.
    def reset(self):
        self.pi.i2c_close(self.device)
        time.sleep(0.5)
        self.device = self.pi.i2c_open(self.busNum, self.address)
        time.sleep(0.5)
    
    ## @brief Cierra puerto I2C.
    def close(self):
        try:
            self.pi.i2c_close(self.device)
        except:
            print("Unknown i2c handle")
    
    ## @brief Abre puerto I2C.
    def open(self):
        self.device = self.pi.i2c_open(self.busNum, self.address)
